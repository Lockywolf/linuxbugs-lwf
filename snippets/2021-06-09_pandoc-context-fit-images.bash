#!/bin/bash
# -*-  -*-
# Time-stamp: <2021-06-10 22:13:56 lockywolf>
#+title: How to make pandoc not break images.
#+author: lockywolf
#+date: <2021-06-10 Thu 22:13>
#+created: <2021-06-10 Thu 22:13>
#+refiled:
#+language: bash
#+category: computers
#+filetags: :pandoc:documents:markup
#+creator: Emacs 27.1

#https://tex.stackexchange.com/questions/159948/pandoc-generated-pdf-contains-large-images-how-can-i-make-them-fit-into-the-pag
set -e

# This is how you make pandoc scale images when they are huge.
# (I had this problem with Markdown)
# LateX didn't work, because it was afraid of too many images.

# pandoc -D mytemplate.latex
# insert %\setupexternalfigures[factor=fit]
# or \setupexternalfigures[scale=150]

#pandoc --template=mytemplate.latex [in] -o out.pdf
