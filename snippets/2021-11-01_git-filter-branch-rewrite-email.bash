#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2021-11-01 16:16:13 lockywolf>
#+title: 
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs 27.1



git filter-branch -f --env-filter '
OLD_EMAIL="test@example"
CORRECT_NAME="Correct Name"
CORRECT_EMAIL="example@test"
if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
