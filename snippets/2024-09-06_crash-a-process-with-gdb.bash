#!/bin/bash

if [[ "$1" == "" ]] ; then
  printf '%s\n' "usage: $0 <pid>"
  exit 1
fi

gdb -p "$1" -batch -ex 'set $rip=0'  -ex continue  -ex quit

