#!/bin/bash

set -e -o pipefail
shopt -s extglob

declare TAGSDIR
TAGSDIR="chinesepod_upper/"
declare TARGETDIR
TARGETDIR="chinesepod_transcripts/"
declare COPYTO
COPYTO="chinesepod_transcripts_renamed/"
#pushd "$TAGSDIR"
find "$TAGSDIR" -type f -print0 | while IFS= read -r -d '' filename; do
    #mv "$file" "${file// /_}"
    #    title=${title%%\.aac}
#    printf "filename=%s\n" "$filename"
    pattern="$TAGSDIR([[:digit:]]+)-([[:alpha:]_]+)-([[:alpha:]]+)-(.+)\.aac"
    #+([a-zA-Z])-+([a-zA-Z]*(.)"
    if [[ "$filename" =~ $pattern ]]
    then
	printf "File=%s\n" "$filename"
	number="${BASH_REMATCH[1]}"
	title="${BASH_REMATCH[4]}"
	printf "number=%s title=%s\n" "$number" "$title"
        find "$TARGETDIR" -iname "$number""trad-*" -print0 | while read -r -d '' targetfile
	do
	    if [[ "$targetfile" =~ "$TARGETDIR"(.*)(\.html|\.pdf|\.htm)$ ]]
	    then
		targetbase="${BASH_REMATCH[1]}"
		targetext="${BASH_REMATCH[2]}"
		#printf "targetbase=%s ext=%s\n" "$targetbase" "$targetext"
		mv -v "$TARGETDIR$targetbase$targetext"  "$COPYTO$targetbase-$title$targetext"
	    else
		printf "Match target failed: targetfile=%s\n" "$targetfile"
		exit 1
	    fi
	done
    else
	printf "Match failed: %s pattern=%s\n" "$filename" "$pattern"
	exit 1
    fi
done

