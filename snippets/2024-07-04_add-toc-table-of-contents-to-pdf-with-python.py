#!/usr/bin/env python3

import fitz  # PyMuPDF


def add_toc_to_pdf(input_pdf, output_pdf, toc_entries):
    """
    Add a table of contents (TOC) to a PDF file.

    Parameters:
    input_pdf (str): The path to the input PDF file.
    output_pdf (str): The path to the output PDF file.
    toc_entries (list): A list of TOC entries. Each entry is a list with three elements:
                        [level, title, page number].

    Example of toc_entries:
    [
        [1, 'Chapter 1', 1],
        [2, 'Section 1.1', 2],
        [2, 'Section 1.2', 3],
        [1, 'Chapter 2', 4],
        [2, 'Section 2.1', 5]
    ]
    """
    # Open the input PDF
    pdf_document = fitz.open(input_pdf)

    # Set the table of contents
    pdf_document.set_toc(toc_entries)

    pdf_document.set_page_labels([{'startpage': 1, 'style': 'r', 'firstpagenum': 10},
                                  {'startpage': 6, 'style': 'D', 'firstpagenum': 1}])

    # Save the output PDF with the TOC
    pdf_document.save(output_pdf)
    print(f"Table of contents added and saved to {output_pdf}")

# Example usage
input_pdf_path = "input.pdf"
output_pdf_path = "output_with_toc.pdf"
toc_entries = [
    [1, 'Preface', 4],
    [1, 'Chapter 1: Banach Function Spaces', 6],
    [2, 'Section 1.1: Banach Function Spaces', 7],
    [2, 'Section 1.2: The Associate Space', 12],
    [2, 'Section 1.3: Absolute Continuity Of The Norm', 18],
    [2, 'Section 1.4: Duality And Reflexivity', 24],
    [2, 'Section 1.5: Separability', 29],
    [2, 'EXERCISES AND FURTHER RESULTS FOR CHAPTER 1', 35],
    [2, 'NOTES FOR CHAPTER 1', 38],
    [1, 'Chapter 2: Rearrangement-Invariant Banach Function Spaces', 39],
    [1, 'Chapter 3: Interpolation of Operators on Rearrangement-Invariant Spaces', 98],
    [1, 'Chapter 4: The Classical Interpolation Theorems', 185],
    [1, 'Chapter 5: The K-Method', 292],
    [1, 'Appendix A', 442],
    [1, 'References', 444],
    [1, 'Bibliography', 445],
    [1, 'Index', 460],
    [1, 'List of Notations', 460],

]

add_toc_to_pdf( "Colin-Bennett-and-Robert-Sharpley_Interpolation-of-operators.pdf", "Colin-Bennett-and-Robert-Sharpley_Interpolation-of-operators.withtoc.pdf", toc_entries)


