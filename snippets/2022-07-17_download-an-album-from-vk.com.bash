#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-07-18 00:35:26 lockywolf>
#+title: 
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


# Stage : mkdir album name
#https://vk.com/album-4408148_43908286


# stage 0: obtain ids
URL="$1"

printf "Debugging, URL=%s\n" "$URL"

LWF_RE="https://vk.com/album([[:digit:]-]*)_([[:digit:]]*)"

if [[ "$URL" =~ $LWF_RE ]]
then
  printf "owner_id=%s\n" "${BASH_REMATCH[1]}"
  owner_id="${BASH_REMATCH[1]}"
  printf "album_id=%s\n" "${BASH_REMATCH[2]}"
  album_id="${BASH_REMATCH[2]}"
else
  printf "does not match\n"
fi

# Stage 1: obtain the json
curl "https://api.vk.com/method/photos.get?owner_id=""$owner_id""&album_id=""$album_id""&count=1000&offset=0&access_token=08ee532d08ee532d08ee532dbc08a6df23008ee08ee532d54ae800969245a7851077ff5&v=5.131" | jq > photolist.json

# Stage 2: obtain the 
/bin/cat photolist.json  | jq -r '.response.items[] | .sizes | .[-1] | .url' > urls.wget

# wget it

wget --continue --input-file urls.wget

for i in *jpg*
do
  mv ./"$i" ./"$i".jpg
done
