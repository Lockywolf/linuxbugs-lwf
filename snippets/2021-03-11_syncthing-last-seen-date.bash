# -*- mode:sh; -*-
# Time-stamp: <2021-03-12 12:44:56 lockywolf>
#+title: Check sync status of syncthing.
#+author: lockywolf
#+date:
#+created: <2021-03-11 Thu 20:48>
#+language: bash
#+category: utils
#+filetags: :conky:utils
#+creator: Emacs 27.1


curl -X GET -H "X-API-Key: MY-KEY" -s 'http://localhost:8384/rest/stats/device' | sed 's/[^[:print:]]//g' | jq -Mar -c '."DEVICE-ID"."lastSeen"' | sed 's/\"//g'
