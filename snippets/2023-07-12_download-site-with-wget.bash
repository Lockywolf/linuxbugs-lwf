#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-07-12 14:26:54 lockywolf>
#+title: Download a site mirror with wget
#+author: lockywolf
#+date: 
#+created: <2023-07-12 Wed>
#+refiled:
#+language: bash
#+category: scripts
#+filetags: :books:download:website
#+creator: Emacs-30.0.50/org-mode-9.6.1


set -e

wget --recursive --no-clobber --page-requisites --html-extension --convert-links  --domains 'www.test.example' --no-parent 'https://www.test.example/'
