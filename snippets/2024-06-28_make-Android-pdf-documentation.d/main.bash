#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2024-07-04 09:38:24 lockywolf>
#+title: Make Android documentation into pdf.
#+author: lockywolf
#+date: 
#+created: <2024-06-28 Fri 13:19:37>
#+refiled:
#+language: bash
#+category: programming
#+filetags: :linux:latex:pdf:documentation:android:programming:software:reading:books
#+creator: Emacs-30.0.50/org-mode-9.7-pre

set -x

CWD="$(pwd)"
cd "$(dirname "$0")" || exit 1


mkdir out

cd out

#curl https://source.android.com/docs/core >  core.xhtml

xmlstarlet fo --html core.xhtml > formatted.core.xhtml || { printf "main doc file is not valid html, something is wrong\n" ; exit 1 ; }

xmlstarlet sel --help --html core.xhtml


# pandoc --pdf-engine=lualatex -f html -o file.tex https://source.android.com/docs/core https://source.android.com/docs/core/architecture
