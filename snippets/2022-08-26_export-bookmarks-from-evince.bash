#!/usr/bin/env bash

# apply this command to a .pdf or .djvu file previously opened in evince.

gio info -a "metadata::evince::bookmarks" "$@"

