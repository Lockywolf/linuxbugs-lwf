#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-09-22 17:18:11 lockywolf>
#+title: 
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs-30.0.50/org-mode-9.6.8Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


if [[ "$1" == "" ]] ; then
  printf "Usage: %s pid\n" "$0" 1>&2
fi

readlink /proc/$(pstree -p $1  | head -n 1 | sed -E 's/.*\(([[:digit:]]*)\).*/\1/g')/exe
