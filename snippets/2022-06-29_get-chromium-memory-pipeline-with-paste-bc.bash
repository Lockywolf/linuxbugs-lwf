#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-06-29 12:13:57 lockywolf>
#+title: Get chromium memory consumption with ps.
#+author: lockywolf
#+date: 
#+created: <2022-06-29 Wed 12:13>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :memory:admin:performance
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23
 

ps -A v --sort rss | grep chromium | cut -c 1-120 | awk '{print $8; }' | paste -s -d + - | bc
