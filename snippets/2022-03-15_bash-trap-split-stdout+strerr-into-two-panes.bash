#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-03-15 12:01:19 lockywolf>
#+title: 
#+author: tirnanog
#+date: 
#+created: <2022-03-15 Tue 12:01>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :bash:trap:signal:stderr
#+creator: Emacs 27.2


# Set up temp file
stderr=
trap 'rm -f -- "$stderr"' 0
stderr=$(mktemp) || exit

# run rsync
rsync_command >>"$LOGFILE" 2>"$stderr"
rc=$?

# upon success, silence the standard output
[ $rc = 0 ] && exec >/dev/null

# convey stderr to the standard output and the logfile
sed -e 's/^/STDERR: /' "$stderr" | tee -a "$LOGFILE"

# propagate the exit status
exit $rc
