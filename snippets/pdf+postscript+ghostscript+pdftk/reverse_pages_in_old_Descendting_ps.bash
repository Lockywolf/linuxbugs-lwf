#!/bin/bash

pstops 1:-0 "$1" "$2"

# or
# note that pdfkt cat accepts 18-1. I should also accept end-1, but I didn't manage.
# ps2pdf file.ps
# pdftk FKNN.pdf cat 18-1 output fknn2.pdf

