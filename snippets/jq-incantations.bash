#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2021-12-24 09:49:51 lockywolf>
#+title: How to use jq to query json output of everything.
#+author: lockywolf
#+date: 
#+created: <2021-12-03 Fri 23:12>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :bash:admin:jq:json:ip:ip-addr:network
#+creator: Emacs 27.1



# Obtail IP address from ip
/sbin/ip -j a | jq '.[] | select(.ifname=="<whatever>").addr_info | .[] | select(.scope=="global").local'

# Get default broadcast. The | select( .dst="default" ) construct.
B_ADDR="$(/sbin/ip -j addr show dev $(/sbin/ip -json route | jq -r '.[] | select(.dst=="default").dev') | jq -r '.[0].addr_info[0].broadcast')"
