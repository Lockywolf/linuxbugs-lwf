git config --global alias.diffstat '!git --no-pager diff --stat --stat-width=160 -w --ignore-line-breaks'

git symbolic-ref --hort HEAD
