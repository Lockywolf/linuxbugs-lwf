#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2024-04-25 14:21:42 lockywolf>
#+title: 
#+author: 
#+date: 
#+created: <2024-04-25 Thu 13:53:14>
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs-30.0.50/org-mode-9.7-pre


1. root@laptop:/tmp# l_good=0 ; l_bad=0 ; for i in dbus* ; do printf '%s\n' "$i"; timeout  1s dbus-monitor --address  unix:path=/tmp/$i --monitor ; if [[ "$?" == 124 ]] ; then echo good ; l_good=$(( l_good + 1 )) ; else  rm /tmp/$i  ; l_bad=$(( l_bad + 1 )) ; fi  ; done  ; echo l_good=$l_good ; echo l_bad=$l_bad
dbus-0LeCmKRJot
Failed to register connection to bus at unix:path=/tmp/dbus-0LeCmKRJot: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-2C2n98pCvq
Failed to register connection to bus at unix:path=/tmp/dbus-2C2n98pCvq: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-41NFOjWvbC
Failed to register connection to bus at unix:path=/tmp/dbus-41NFOjWvbC: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-4ienmYJCKS
Failed to register connection to bus at unix:path=/tmp/dbus-4ienmYJCKS: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-4kqCkARUjA
Failed to register connection to bus at unix:path=/tmp/dbus-4kqCkARUjA: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-5M302TDfQj
Failed to register connection to bus at unix:path=/tmp/dbus-5M302TDfQj: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-5Ua3n9MYak
Failed to register connection to bus at unix:path=/tmp/dbus-5Ua3n9MYak: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-5hrdCMFxT3
Failed to register connection to bus at unix:path=/tmp/dbus-5hrdCMFxT3: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-6USrLRAd5u
Failed to register connection to bus at unix:path=/tmp/dbus-6USrLRAd5u: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-6vEBklWZIb
Failed to register connection to bus at unix:path=/tmp/dbus-6vEBklWZIb: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-8LLW4Vo6AA
Failed to register connection to bus at unix:path=/tmp/dbus-8LLW4Vo6AA: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-8WrB4Fewz2
Failed to register connection to bus at unix:path=/tmp/dbus-8WrB4Fewz2: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-8b1OV4u4bW
Failed to register connection to bus at unix:path=/tmp/dbus-8b1OV4u4bW: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-9128IvWAoZ
Failed to register connection to bus at unix:path=/tmp/dbus-9128IvWAoZ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-9K3JuDBttE
Failed to register connection to bus at unix:path=/tmp/dbus-9K3JuDBttE: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-AHxtBSJLxZ
Failed to register connection to bus at unix:path=/tmp/dbus-AHxtBSJLxZ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-AOXe5YhYhl
Failed to register connection to bus at unix:path=/tmp/dbus-AOXe5YhYhl: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-B123dnblkY
signal time=1714024300.539911 sender=org.freedesktop.DBus -> destination=:1.57 serial=2 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameAcquired
   string ":1.57"
signal time=1714024300.539942 sender=org.freedesktop.DBus -> destination=:1.57 serial=4 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameLost
   string ":1.57"
good
dbus-BMqMgSmLiH
Failed to register connection to bus at unix:path=/tmp/dbus-BMqMgSmLiH: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-CAxUGQzJim
Failed to register connection to bus at unix:path=/tmp/dbus-CAxUGQzJim: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-DLfauDfPcd
Failed to register connection to bus at unix:path=/tmp/dbus-DLfauDfPcd: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-EOEEEYXCrJ
Failed to register connection to bus at unix:path=/tmp/dbus-EOEEEYXCrJ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-EVkF5BpnxL
Failed to register connection to bus at unix:path=/tmp/dbus-EVkF5BpnxL: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-EmsDFxdhCv
Failed to register connection to bus at unix:path=/tmp/dbus-EmsDFxdhCv: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-F5gSbugzv3
signal time=1714024301.610443 sender=org.freedesktop.DBus -> destination=:1.13779 serial=2 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameAcquired
   string ":1.13779"
signal time=1714024301.610472 sender=org.freedesktop.DBus -> destination=:1.13779 serial=4 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameLost
   string ":1.13779"
signal time=1714024301.611639 sender=:1.1 -> destination=(null destination) serial=269527 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=EventListenerDeregistered
   string ":1.13779"
   string ""
method call time=1714024301.611715 sender=:1.4538 -> destination=org.a11y.atspi.Registry serial=9248 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611727 sender=:1.67 -> destination=org.a11y.atspi.Registry serial=13717 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611760 sender=:1.55 -> destination=org.a11y.atspi.Registry serial=13717 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611766 sender=:1.54 -> destination=org.a11y.atspi.Registry serial=13717 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611772 sender=:1.2022 -> destination=org.a11y.atspi.Registry serial=11763 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611775 sender=:1.47 -> destination=org.a11y.atspi.Registry serial=13717 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method call time=1714024301.611786 sender=:1.51 -> destination=org.a11y.atspi.Registry serial=13717 path=/org/a11y/atspi/registry; interface=org.a11y.atspi.Registry; member=GetRegisteredEvents
method return time=1714024301.612056 sender=:1.1 -> destination=:1.4538 serial=269528 reply_serial=9248
   array [
   ]
method return time=1714024301.612070 sender=:1.1 -> destination=:1.67 serial=269529 reply_serial=13717
   array [
   ]
method return time=1714024301.612107 sender=:1.1 -> destination=:1.55 serial=269530 reply_serial=13717
   array [
   ]
method return time=1714024301.612118 sender=:1.1 -> destination=:1.54 serial=269531 reply_serial=13717
   array [
   ]
method return time=1714024301.612145 sender=:1.1 -> destination=:1.2022 serial=269532 reply_serial=11763
   array [
   ]
method return time=1714024301.612155 sender=:1.1 -> destination=:1.47 serial=269533 reply_serial=13717
   array [
   ]
method return time=1714024301.612159 sender=:1.1 -> destination=:1.51 serial=269534 reply_serial=13717
   array [
   ]
good
dbus-FApTjaGRLl
Failed to register connection to bus at unix:path=/tmp/dbus-FApTjaGRLl: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-FKjh5XY7oW
Failed to register connection to bus at unix:path=/tmp/dbus-FKjh5XY7oW: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-FixfOPREpM
Failed to register connection to bus at unix:path=/tmp/dbus-FixfOPREpM: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Fq5DAD8IFc
Failed to register connection to bus at unix:path=/tmp/dbus-Fq5DAD8IFc: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-GBMO3HdO8T
Failed to register connection to bus at unix:path=/tmp/dbus-GBMO3HdO8T: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-GxLElAJHEr
Failed to register connection to bus at unix:path=/tmp/dbus-GxLElAJHEr: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Gyv6gt3kBe
Failed to register connection to bus at unix:path=/tmp/dbus-Gyv6gt3kBe: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-HYbTv4xifg
Failed to register connection to bus at unix:path=/tmp/dbus-HYbTv4xifg: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-IrpyLHs7wc
Failed to register connection to bus at unix:path=/tmp/dbus-IrpyLHs7wc: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-JJR801DXkA
Failed to register connection to bus at unix:path=/tmp/dbus-JJR801DXkA: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-KccqtQyt9U
Failed to register connection to bus at unix:path=/tmp/dbus-KccqtQyt9U: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-L6oXvKGCZv
Failed to register connection to bus at unix:path=/tmp/dbus-L6oXvKGCZv: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-LLHluATcEp
Failed to register connection to bus at unix:path=/tmp/dbus-LLHluATcEp: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-LLSmHvBhE9
Failed to register connection to bus at unix:path=/tmp/dbus-LLSmHvBhE9: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-NRrGpjfSSt
Failed to register connection to bus at unix:path=/tmp/dbus-NRrGpjfSSt: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-PD3T0zg4HN
Failed to register connection to bus at unix:path=/tmp/dbus-PD3T0zg4HN: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-PJXKB0WAyW
Failed to register connection to bus at unix:path=/tmp/dbus-PJXKB0WAyW: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-PZLaGW4N9K
Failed to register connection to bus at unix:path=/tmp/dbus-PZLaGW4N9K: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Phq9STMMQz
Failed to register connection to bus at unix:path=/tmp/dbus-Phq9STMMQz: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-QO2TDiGUGo
Failed to register connection to bus at unix:path=/tmp/dbus-QO2TDiGUGo: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-QXGIvwddnH
Failed to register connection to bus at unix:path=/tmp/dbus-QXGIvwddnH: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-QkpvGc6szq
Failed to register connection to bus at unix:path=/tmp/dbus-QkpvGc6szq: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-R7b5DeSxv7
Failed to register connection to bus at unix:path=/tmp/dbus-R7b5DeSxv7: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-SGFJuEhrR6
Failed to register connection to bus at unix:path=/tmp/dbus-SGFJuEhrR6: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-SlvocxBaB9
Failed to register connection to bus at unix:path=/tmp/dbus-SlvocxBaB9: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-T2h4Dnfprw
Failed to register connection to bus at unix:path=/tmp/dbus-T2h4Dnfprw: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-TRMMJDiqlL
Failed to register connection to bus at unix:path=/tmp/dbus-TRMMJDiqlL: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Uh9mcztDVR
Failed to register connection to bus at unix:path=/tmp/dbus-Uh9mcztDVR: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-UlxWxAOnJe
Failed to register connection to bus at unix:path=/tmp/dbus-UlxWxAOnJe: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-VWtHx7c4DQ
Failed to register connection to bus at unix:path=/tmp/dbus-VWtHx7c4DQ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-VbSAQxK9aF
Failed to register connection to bus at unix:path=/tmp/dbus-VbSAQxK9aF: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-XH6v4Xz95O
Failed to register connection to bus at unix:path=/tmp/dbus-XH6v4Xz95O: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-XpBFpayUYR
Failed to register connection to bus at unix:path=/tmp/dbus-XpBFpayUYR: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-YGcYykLuqm
Failed to register connection to bus at unix:path=/tmp/dbus-YGcYykLuqm: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-ZET9W7PNho
Failed to register connection to bus at unix:path=/tmp/dbus-ZET9W7PNho: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Zk1DY6HVuy
Failed to register connection to bus at unix:path=/tmp/dbus-Zk1DY6HVuy: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-Zo4HJb8KZW
Failed to register connection to bus at unix:path=/tmp/dbus-Zo4HJb8KZW: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-avyuzhNX6f
Failed to register connection to bus at unix:path=/tmp/dbus-avyuzhNX6f: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-cA3EE10pBM
Failed to register connection to bus at unix:path=/tmp/dbus-cA3EE10pBM: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-cRevR0L3CW
Failed to register connection to bus at unix:path=/tmp/dbus-cRevR0L3CW: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-cn45jGJX6U
Failed to register connection to bus at unix:path=/tmp/dbus-cn45jGJX6U: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-dT7VMHfJJZ
Failed to register connection to bus at unix:path=/tmp/dbus-dT7VMHfJJZ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-gzv8XJZ1j5
Failed to register connection to bus at unix:path=/tmp/dbus-gzv8XJZ1j5: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-hOAuuyjDrI
Failed to register connection to bus at unix:path=/tmp/dbus-hOAuuyjDrI: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-iKYca8oRYH
Failed to register connection to bus at unix:path=/tmp/dbus-iKYca8oRYH: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-j7ZkscTACh
Failed to register connection to bus at unix:path=/tmp/dbus-j7ZkscTACh: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-jvwqquU6Dg
Failed to register connection to bus at unix:path=/tmp/dbus-jvwqquU6Dg: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-k75u0roNBR
Failed to register connection to bus at unix:path=/tmp/dbus-k75u0roNBR: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-kDyo7RGdaV
Failed to register connection to bus at unix:path=/tmp/dbus-kDyo7RGdaV: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-kMaAxHlOYy
Failed to register connection to bus at unix:path=/tmp/dbus-kMaAxHlOYy: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-lZGl0gdtrT
Failed to register connection to bus at unix:path=/tmp/dbus-lZGl0gdtrT: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-m4NKXOPCA3
Failed to register connection to bus at unix:path=/tmp/dbus-m4NKXOPCA3: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-mM9WOwG7PB
Failed to register connection to bus at unix:path=/tmp/dbus-mM9WOwG7PB: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-mdhvtLOtIQ
Failed to register connection to bus at unix:path=/tmp/dbus-mdhvtLOtIQ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-mwpyYGXA4u
Failed to register connection to bus at unix:path=/tmp/dbus-mwpyYGXA4u: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-nVsvl2SYOX
Failed to register connection to bus at unix:path=/tmp/dbus-nVsvl2SYOX: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-oGMsjAa9IR
Failed to register connection to bus at unix:path=/tmp/dbus-oGMsjAa9IR: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-oKQ0jMJEXH
Failed to register connection to bus at unix:path=/tmp/dbus-oKQ0jMJEXH: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-pAjlEci6Ae
Failed to register connection to bus at unix:path=/tmp/dbus-pAjlEci6Ae: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-qQWtrkO7VD
Failed to register connection to bus at unix:path=/tmp/dbus-qQWtrkO7VD: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-qork7hIwDx
Failed to register connection to bus at unix:path=/tmp/dbus-qork7hIwDx: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-rjPTMdGife
Failed to register connection to bus at unix:path=/tmp/dbus-rjPTMdGife: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-s1CDpPmfBr
Failed to register connection to bus at unix:path=/tmp/dbus-s1CDpPmfBr: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-sNKjXUJADP
Failed to register connection to bus at unix:path=/tmp/dbus-sNKjXUJADP: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-sUB4wmLaEV
Failed to register connection to bus at unix:path=/tmp/dbus-sUB4wmLaEV: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-t33pmSoDkj
Failed to register connection to bus at unix:path=/tmp/dbus-t33pmSoDkj: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-tL0oVo86P6
Failed to register connection to bus at unix:path=/tmp/dbus-tL0oVo86P6: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-tY544t2HGx
Failed to register connection to bus at unix:path=/tmp/dbus-tY544t2HGx: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-uS2uEsCXhI
Failed to register connection to bus at unix:path=/tmp/dbus-uS2uEsCXhI: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-uenehZdTKp
Failed to register connection to bus at unix:path=/tmp/dbus-uenehZdTKp: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-vFDhAf3WLR
Failed to register connection to bus at unix:path=/tmp/dbus-vFDhAf3WLR: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-vcQxpkzpsj
Failed to register connection to bus at unix:path=/tmp/dbus-vcQxpkzpsj: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-veCNwDnpCJ
Failed to register connection to bus at unix:path=/tmp/dbus-veCNwDnpCJ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-vhOCzX8jpi
Failed to register connection to bus at unix:path=/tmp/dbus-vhOCzX8jpi: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-vlEQFp2XMI
Failed to register connection to bus at unix:path=/tmp/dbus-vlEQFp2XMI: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-wQMfAwFR4D
Failed to register connection to bus at unix:path=/tmp/dbus-wQMfAwFR4D: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-x16eB0v0Q4
Failed to register connection to bus at unix:path=/tmp/dbus-x16eB0v0Q4: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-xUVZSRpbYY
Failed to register connection to bus at unix:path=/tmp/dbus-xUVZSRpbYY: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-xcwH67coNZ
Failed to register connection to bus at unix:path=/tmp/dbus-xcwH67coNZ: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-xmlyw0aLgj
Failed to register connection to bus at unix:path=/tmp/dbus-xmlyw0aLgj: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-yD83lz7mLG
Failed to register connection to bus at unix:path=/tmp/dbus-yD83lz7mLG: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-yidsNHSvjB
Failed to register connection to bus at unix:path=/tmp/dbus-yidsNHSvjB: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-yodGXvQAl8
Failed to register connection to bus at unix:path=/tmp/dbus-yodGXvQAl8: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-yq97jylq8T
Failed to register connection to bus at unix:path=/tmp/dbus-yq97jylq8T: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
dbus-zmYfzW6MUm
Failed to register connection to bus at unix:path=/tmp/dbus-zmYfzW6MUm: Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.
l_good=2
l_bad=108


2. lsof -U | grep '^dbus' | grep lockywolf

3. If it helps: for task in $( ps -e | awk '{print $1}') ; do echo -n "PID: \"$task\" "; grep ctxt /proc/$task/status; done

