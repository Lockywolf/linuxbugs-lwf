#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2024-12-12 19:32:34 lockywolf>
#+title: Print word usage from time, learning Chinese.
#+author: 
#+date: 
#+created: <2024-05-09 Thu 13:19:26>
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs-30.0.50/org-mode-9.7-pre

if [[ $1 == "" ]] ; then
  printf "usage: %s <dir>" "$0"
fi

{
echo date words-learnt
for f in "$1"/*.pqb ; do
#  echo "$f"
  REG='flashbackup-([[:digit:]]{10})\.pqb'
  if [[ "$f" =~ $REG ]] ; then
#    echo matches "${BASH_REMATCH[1]}"
    cdate="${BASH_REMATCH[1]}"
    ndate=$(stat --format='%y' "$f" | cut -f 1 -d ' ')
    #ndate=$(date --date="$ndate" +%s)
  else
    echo "not matches!"
    exit 1
  fi
  nwords=$(sqlite3 "$f" 'select count(*) from pleco_flash_cards;')
  printf "%s %s\n" "$ndate" "$nwords"

done } > print-word-usages.csv

echo finished successfully
