set terminal jpeg size 1024,768
# Specify the output file name
set output "output_graph.jpg"

# Set the x-axis data format
set xdata time
set timefmt "%Y-%m-%d"

# Set the output format for the x-axis
set format x "%Y-%m-%d"

# Rotate x-axis labels to avoid overlap
set xtics rotate by -45

# Set the x and y axis labels
set xlabel "Date"
set ylabel "Words Learnt"

# Set the title of the graph
set title "Date vs Words-Learnt"

# Plot the graph
plot "print-word-usages.csv" using 1:2 with linespoints title "Words Learnt"
