;;; 2023-05-26_replace-text-as-regex-with-emacs-rx.el --- replace string by regex  -*- lexical-binding: t; -*-

(defun my-replace ()
  (interactive)
  (save-excursion
    (while (re-search-forward (rx (seq "\\mypar " (submatch (*? anychar)) ?\n ?\n )) nil t)
      (replace-match "\\\\myparwitharg{\\1}\n\n" nil nil))))


(defun my-delete ()
  (interactive)
  (while (re-search-forward
          (rx (seq "Node:" (submatch (*? not-newline)) ?\n
                   "Next:" (submatch (*? not-newline)) ?\n
                   "Previous:" (submatch (*? not-newline)) ?\n
                   "Up:" (submatch (*? not-newline)) ?\n
                   "<br>" ?\n)) nil t)
    (delete-region (point) (match-beginning 0) ))
  ;;(set-mark-command nil)
  ;;(goto-char (match-beginning 0))
  )

(provide '2023-05-26_replace-text-as-regex-with-emacs-rx)
;;; 2023-05-26_replace-text-as-regex-with-emacs-rx.el ends here

