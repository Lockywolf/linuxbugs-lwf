#!/usr/bin/bash
# epub to pdf with pandoc
pandoc --variable=documentclass:extarticle --variable=papersize:a4 --variable=fontsize:14pt --variable=margin-left:1cm --variable=margin-right:1cm --variable=margin-top:1cm --variable=margin-bottom:2cm --pdf-engine=xelatex  --variable mainfont="Palatino" --variable sansfont="Helvetica" --variable monofont="Menlo" -f epub -t latex -o toprint.pdf toconvert.epub
# markdown to pdf with bad fonts and such
pandoc -N --template=template.tex --variable mainfont="Palatino" --variable sansfont="Helvetica" --variable monofont="Menlo" --variable fontsize=12pt --variable version=2.0 MANUAL.txt --pdf-engine=xelatex --toc -o example14.pdf

# pandoc -V geometry:left=2cm -V geometry:right=2cm -V geometry:top=1cm -V geometry:bottom=2cm Michael\ A.\ Hunzeker\ -\ Dying\ to\ Learn_\ Wartime\ Lessons\ from\ the\ Western\ Front\ \(Cornell\ Studies\ in\ Security\ Affairs\)-Cornell\ University\ Press\ \(2021\).epub -o test.pdf
