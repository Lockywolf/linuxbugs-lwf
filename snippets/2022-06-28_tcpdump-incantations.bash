#!/bin/bash
#+timestamp: Time-stamp: < >
#+created: 2022-06-28


tcpdump -vvv -n --immediate-mode -i any 'host 192.168.1.1' -w - -U | tee /tmp/server-tcpdump.pcap | tcpdump -r -
