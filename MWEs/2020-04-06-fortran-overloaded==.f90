module test

  type test_type
     character(len=:), allocatable :: value
   contains
     procedure :: eq_test_test
     procedure :: eq_test_char
     generic, public :: operator(==) => eq_test_test, eq_test_char
  end type test_type
contains
  logical function eq_test_test( this, that ) result( retval )
    class(test_type), intent(in) :: this
    class(test_type), intent(in) :: that
    retval = .true.
  end function eq_test_test
  logical function eq_test_char( this, that ) result( retval )
    class(test_type), intent(in) :: this
    character, intent(in) :: that
    retval = .false.
  end function eq_test_char

  subroutine test_eq()
    type(test_type) :: t1
    type(test_type) :: t2
    character(len=:), allocatable :: t3
    t3 = "hello"
    if (t1 == t2) then
       print *, "test dt =="
    end if
    if (t1 == t3) then
       print *, "test char =="
    end if
    if (t1 == "hello") then
       print *, "test char =="
    end if
  end subroutine test_eq
  
end module test

program main
end program main
