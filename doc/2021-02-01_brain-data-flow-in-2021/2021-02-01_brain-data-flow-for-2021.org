# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2021-03-12 12:52:35 lockywolf>
#+title: A sketch of a brain data flow for an adult in 2021.
#+author: lockywolf
#+date: unpublished
#+created: <2021-02-01 Mon 13:33>
#+language: hx
#+category: programming
#+filetags: :uml:programming:computers:software:software architecture:systems engineering:systems
#+creator: Emacs 27.1/org-mode 9.4


This sketch is not (yet) implemented in software, it is just an imaginary construct that may be useful for reference.

* TODO Body

#+begin_src plantuml :noweb no :exports both :file 2021-01-01_overall-component-diagram.png
@startuml

skinparam componentStyle uml2

header some header

footer some footer

title A plan for data flow in 2021

caption Unfinished. Version <2021-02-01 Mon 15:49>

legend
Unfinished
TODO:
1. Style services
2. Style crons
3. Style setups
4. Style TODO/DONE
5. Style friendly/adversarial services
6. Mark points of human intervention (e.g. captcha)
7. Make components consistent
end legend

package "Foreign Services" {
component "Facebook" as cFb
component "Twitter" as cTw
component "Instagram" as cIn
component "Telegram" as cTg
component "Vkontakte" as cVk
component "WeChat" as cWc
component "Discord" as cDi
component "Jabber" as cJa
component "TikTok" as cTt
component "DouYin" as cDy
component "WeiBo" as cWb
component "LinkedIn" as cLi
component "Skype" as cSk
component "LiveJournal" as cLj
component "WordPress" as cWp
component "Blogger" as cBg
component "WhatsApp" as cWa
component "AcademiaEdu" as cAe
component "ResearchGate" as cRg
}

cloud "Assorted Providers <<email>>" as cAP

package DigitalOcean{
database "Dovecot (qmail?)" as dbDovecot
node "RSS-Bridge" as cRB
node "TT-RSS" as cTTRSS
database "PostgreSQL" as dbPG
node "Panopticon (TODO)\nContacts manager" as cPan
}



cFb -> cRB : News
cTw -> cRB : News
cIn -> cRB : News
cTg -> cRB : News
cVk -> cRB : News
cWc -> cRB : News


cAP --> dbDovecot : Via for-site-email@domain.name

dbDovecot --> cRB



cRB --> cTTRSS
cloud "RSS" as cRSS

cRSS --> cTTRSS



cTTRSS --> dbPG



cFb -> cPan : Contacts
cTw -> cPan : Contacts
cIn -> cPan : Contacts
cTg -> cPan : Contacts
cVk -> cPan : Contacts
cWc -> cPan : Contacts


package Google {
database "Google Contacts" as dbGcontacts
database "Gmail" as dbGmail
}



cPan -> dbGcontacts

dbGcontacts -> cRB : Feed information


dbGcontacts --> dbGmail : Context provider
dbGmail     --> dbGcontacts : Other contacts miner

package Android {
database "Android\nAddress\nBook" as dbAndroidContacts
database "Androind\nGmail" as dbAndroidGmail
}


dbGcontacts <--> dbAndroidContacts
dbGmail <--> dbAndroidGmail


package Laptop {
database "vdir" as dbVdir
node "khard" as cKhard
node "mu" as cMu
database "maildir" as dbMaildir
component "Thunderbird" as cTb
cTb <--> dbGcontacts
cTb <--> dbGmail

package Emacs {
node "ebdb" as cEbdb
node "mu4e" as cMu4e
}
}

dbGcontacts <--> dbVdir : vdirsyncer


cKhard<--> dbVdir


cEbdb<--> dbVdir


dbGmail <--> dbMaildir : mbsync




dbMaildir <--> cMu
cMu <--> cMu4e

@enduml
#+end_src

#+RESULTS:
[[./2021-02-01_overall-component-diagram.png]]


