;;; -*- mode: org; -*-
;; Time-stamp: <2022-02-18 13:19:09 lockywolf>
;; Created    : 2019-05-21T12:50:31 by lockywolf

* How I tried to tune font and colour parameters in Slackware.
** Find a good font
I found it best to use the KDE Font Manager from KDE System Settings
(systemsettings command)
Choose the most representative text for you. Latin is included automatically,
so I wrote a bit in French, Russian, Simplified Chinese and Traditional
Chinese.
Then I created a group for fonts which have all the characters I needed, and
started comparing them. You can also launch two instances of the Font Manager
to compare fonts side by side.

(In the meantime you can try to make other groups, for example, for the fonts
you like, or for the ones which have the glyphs for the languages you need.)

In my case, there are not too many options: very fonts satisfy my
requirements:
Source Han (8 flavours, most of which are very similar)
TW-Kai (Cursive)
TW-Sung (Serif)
Unifont (3 flavours, bitmap)
WenQuanYi Micro Hei
WenQuanYi Micro Hei Mono
WenQuanYi Zen Hei
WenQuanYi Zen Hei Mono

(Commentary: Hei in Chinese typography roughly means 'Sans-serif', Kai
roughly means 'Cursive', Sung/FangSong/Song/Ming mean something like 'Serif'. All chinese
fonts themselves are mono-wide, so this parameter is inferred from the latin
part of the font.)

The choice is not very big, and is honestly even bigger if you can relax your
requirements. Mine were very strict, as I 

** Font parameters
*** Finding the subpixel order.
I went to http://www.lagom.nl/lcd-test/subpixel.php and found that my subpixel order is RGB.
*** Finding DPI.
xdpyinfo gave me wrong information. That is:
dimensions:    1366x768 pixels (361x203 millimeters)
resolution:    96x96 dots per inch

Whereas in reality my display is: 309x174 millimiters
That is 112x112 dots per inch (DPI)

I just measured that with a ruler.
Also, xrandr gives me the same information, that is, the dimensions are correct.

*** List of used fonts.
Main font: WenQuanYi Micro Hei Regular, size 16. Note that the font is scalable,
therefore 16 is obtained by scaling, not the basic font.
Secondary font: WenQuanYi Micro Hei Regular, size 8.
Mono font: WenQuanYi Micro Hei Mono Regular, size 16. The font is also
scalable, so the size 16 is fake.
Serif font: WenQuanYi Zen Hei Medium, size 16. The font is also scalable.

*** Where I actually tuned the font.
**** XFCE settings - Appearance -> Fonts
Default Font: WenQuanYi Micro Hei Regular 16
Default Monospace Font: WenQuanYi Micro Hei Mono Regular 16
Enable anti-alising: selected
Hinting: Full
Sub-pixel order: RGB
Custom DPI setting: selected, 112
**** XFCE settings - Window Manager
Title font: WenQuanYi Micro Hei Regular 8
**** CPU Frequency Monitor
Font: WenQuanYi Micro Hei Regular 8
**** Sensors Plugin Properties -> View
Show title: empty
UI style: text
Show labels: empty
Font size: x-small
Show units: empty
Small horizontal spacing: selected
**** Datetime (Orage?)
Date Font: WenQuanYi Micro Hei Regular 8
Time Font: WenQuanYi Micro Hei Regular 8
**** Generic Monitor
WenQuanYi Micro Hei Regular 8
**** Emacs
In .emacs:
(let
    ((font "-WQYF-WenQuanYi Micro Hei Mono-normal-normal-normal-*-*-*-*-*-*-0-iso10646-1"))
(set-face-attribute 'default nil :height 150 :font font))

Because the font is scalable, the size is not given as a font parameter, but
rather as a scaling :height property.
**** Firefox
***** about:preferences -> General -> Language and Appearance
Default font: WenQuanYi Micro Hei
Size 16
****** Advanced
******* Simplified Chinese
Proportional: Sans Serif
Size: 16
Serif: WenQuanYi Zen Hei
Sans-serif: WenQuanYi Micro Hei
Monospace: WenQuanYi Micro Hei Mono
Size: 16
Minimum font size: 16
******* Cyrillic
Proportional: Sans Serif
Size: 16
Serif: WenQuanYi Zen Hei
Sans-serif: WenQuanYi Micro Hei
Monospace: WenQuanYi Micro Hei Mono
Size: 16
Minimum font size: 16
******* Latin
Proportional: Sans Serif
Size: 16
Serif: WenQuanYi Zen Hei
Sans-serif: WenQuanYi Micro Hei
Monospace: WenQuanYi Micro Hei Mono
Size: 16
Minimum font size: 16
**** Skype
Increase scale until roughly fits the rest.
**** Java
Create a profile file: /etc/profile.d/z-jdk.sh:
#!/bin/sh
export _JAVA_OPTIONS="-Dsun.java2d.uiScale=1.35 -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
#"-Dswing.plaf.metal.controlFont=HanWangHeiLight -Dswing.plaf.metal.userFont=HanWangHeiLight"
#"-DWindows.controlFont=HanWangHeiLight-plain-15 -DWindows.menuFont=HanWangHeiLight-plain-16 -DPlastic.controlFont=HanWangHeiLight-plain-15 -DPlastic.menuFont=HanWangHeiLight-bold-15 -DGtk.controlFont=HanWangHeiLight-plain-15 -DGtk.menuFont=HanWangHeiLight-plain-16"
**** xfce4-terminal
Edit->Preferences->Appearance->Font
Use system font: selected
Increase Bash scale by C-= several times.
**** OmegaT
Open OmegaT, set font in preferences:
WenQuanYi Micro Hei Regular
Size 23 
Close OmegaT
Set java profile.
(Font tab will not be available, but the settings will stay.)

**** KDE
In systemsettings->appearance->style->applications->widget style->gtk+
If you don't have such a setting, install gtk-theme-engine.
The Fonts tab doesn't play a role if you have set a gtk theme.

Antialiasing: System Settings
DPI: Probably will also be inherited from the system

**** Wayland and Gnome
dconf-editor: org.gnome.desktop.interface.font-name

This worked in Dino, for example.
**** ~/.config/fontconfig/fonts.conf

     #+begin_src xml
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
<alias>
        <family>sans-serif</family>
        <prefer>
                <family>WenQuanYi Micro Hei Mono</family>
        </prefer>
</alias>
<match target="font">
  <edit mode="assign" name="size">
     <double>15.0</double>
  </edit>
</match>
    <match target="font">
        <edit mode="assign" name="rgba">
            <const>none</const>
        </edit>
    </match>
    <match target="font">
        <edit mode="assign" name="hinting">
            <bool>true</bool>
        </edit>
    </match>
    <match target="font">
        <edit mode="assign" name="hintstyle">
            <const>hintmedium</const>
        </edit>
    </match>
    <match target="font">
        <edit mode="assign" name="antialias">
            <bool>true</bool>
        </edit>
    </match>
    <dir>~/.fonts</dir>
    <dir>/home/lockywolf/.Fontmatrix/Activated</dir>
</fontconfig>

     #+end_src

* Color parameters calibration
TODO: I only know about LCMS.
*** Screen calibration.
*** Webcam calibration.
*** Second monitor calibration.

* Theme discussion
** General discussion
A theme essentially consists of the following items, which must be all in accord
for it to make sense. This document is primarily aimed at Slackware, but should also be applicable to other distributions (or maybe even other Unices), so ideally we should have two versions: with Slackware branding and without.

** Colours
Slackware's main colour pair is black-and-white, and the secondary is black-and-blue.
Human eye is overly sensitive to blue and green, so it's better to avoid these colours in general, except the elements that imperatively must be well noticeable.
An ideal colour combination would be 'orange-on-black', but we also don't want to seem extremely fringy. It is fine if the theme gives a feeling that 'the owner is a computing professional', but not that 'the owner is a rebellious kid' or 'the owner is a nostalgying old fag'.

*** Background
Black/dark grey

*** Foreground
Grey

*** Default text
White
*** Emphasis
Blue
** Vectorisation
It would be good if every element of the UI could be defined as a component of a font. This would provide an interesting property of every element obeying whatever is required from fonts, and would also guarantee consistency with existing typographic technologies. As a byproduct, it would give the system a slight look-and-feel of old text-based terminals in the best sense. Not as ugly elements, but rather as being able to rely on absolute and relative sizes.
However, this doesn't seem to be attainable at the current state of GUI in UNIX. What can be ensured, however is having every element defined as a vector graphics. This at least can be guaranteed for fonts, cursors and pictogramms (icons).

** Window decorations
Windowck-dark seems to do the job.
Blackbird is another suggestion, maybe better, because it also works for the colours.
*** Must be gray, so that they are clearly identified as a boundary between black and white regions.

*** Must be thin, because most of the window management is still done with a keyboard.

*** Must be thick enough to contain 8pt size font when needed.

** Icons
*** Must include all glyphs, and all must be distinct.
*** White on black, or, if possible, outlined in two colours.
HighContrast, a theme included in the default Slackware installation, is actually very good, _BUT_ as Xfce-Theme-Manager tells me, there is a slight issue with it. Some glyphs are actually identical.

** Splash screens
*** Bootloader
Slackwre includes a default login screen for Lilo.
Can it be reused?
*** Bootsplash
*** DM
*** Welcome screen
*** Desktop Background
Potentially, it may be a good idea to use home photos as a changing desktop background.
Otherwise, I'm visiting them too seldom.
*** Screensaver unlocking screen

** Mouse Cursor
*** Must be big, about 30pt size.
*** Must be available as both left and right handed versions.
*** Must leave some visibility to things under it. That is, it must be either semi-transparent, or must be inverting the colour under it in some sense.
*** Must have a sharp point that actually points to the point selected. Round edges are not precise enough.
*** I (Lockywolf) would prefer the one which is red, although blue would be a more obvious Slackware choice. Possibly, it should be blue in the branded version, and red in the non-branded version.

** Mouse wheel
*** One line scroll
*** Scroll in one direction (I prefer moving viewport)
*** Horizontal scrolling?
* Theme items:
** Xfce settings
*** Window decorations
Blackbird
*** Appearance settings
Blackbird. However, xfce 4.12 required several tunables, see a few tweaks:
https://github.com/lockywolf/Blackbird
**** TODO There is still a problem with non-displayed search bar in evince
*** Mouse Cursor
Crystal blue left (!)
*** Qt look and feel
*** XFCE login splash
*** Mouse settings
*** Icon Theme
HighContrast black-and-white theme
** xscreensaver
*** login screen
*** screensaver
** KDE settings
*** TODO
** Java
#+begin_src bash
cat > /etc/profile.d/z-lwf_jdk.sh <<EOF
export _JAVA_OPTIONS=""
export _JAVA_OPTIONS="$_JAVA_OPTIONS -Dsun.java2d.uiScale=1.35"
export _JAVA_OPTIONS="$_JAVA_OPTIONS -Dawt.useSystemAAFontSettings=on"
export _JAVA_OPTIONS="$_JAVA_OPTIONS -Dswing.aatext=true"
export _JAVA_OPTIONS="$_JAVA_OPTIONS -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
#"-Dswing.plaf.metal.controlFont=WenQuanYiMicroHei -Dswing.plaf.metal.userFont=WenQuanYiMicroHei"
#"-DWindows.controlFont=WenQuanYiMicroHei-16 -DWindows.menuFont=WenQuanYiMicroHei-16 -DPlastic.controlFont=WenQuanYiMicroHei -DPlastic.menuFont=WenQuanYiMicroHei"
EOF
#+end_src

** KDM
** eLilo
** bootsplash
** Firefox
** Chrome
** VIM 
*** Colours in console
*** Colours in GUI
** LibreOffice
** Qt5
There is a tool called qt5ct, which should do theme configuration for qt5
<2022-02-18 Fri 13:18> it does not work on wayland-qt, so I disabled it.
*** Appearance Style Fusion
*** Appearance Standard dialogs GTK3
*** Appearance Palette Custom
*** Appearance Color scheme darker
*** Fonts General WenQuanYi Micro Hei 16
*** Fonts Fixed width WenQuanYi Micro Hei Mono 16
*** Icon Theme HighContrast
** Emacs
*** Mouse cursor
*** Theme
*** TODO?
** Motif
To set a font, add the following to .Xresources or .Xdefaults
*renderTable: rt
*rt*fontType: FONT_IS_XFT
*rt*fontName: Micro Hei
*rt*fontSize: 16

** Thunar
** Hexchat
Settings->Preferences->Interface->Appearance->Font->Browse
WenQuanYi Micro Hei Mono 16
** conky 
file://~/.config/conky/conky.conf
    font = 'WenQuanYi Micro Hei:size=14',
-- Shades of Gray
	color1 = '#DDDDDD',
	color2 = '#AAAAAA',
	color3 = '#888888',
-- Orange
	color4 = '#EF5A29',
-- Green
	color5 = '5757F3',
-- Red
	color6 = '#D61D1D' 

** Midnight Commander
** Wine
** Matlab
** Mumble
Configure->Settings->User Interface->Theme: None
** Skype
Tools->Settings->Appearance
Colour: black
Mode: Use system settings (or Dark or High contrast dark)
Enable compact list mode: yes
Show user and conversation items: yes
** Adobe Reader

* References
** Fonts
 - [[http://epsi-rns.github.io/desktop/2017/04/13/getting-xlfd-font.html][An interesting article on XLFD]]
 - [[https://wiki.archlinux.org/index.php/X_Logical_Font_Description][Arch Linux on XLFD]]

** Themes 

** Colour calibration
