* This file is a quick guide on how to improve responsiveness in Linux.

On my machine, there were two problems with responsiveness.

** Too much memory eaten by Firefox.

We should limit Firefox to 4Gb of RAM and 3 cpus.

Something like prlimit should help us.

I wrote a sketch of scripts/firefox, which does prlimit.

Restrict firefox processes 

** Swapping of Xorg and other gui apps.

** Overall responsiveness must be improved.

http://www.webupd8.org/2010/11/alternative-to-200-lines-kernel-patch.html

This link is very old. We need a new manual with cgroups2.
