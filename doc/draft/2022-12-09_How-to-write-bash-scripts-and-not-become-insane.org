# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2022-12-09 14:22:15 lockywolf>
#+title: How to write bash scripts for system automation and save effort.
#+author: lockywolf
#+date:
#+created: <2022-12-09 Fri 11:09>
#+refiled:
#+language: en
#+category: howto
#+filetags: :howto:bash:linux:programming,
#+creator: Emacs/org-mode

In this howto I am trying to summarise some tricks that I found useful when writing
scripts.

I do not pretend that my knowledge of bash is any good, I am, rather, trying to sum u up human errors that I repeatedly make when writing scripts for home automation.

* TODO Body

** Input and output

*** STDIO
STDOUT is for debugging information.
STDERR is for critical messages that require human intervention.

*** Append or overwrite log files?



** Decent logging

$0 is not very stable, but in your own scripts, try making sure that it is reasonable.

#+begin_src shell :exports both :results output :eval no
l_s=$(basename "$0")
l_scriptname=$(printf '%s' "$l_s" | sed -E 's|[^[:alnum:]._]/||g')
l_script_with_param=$l_scriptname
for param in "${@}" ; do
    p=$(printf '%s' "$param" | sed -E 's|[^[:alnum:]._]/||g')
    l_script_with_param="$l_script_with_param"_"$p"
done
function mylog
(   printf "%s:%s:" "$(date --iso=seconds)" "$l_scriptname_with_param"
    printf "$@"
)
LOGFILE="/var/log/lockywolf/$l_script_with_param.log"
mkdir -p "$(dirname "$LOGFILE")"
#+end_src

** Running from dcron



** Option parsing

