# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2021-01-01 15:54:44 lockywolf>
#+title: How to make LibreOffice suck less.
#+author: lockywolf
#+date: 
#+created: <2021-01-01 Fri 15:42>
#+language: hx
#+category: howtos
#+filetags: :computers:programming:information:humanities
#+creator: Emacs 27.1/org-mode 9.4


LibreOffice is bad.

Although it has a long history, as StarOffice initially, and later as OpenOffice, and even later as LibreOffice, its development has been mostly chaotic.
This, as well as the fact that people already have certain expectations (the worst enemy of a programmer) of how office packages are expected to work, has given LibreOffice a certain notoriety that is these days hard to dispel.

Those of us who are programmers, no doubt pay a huge tribute to LibreOffice developers, who have managed to make such a giant piece of software available for free.
Indeed, even though the infamy of LibreOffice is earned, nevertheless it is _the_ office package of UNIX, and in any case serves a colossal service to the world of document interoperability.

However, this enormous respect we, the programmers, pay to LibreOffice, somehow dims our vision and prevents us from accepting LibreOffice's drawbacks and seeing its flaws.

This document is intended to become (subject to time restrictions and the amount of office work performed by the author of these lines) a short-cut that attempts to reconcile the programmers view on the subject with that of the person who has to do the work using LibreOffice involuntarily.

* TODO Body

** Add your name and organisation to the LibreOffice settings.

Seriously, do if as soon as you start using the package.
Every bit of context helps as you open the files later on.

Tools->Options->User Data

** Add your GPG encryption/signing key.

Tools->Options->User Data

If you do not have one, make one, but be very careful.
GPG is an insanely tricky business.


