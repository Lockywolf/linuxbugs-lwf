#+BLOG: wordpress
#+POSTID: 977
# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2024-08-02 16:11:20 lockywolf>
#+title: Review for "Educated" by Tara Westover.
#+author: lockywolf
#+date: <2024-07-29 Mon 12:37>
#+created: <2024-06-16 Sun 14:48:05>
#+refiled:
#+language: hx
#+category: review, 
#+filetags: :literature:english:psychology:phd:education:science:love
#+creator: Emacs-30.0.50/org-mode-9.7-pre



[[file:Educated_(Tara_Westover).png]]

Due to various technical difficulties I had to write this essay twice.
This is the second version.

@@wp:<!--more-->@@


* Review

** Background

This book has two aspects that may render it interesting to the general public: describing an experience of living with a bipolar disorder, and a 21-th century version of a Bildungsroman.

In some sense it is a book about a girl getting far out of her initial social stratum by getting a good education level, and reflecting on her own experience.

On the other hand this is a book about spirits deeper than conscious (and older than the trees) dominating our brain.

** Synopsis

The plot is happening in our time, roughly since 1990 until 2017.
A girl is growing in rural Idaho in the USA.
Her family is Mormon, as well as everyone in the nearest town, therefore she has a lot of brothers and sisters.

They are religious, hard-working, and on the surface distrustful of the government.
Therefore she, as well as everyone in her family, is not allowed to go to a school or to a doctor.

This rule is not rigourously enforced however.
Or should I say, it is not enforced systematically.
When some members of the family decide to leave, they end up doing that.
Her brothers eventually do, just as she does.

And the more education they are getting, the more distanced they become from the core of the family, eventually ending up in something I would call a "divorce", if people could divorce their relatives.
That is, they end up never almost talking after she gets her PhD in history.
And although she wants a reconciliation, the issue is not resolved by the end of the book.

Throughout the rest of this review I will try to express my feelings about different phenomena depicted (with great colour) throughout this book, and how they relate to either my life, or lives of people around me.

It is noteworthy that this book is perhaps the first one I have read about people roughly my age, so I should have quite a lot to reflect upon, and indeed I do.

** Money

The thought that is chasing me all over this book is just how much different from my own experience is the attitude to money they have in America.

It is true that one's monetary worth is not exactly proportional to how smart or how educated one is, but in this book this inconsistency is just outright baffling.

I should say it in other words: in Russia being honestly hardworking almost never brings you money and success.
This is not to say that Russia is totally anti-meritocratic, Russia has both a meritocratic and a corrupt way of attaining wealth and power.
But in Russia if you are rich, you are either a social engineer, skilful in climbing the social ladder by fair and unfair means, or you are a well-educated, highly proficient "engineer" (not necessarily a scientific engineer) that the socially-aware people use as a tool in their cunning schemes.

In the book the opposite is happening.
No matter how high in the education ladder the protagonist is climbing (and eventually becoming self-sustainable), her initially poor, ignorant, and mis-informed family ends up more affluent and powerful than her.
Is this the American Dream coming true?

Seriously, I started reading the book with the feeling that this is going to be a "break away from poverty with education" type of book that we have so plentiful in Europe.
But this is not the case, these people, despite their provincial culture, are not poor at all.
Her father is buying industrial heavy machinery: forklifts, harrows, man-baskets.
She is driving a car since 16 or even 14.

She is working in a supermarket, and for her father in his business, and he is paying her (!) enough money to pay for that extremely expensive American education.
Gees, her father, the one she calls a tyrant and an eccentric is actually paying her.
An unimaginable thing in Russia, where in patriarchal families the children are obliged to give their salaries to the head of the family.

** Control

Admittedly was not writing the book about money.
For her the book was really about emotions and about control.

A very American thing, even though filial piety and family domination very well exist in Russia, Europe, and China.
But it is really the Americans who I have seen contemplating about this more than anyone else.
I have an acquaintance in China, a former American, who is feeling much freer in the relatively restrictive Chinese society than back in America, because he is gay.
And despite all the American march towards tolerance and acceptance he feels himself better here, where nobody really gives even a little shit about who he sleeps with.

In Russia there are quite a lot of dominating patriarchal families with tyrant fathers/mothers who terrorise their children, but "getting out into the city" is usually the ultimate solution.
People very seldom go back to the villages, and remote control via shame, guilt, does not really work in Russia.
Russia is, after all, really about violence more than anything else, and it is hard to project violence over the distance.

Church and religion play a big role in the story.
I still have not fully understood the attitude of the author towards the Church, but by the objective criteria, the Church is what has shown her the way to freedom.
This is very painful to read for a Russian, for whom the Church is another way of human oppression.
The devout Orthodox attain Church responsibilities _in addition_ to the filial ones, not as a substitute.
For her the Church was the way out, the method whereby she learns about the outer world, about the university, while singing in the choir.
The Church helps her with a sick tooth, the Church helps her with a grant application.

Her father tries to control her via the money needed for her tuition, but eventually fails.

** Chastity

Mormons officially recognise polygamy.
Arthur Conan Doyle satirised them for that even before the 20th century.
There is just one chapter where the author discusses polygamy, but the inconsistencies in the attitude to women and her in particular is weaved throughout the book.
She is depicting women who are weak and unable to resist men, such as herself and her elder brother, but at the same time it is her mother who ends up ruling in the family when her herbalist business goes up.

She struggles with her relationship with men, with other women, with her father, with "city life", and even with herself.
She wants to look good so that her boyfriend would like her, but she also looks down at other women who want to be admired by men, and dress "frivolously".

She lays the burden of this hypocrisy on her father, who seemingly "practised what others just believed", and I would have agreed to her if I didn't know my relatives, who, obviously, grew in unrelated circumstances and conditions.
They are neither Mormon, nor even a christian.
Nevertheless, they are very shy and easily embarrassed when confronted with the physical part of the world.
Once I heard an opinion that skirts are intentionally designed in such a way that would make it easier for men to assault women.
On the other hand on the other hand I remeber hearing that trousers are improper for women because of being men's clothing.
I have no idea how they live in a real world, but I remember being asked me several times about how girls in my school had dressed, and being never quite satisfied with my answer.

Not exactly related to that, but I spotted a few things that time and again appear in the women's writing trying to reflect on the differences between men and women.

One more example is her saying that "you are not defined by your clothing".
I have heard that many times from women as a revelation (Zhu JingNing is writing about this in her book about making business female-way), even though for men this has never been a secret.
Actually spending conscious effort choosing clothing for the very small advantage it might be giving in a working environment might be worth it.

I do understand what she is writing about when she is speaking about "not having a right to be here", but I should really speak about this in the next section.

** Bipolar disorder and Medical Establishment

She thinks that her father has a bipolar disorder.

This might very well be true.

However, bipolar disorder is a hereditary disease, and in fact cannot really be called a disease, because it is not necessarily debilitating.
Throughout reading this book I really had a feeling that she has a bipolar disorder herself, just as all of her family.

After all, she is talented.
I should be writing this in a different section, but the book is written in a wonderful, colourful and vivid language.
She managed to study on all As, even when having a sick tooth and prepare herself for a university while never even attending a school.
She is a genetic genius.
And while this all are great traits, it somehow discounts her accounts of her life.

No, I am not saying that she is lying.
I am saying that we are all influenced by our genes more than we are willing to admit.

The same animalistic stubborness and headstrongness that she is so strongly disapproving in her father is what actually let her enter a university without any school education, and what let her other brother become a Purdue PhD.

The book has a lot of stories about their family disregarding healthcare and relying on improvised treatment methods.
And since most of them are probably useless, we just have to admit that humans are way-way more durable than we can imagine.
They are getting in car crashes, falling from heights, getting burned alive, have brain injuries and holes in theirs skulls, and still survive.

I cannot, on the other hand, say that everything her father is saying about "medical establishment" is wrong.
Conventional medicine is full of fraud, overprescription, and plain neglect on the side of the medical providers.
It is true that their herbalist approach is in no way a substitute, and the fact that it is it that made her family rich at the end is making me lose my hope in humanity.

Use your own brain, that is the only thing I can recommend to my readers.

** Mores of the age

We often do things that are unimaginably stupid to other people, and even to ourselves later in life.

One of the first chapters of the book contains a description of her father preparing for the End of Days, and one of the latest chapters contains a description of the family using their substantive wealth to build their own chapel, and generally extend their 4-bedroom cottage into a well-defended castle.

Is it a stupid thing to do?
Maybe it is.

But I am writing these lines while the code for my own Android flavour is being compiled on my laptop for my "new" phone.
I had bought this phone about New Year, and I still haven't managed to migrate to it.
Why?
Because it very heavily relies on the "Cloud" "AI" services "provided" by the OPPO company.
Supposedly for free.
But nothing in this world is free.
A "cloud" is just another man's computer.
Can you trust this man?
Maybe you actually can.
Until you are a famous politician and a businessman, and some of the employee of that company is sending you an email with your naked photos and demanding a ransome.

But this feeling of being left at the mercy of the "Leviathan" is just horrible.

Make no mistake, this "self-reliance" that her father is vehemently defending, and which is nowadays called "conservatism", is in reality neither old nor conservative.
People have never lived autonomous lives, people have always been collective species.
Now her father being able to say "No" to the collective, and not just the national collective, but even to the rural community is a harbinger of the future, not the past.
Not all of the future, just a part of the future, and maybe even not the brightest part of the future.
But some of it.

(Make no mistake, future has collectivism in it too, just remember how much of the COVID isolation has been imposed onto us in 2021.)

And this self-reliance that looks so hilarious and backwards to use now, in fact relies on so many things that the progress has given to us in the 20th century.
I have to repeat myself, her father could afford buying heavy machinery by himself, as an individual entrepreneur.
My grandpa only managed to buy a single rototiller by the end of his life.

And although I, myself, resemble her much more than her father, and I understand why she loathes that stronghead and limited world of the father, I can't stop thinking that there is something indomitable in that independent attitude that "nothing can save you except yourself".

** Having children

There is one more thing I need to write into this review, and perhaps into this section, for the lack of a better one.

Modern life and this book is largely about the fight of conservatism and progressivism, has lead to the reality in which children are unable to properly inherit the things that their parents owned.

They can't do that because their parents are still alive.

Britain has left this impression in particular on me -- a country of old people.

So what used to be natural for people for thousands of years, of children defeating their parents in a symbolic combat, and inheriting the world after them, is not here anymore.

This book gives good account of it.
The real, basic, fundamental issue that she had with her father was that her father was still alive when she got her PhD, a thing hardly imaginable during most of the human history.

We are already old, but our parents are still alive.

One of the signs of the abnormality of the world we are living in is the fact that although sh managed to defeat her father, by all accounts, by earning a PhD, and by making money on her own, and by refusing his control, she still have not managed to bear the fruit of her victory.
People live long now.

And all that despite the disregard for government's healthcare that her family exhibited.

But her parents had many children (and most of her relatives).
She had many aunts, to whom she could reach in the search of help, she had brothers and sisters, some of which abandoned her, and some, instead, sided with her in a family conflict.

This book made me thinks that having many brothers and sisters was what actually liberated her from a lot of tyranny from her family.
Her family just did not have enough time and attention to oppress many kids, so some freedom could slip through the fingers.
I have seen the opposite too many a time, even among my classmates, who, even though they lived with "progressive" and "liberal" parents, were so dominated by them that they never grew up.
As this book shows, manage to grow up by themselves, given that you don't fuck them up too much.

** Inconsistencies

Although this might not be evident, this book is political.

The everlasting American struggle between the NIMBY rednecks and socialist city-dwellers is visible like in few other books.

I could not deny that the book is written in a compelling manner, and I can reflect on what she is saying.

But I have not heard the other side of the story, from her other relatives.
She accuses her brother of something, but after all he manages to contain himself in almost all circumstances.
Poor dog though.
Do not get me wrong, he probably does need help, and being a rural Mormon is not an excuse to be a manipulative abuser.

But sometimes she just writes things that almost seem like the "Faith creed", for example getting vaccinated in England.
As if to prove to herself that she now believes in a different faith, different to that of her father.
Or saying that she didn't feel anything in the Sacred Groove.
After all, she has been a Mormon for a long time, and that Groove is significant as a cultural place too.
Seems quite like changing a faith rather than becoming faith-free, which, again, is a very American thing to do.

** Writing style and conclusion

At the very end of this chaotic and inconsistent essay I want to write about the book's qualities as a piece of English literature.
The book is great -- vivid, colourful, very smooth, flowing into the brain like a stream of ambrosia.

The images of people she is depicting are convex, believable, easy to imagine and visualise.
She is very talented and I would probably really enjoy reading her other works too.


This book made me learn a lot of names for herbs, agricultural and construction tools, nature, and animals.

Would I recommend reading this book to anyone?
Yes, I will.
In some sense it is a good book about growing up in a "new world".

It is a little bit like "Harry Potter".
Not too much, but it is certainly within that "Bildungsroman" that became so iconic in the British culture.


* Contact

- Telegram :: http://t.me/unobvious
- GitLab ::   http://gitlab.com/lockywolf
- PayPal :: https://paypal.me/independentresearch
- WordPress :: https://lockywolf.wordpress.com

# Educated_(Tara_Westover).png http://lockywolf.files.wordpress.com/2024/07/educated_tara_westover.png
