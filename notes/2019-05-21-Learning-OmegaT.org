# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2020-10-27 18:54:35 lockywolf>
#+created: 2019-05-21T06:02:37
#+title: Learning OmegaT.

* Questions that appeared while learning OmegaT.
*** What is the unicode name for a dash?
*** How to type a dash in a standard Linux keymap?
*** How to type a dash in scim?
*** How to type a dash in UNICODE?
*** How to use compose-key to type in a dash?
*** What is 'Fuzzy Matches'?


* Cheatsheet
** Hotkeys
C-d -- generate a translated document.
C-M-v -- validate tags.
C-e -- project settings.
C-l -- see project files.
C-u -- move to the next segment.
C-g -- add an item to glossary.
C-f -- search.

* Suggestions (bugreports)
** Edit Project (C-e)
*** Add a hover hint to Enable Sentense-level Segmenting
*** Add a hover hint to Auto-propagation of Translations.
*** Add a hover hint to Remove Tags.

* Emacs Questions that appeared while learning OmegaT.
*** How can I efficiently look for character names and synonyms?
*** How does blackboard-bold-mode work?
*** How does the charmap package work?
