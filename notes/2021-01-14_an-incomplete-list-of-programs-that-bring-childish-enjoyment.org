# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2023-03-02 17:31:45 lockywolf>
#+title: An incomplete list of programs that bring childish enjoyment and the feeling of almightiness.
#+author: lockywolf
#+date: unpublished
#+created: <2021-01-14 Thu 22:21>
#+language: hx
#+category: programming
#+filetags: :computers:programming:philosophy
#+creator: Emacs 27.1/org-mode 9.4


Some programs become mundane five minutes after they are installed.
For example, ICQ, or any other messaging system.
They look like they have always been there even if they are brand new.

Others may have been out for ages, but still leave you with a feeling of awe and fun.
This document lists a subset of the second kind.

* GNU Screen / tmux
* rmlint
* locate
* antiword/antiwordxp.rb
* xpra
* jq
