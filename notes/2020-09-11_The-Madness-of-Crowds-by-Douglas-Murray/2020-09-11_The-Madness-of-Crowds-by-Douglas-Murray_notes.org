# -*- mode:org; eval: (visual-line-mode) -*-
# Time-stamp: <2023-12-27 15:02:59 lockywolf>
#+date: <2020-09-11 Fri 15:35>
#+author: lockywolf gmail.com
#+title: A notes file for reading "The Madness of Crowds" by Douglas Murray

* Notes

** we have been living through a period of more than a quarter of a century in which all our grand narratives have collapsed

** It was inevitable that some _pitch would be made for_ the deserted ground.

** The Royal College of Psychiatrists considers that sexual orientation is determined by a combination of biological and postnatal environment factors.’

‘There is no evidence to go beyond this and impute any kind of choice
into the origins of sexual orientation.’

It is not the case that sexual orientation is immutable or might not
vary to some extent in a person’s life. Nevertheless, sexual
orientation for most people seems to be set around a point that is
largely heterosexual or homosexual. Bisexual people may have a degree
of choice in terms of sexual expression in which they can focus on
their heterosexual or homosexual side. It is also the case that for
people who are unhappy about their sexual orientation – whether
heterosexual, homosexual or bisexual – there may be grounds for
exploring therapeutic options to help them live more comfortably with
it, reduce their distress and reach a greater degree of acceptance of
their sexual orientation.

** American Psychiatrist Association: kinda same thought: homosexuality is something totally obscure.

There is no consensus among scientists about the exact reasons that an
individual develops a heterosexual, bisexual, gay or lesbian
orientation. Although much research has examined the possible genetic,
hormonal, developmental, social and cultural influences on sexual
orientation, no findings have emerged that permit scientists to
conclude that sexual orientation is determined by any particular
factor or factors. Many think that nature and nurture both play
complex roles; most people experience little or no sense of choice
about their sexual orientation.

** Douglas is speaking about the "Hardware vs. Software" issue. 

This debate is very close to myself. 
My life in Edinburgh was incredibly miserable.
Why?
I am an "industrious man". 
I love what I do.
I love learning.
I love research.
I love trying new things and I like trying foreign cultures.

Why did I feel so horrible, miserable and inefficient in Edinburgh?
Why is it that I only managed to switch to the production mode in Russia, and even more in China?

Is it hardware or software?

It seems to me that "nurture" and "environment" cannot be classified as purely "hardware" or software.
"Firmware" perhaps?
I am not sure though.

** Women want to know what it is that men are after, what they want and what – if anything – they might be feeling during the act of sex. These questions are a staple of conversation between friends and a source of unbelievable private concern and angst at some stage (sometimes all) of most people’s lives from adolescence onwards.

** If there is any one thing in society that gets even close to matching the confusion and angst of women about men, it is of course the list of questions which men have about women. The subject of nearly all dramatic comedy is the inability of men to understand women. What are they thinking? What do they want? Why is it so hard to read their actions? Why does each sex expect the other to be able to decode their words, actions and silences, when no member of the opposite sex has ever been given a decoding manual for the opposite sex?

** All women have something that heterosexual men want. They are
holders, and wielders, of a kind of magic. But here is the thing: gays
appear in some way to be in on the secret. That may be liberating for
some people. Some women will always enjoy talking with gay men
about the problems – including the sexual problems – of men. Just as
some straight men will always enjoy having this vaguely bilingual friend
who might help them learn the other language. But there are other
people for whom it will always be unnerving. Because for them gays will
always be the people – especially the men – _who know too much_.

** The utility of such groups is obvious: their ‘highly diverse struggles:
urban, ecological, anti-authoritarian, anti-institutional, feminist, anti-
racist, ethnic, regional or that of sexual minorities’ give purpose and
drive to a socialist movement that needs new energy. What is more,
unless they cohere together these groups might just pursue their own
agendas and their own needs. What is needed is to bring all these
movements under one umbrella: the umbrella of the socialist struggle.

** Laclau and Mouffe write of ‘what interests us about these new social
movements’ and explain how it ‘leads us to conceive these
movements as an extension of the democratic revolution to a whole
new series of social relations. As for their novelty, that is conferred
upon them by the fact that they call into question new forms of
subordination.’

** An example of a shitty sentence.

The move from a structuralist account in which capital is understood
to structure social relations in relatively homologous ways to a view
of hegemony in which power relations are subject to repetition,
convergence, and rearticulation brought the question of temporality
into the thinking of structure, and marked a shift from a form of
Althusserian theory that takes structural tonalities as theoretical
objects to one in which the insights into the contingent possibility
of structure inaugurate a renewed conception of hegemony as bound up
with the contingent sites and strategies of the rearticulation of
power.

** At which point Peterson explained to him that the purpose of putting on lipstick and rouge is to simulate sexual arousal.

Not the main point though.
The main one is to get _power_.

** an example of people deliberately and lazily adopting simplified misrepresentations of
what other people are saying in order to avoid the difficult discussion
that would otherwise have to take place

** The confusion that Nicki Minaj acts out here is representative of a
whole host of other things in our culture. It contains an unresolvable
challenge and an impossible demand. The demand is that a woman
must be able to lap-dance before, drape herself around and wiggle her
ass in the face of any man she likes. She can make him drool. But if that
man puts even one hand on the woman then she can change the game
completely. She can go from stripper to mother superior in a heartbeat.
She can go from ‘Look at my butt, waving in front of your face’ to
‘How dare you think you can touch the butt I’ve been waving in front
of your face all this time.’ And it is he who must learn that he is in the
wrong.


** traditional masculinity – marked by stoicism, competitiveness, dominance and aggression, is undermining men’s well-being’

** The survey found that only 9
per cent of British women used the word ‘feminist’ to describe
themselves. Only 4 per cent of men did. The vast majority of people
surveyed supported gender equality. In fact a larger number of men
than women supported equality between the sexes (86 per cent versus
74 per cent). But the vast majority also resisted the ‘feminist’ label.

** There is little enough recourse when old school journalism tramples across someone’s life. But on the internet there is not even a regulatory body to appeal to if your life has been raked over in this way.

Why would this be such a thing? 
Did this fired Google guy never find a new job? 
I believe he did.

** Okay, the "students" protest and do not want to hear from some "figures". Who cares? Who gives them a right to decide? A university fears losing the money the "students" pay to them? Or what?


** Why cannot we still buy some robot that would grow us food at the window?

Like, I do not eat that much stuff.
Could I just buy a robot shelf, stock it with fertilisers, connect to a wire, and be done with it?
It may even be outside of the city, and would send me food with a drone.

** I need some sort of an agreeableness ... testing? Sometimes I am able to argue. Sometimes not. Why? 

** Is it really necessary to lie in a mating game? I presume, it should be possible to make an alliance with your wife after you have been together for a while, but at first?

** I need to record, otherwise I will forget it. I had a conversation with Olga Vorobiova (Vorobeika) about the studies of LARP, in Denna's chat. And I told her an important thought: not everything can be investigated. Many things are just bound to be left unknown forever. Not because people are malevolent, rather because things get ... dissolved into the substrate too fast. Because entropy doesn't stop. Denying it is just not understanding physics.

** highly politicized people are willing to interpret even extreme remarks from their own political tribe in a generous and forgiving light while reading the remarks of those in any opposing camp in as negative and hostile a light as possible

** On his travels in America in the 1830s, Alexis de Tocqueville noticed the significance of assembly in the United States – specifically that face-to-face meetings of the citizenry allowed them to remedy problems often before any other authority was needed.

* Summary

** So far I have found several things that Murray constantly ignores. 

- The unimaginable hardness to fit your own standards in the world where everyone sees everyone. Part of this madness is due to the fact that in the past everyone could carve for himself a place where he could be a "king". That was a family and a circle of friends. Now you have the whole world to compete with. 
- The hardly imaginable hardness to reach any decent level of proficiency in anything. Too many academics know nothing, but are still academics. And the whole life is not enough to learn what is required for an academic to be an academic. And the competition for the positions in fields that are less complicated is billions per place.
- The thing with discussing the "trans" issue is that many people don't care about it. One of the things with "public discussions" is that they do not represent anyone. 

<2020-09-13 Sun 22:00> I have finished the book. It left me thinking.
I do now really understand whether this book leaves the feeling of optimism or pessimism.
It does leave the feeling that learning social sciences, and more importantly, learning how to ... be an adult. Learn things that adults do and how real adults make decision by books and by choosing a referential group.

Learning Chinese in a proper way is becoming more and more urgent.
Reading Derrida, Foucault, Chomsky and other people who contributed to confusion a lot is also important.
And perhaps the opponents deserve more attention than friends.
Keep your friends close, and your enemies closer.

Gays are dangerous beasts.
A couple of gays defeats a heterosexual couple at almost anything easily.
(As the swan example indicates vividly.)

I do have a female-ish component that needs to be addressed somehow.
Is there a cheap cheaty way out of it? 
Without much effort?
Perhaps just some male beauty services?

I guess, I will not publish a review of this book.

Twitter culture is very, very specific.
Be careful about it.

* Words

1. dementing :: in political, societal context --- forgetting
2. nicked the tripwire :: to nick mean touch lightly
3. put yourself beyond the pale ::  "beyond the pale" means to be outside the bounds of acceptable behavior, morality, or propriety (originally the Pale was the fence between Ireland under English rule and the rest of Ireland)
4. shift mores :: "mores" refers to the customs, norms, and behaviors that are accepted as standard or conventional
5. to strew :: means to scatter or spread things untidily over a surface or area
6. fraught :: full of tensions, stress, and anxiety, usually about some social situation
7. avowals :: open and honest statement affirming something (see vow)
8. volubly :: speaking fluently, quickly, or readily
9. demeaning ::  behavior or treatment that is disrespectful or degrading
10. unfurl :: unroll or unfold something, like a flag of a scroll
11. punters :: British English for "customers"
12. unflustered :: opposite of "flustered", who is nervous, agitated, and confused
13. whacky-backy :: British English for marijuana
14. Jewess :: an outdated, slightly derogatory term for a Jewish lady (Russian жидовка)
15. tribulations :: severe trials or sufferings; difficulties or troubles.
16. snigger :: suppressed or partly concealed laugh, with a sense of superiority
17. twitchy :: making small, jerky movements, often involuntary
18. wracking :: torturous and painful for the brain, especially nerve-wracking
19. scorn :: contempt or disdain for someone or something unworthy or inferior
20. confrères :: colleagues/peers in professional context
21. forbearance ::  refraining from exercising a right, enforcing a penalty, or insisting upon an obligation, magnanimity, generosity
22. kick up :: start up or initiate something, usually some resistance or disturbance
23. opprobrium :: harsh criticism or public disgrace arising from shameful conduct, such as when the journalists write something bad about someone
24. whiplash :: an injury to the neck, due to a sudden change of movement, such as in a car, also figuratively, a sudden change in society "cultural whiplash"
25. hitherto :: "up to this (that) point" (things explained hitherto)
26. tedium :: feeling or state of being bored and doing something tedious
27. mores of the age :: customs, behaviours, and values of an era
28. to be left in the wake :: to be in the middle of the consequences of something (say, a hurricane), ("wake" is a trail of waves after a ship has gone)
29. quibbles :: minor objections, complaints, and criticism. "She never outright rejected my offer of a date, but always quibbled about particular circumstances, so we never met again."
30. dowdy :: unfashionable and lacking in style. (Like my wardrobe.)
31. pulpit :: raised lectern in a church, also used figuratively as an "opportunity to give a sermon"
32. to tout :: promote something vehemently, like street vendors
33. knock-on :: secondary, indirect consequence, side-effect, "the knock-on effect of having a PhD is being admitted to a private chamber in the Lenin Library"
34. emblazoned :: decorated with a bright, attention-seeking design, "my laptop is emblazoned with a Matrix code on it"
35. asinine :: extremely foolish, stupid (lat. donkey-like) "He made asinine remarks and presented himself as a fool."
36. equanimity :: The state of calmness and composure. "Before a dangerous endeavour he maintained his equanimity."
37. imbibe :: drink alcohol, used figuratively as "absorb knowledge and ideas"
38. assail :: The word "assail" means to attack vigorously or violently; to assault. "The castle was assailed by the enemy forces, who launched a fierce attack at dawn." "The politician's character was assailed by his opponents during the heated debate."
39. gobbledygook :: "Gobbledygook" refers to language that is nonsensical, confusing, or overly complicated, often to the point of being incomprehensible. It's typically used to describe speech or writing that uses a lot of technical jargon, bureaucratic language, or convoluted phrasing, making it difficult for the average person to understand. 
40. smattering :: "Smattering" refers to a superficial, slight, or scattered amount of knowledge or information about something. It implies a basic or limited understanding, often just enough to be familiar with a topic but not in-depth. The term is commonly used when someone knows a little bit about various subjects but not to a great extent in any of them.
41. rife with :: The phrase "rife with" means full of or abundant in something, often used to refer to negative or undesirable qualities or conditions. When you describe a situation, place, or thing as being "rife with" something, it suggests that the characteristic is widespread, prevalent, or noticeably common within it. For example: "The old house was rife with mold," indicating that the mold was widespread throughout the house. "The company's culture is rife with corruption," suggesting that corrupt practices are common and pervasive within the company.
42. Slink :: The word "slink" refers to moving in a smooth, quiet manner, typically in a way that is intended to go unnoticed. Often, "slink" implies a sense of stealth or surreptitiousness. It's commonly used to describe someone or something moving in a furtive or sneaky way. For instance, you might say, "The cat slinked through the room, trying not to attract attention," to describe the cat moving quietly and carefully, trying to be as inconspicuous as possible.
43. midriff :: "Midriff" refers to the region of the human body between the chest and the waist. It's the area of the torso that is exposed when wearing clothing such as crop tops or low-rise pants. The term can be used in both anatomical contexts to describe this part of the body, as well as in fashion contexts to refer to styles of clothing that reveal this area. "Show his midriff."
44. segue :: "Segue" is a term used to describe a smooth, uninterrupted transition from one thing to another. It originally comes from music and performance, where it denotes moving from one part of a performance to another without a break. The word has been adopted more broadly to refer to any kind of smooth transition in conversation, narrative, or different sections of a work. For example, in conversation, when someone changes the topic smoothly and naturally, they are said to "segue" into a new topic. In a TV show or movie, a "segue" might be a smooth transition from one scene to another that maintains the flow of the narrative.
45. witter :: "Witter" is a somewhat less common word, primarily used in British English, that means to chatter or babble pointlessly or at unnecessary length. When someone is described as "wittering," it implies that they are talking a lot without saying anything particularly important or interesting, often in a somewhat nervous or agitated manner. For example, if someone says, "He kept wittering on about his vacation plans," it suggests that the person was talking excessively and perhaps tiresomely about their plans.

46. racked with confusion ::
47. umpteenth ::
48. lip-service ::
49. tut-tut ::
50. mangy-looking ::
51. let-off ::
52. risible. ::
53. ineluctably ::
54. leg-up ::
55. heave-some ::
56. tenuous ::

57. discern ::
58. compunction ::
59. on the cusp ::
60. pivoted on a dime ::
61. surreptitiously ::
62. anti-miscegenation ::
63. imbroglio ::
64. peroration ::
65. regurgitate ::
66. foment ::

67. atone ::
68. lampoon ::
69. umpire ::
70. condescension ::
71. fad ::
72. encroaching ::
73. castigated ::
74. dalliances ::
75. ire ::
76. surmised ::

77. amped off ::
78. mushing ::
79. plaudits ::
80. indignant ::
81. porcine ::
82. ordure ::
83. slurry ::
84. to tar ::
85. crop up ::
86. abasement ::

87. schadenfreude ::
88. quagmire ::
89. recourse ::
90. frailty ::
91. rumbustious ::
92. sidles ::
93. qualms ::
94. transpire ::
95. aggravation ::
96. flay ::

97. opprobrium ::
98. pugilist ::
99. flicking ::
100. pouting ::
101. childhood haunts ::
102. stand-in ::
103. flaunt ::
104. adumbrate ::
105. unsnap a onesie ::
106. doozy ::

107. cowed by ::
108. out of line ::
109. vying ::
110. derange ::
111. animosity ::
112. cuttlefish ::
113. prelapsarian ::
114. coy ::
115. nixed ::

116. frailties ::
117. unruffled :: 

