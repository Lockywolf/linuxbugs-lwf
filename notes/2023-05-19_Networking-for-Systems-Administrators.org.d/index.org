#+BLOG: wordpress
#+POSTID: 540
#+DATE: [2023-05-19 Fri 23:01]
# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2025-02-08 13:08:59 lockywolf>
#+title: Networking for Systems Administrators by Michael W Lucas, a review.
#+author: lockywolf
#+created: <2023-05-19 Fri 21:43>
#+refiled:
#+language: en
#+category: review
#+filetags: :programming:admin:linux:openbsd:windows:software:networking
#+creator: GNU Emacs 30.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2023-03-24

[[file:01_cover_networking-for-systems-administrators.jpg]]

This book review is going to be really short, well, partly because the book itself
is short. However, short here does not imply lack of value. I have discovered that
the books by Michael W Lucas always seem to play a role that is at the same time
very niche, and very valuable. So I have taken writing this review as an
opportunity to also reflect on this niche.

@@wp:<!--more-->@@

* Who is this book aimed at.

[[file:02_sysadmin_system-administrator-network-engineer-characters-set-network-diagnostics-users-support-server-maintenance-cartoon-illustrations_178650-2934.jpg]]

What is "Networking for Systems Administrators"? Well, basically it is an
introduction into computer networks for people who are otherwise totally
unfamiliar with either computers or networks.

Don't be misled by the words "Systems Administrators" or even the fact that the
book is from the "Mastery" series. This is more of an irony than a real claim for
domination.

Books from the "Mastery" series are mostly very simple and targeted at the most
entry-level audience. In fact, they contain even less information that the official
manual pages or those guides that you can find on Google in hundreds, spending an
hour.

Does it discount their value? Not in the slightest. Their biggest advantage is that
they, as opposed to most of the cheap quick howtos with clickbaity titles, are
actually correct.

Also, those books generally have a nice style bearing resemblance to fiction books,
possibly the detective stories that Lucas has been writing as a different branch of
his literary career.

* So who are those "Systems Administrators" the book is aimed at?

[[file:03_computer-science-memes-on-210845.jpg]]

There are two answers to this question. The first would be *power users*, a term
invented by Microsoft to describe people most likely to screw up their carefully
crafted heuristics. The second has a slight flavour of national colour, and is hard
to translate in English, but let me go with an occasional-ism "anykeyster" - the IT
department employee who helps people to find the "Any" key on their keyboard.

So, after laughing in the audience of my enlightened friends has abated, let me try
and convince you that this is, really, the most sensitive, the most vulnerable, and
the most in need of community support group of computer users.

Why is that? Because they are already curious enough to start digging into the
subject matter, but not yet experienced enough to distinguish truth from obvious
lies. They are skilful enough to break things, but not skilful enough to repair
them.

So good introductory books on, basically, any subject are of critical importance to
any healthy technical field, and especially to automated thinking.

* What topics does the book cover?

[[file:04_tracert.jpg]]

Let me start from the topics the book does _not_ cover. The book speaks nothing
about a tool which most of us have probably toyed with, pretending to be savvy
hackers - Nmap. The book does mention it, so curious minds might very well get
enticed, and try to follow the link, but I think that it could have been covered
more extensively.

So now let me try and list the things that are touched in the book with noticeable
attention:

1. Addressing with ipv4 and ipv6
2. Routing
3. TCP and UDP with ports
4. Listening and generating TCP and UDP traffic for testing
5. Domain Name System and its client debugging
6. Packet sniffing and basic analysis with tcpdump
7. Packet filtering and firewalling

Ah, okay, one thing that is also missing from the book is firewalling and packet
rewriting. The book has a whole chapter dedicated to it, but it does not nearly
give enough practical knowledge to actually apply it to your own machine.

The rest seven topics can be called the staple of networking, and Lucas faithfully
covers the basics of each one, which, I would hope, would make the user less
misguided by poor googleable howtos and ready to ask Google further questions.

The depth of coverage for each topic is really not that great, but the most basic
examples of command-line usage for each set of commands corresponding to each layer
of networking are properly given.

Myself, even though I have spend significant effort debugging networks of Linux,
BSD, and Windows systems, have found certain command incantations that I haven't
used as much as I should have, for example, interface error statistics ~~ip -s
link~~, and the one that abuses ~~netcat~~ to forward shell access (find it in the
book).

The style, as mentioned before, is light, with jokes and funny but plausible
examples from real life, which makes reading much less boring than your average
thick book on networking or programming.

Indeed, the book is not thick at all. It took me three hours to read it, although I
have to admit, a lot of content evaded my eyes because I had already known it, and
was glossing over. I suspect that for a newbie it might take about three times
more, which is still barely more than a single working day.

Shall I recommend this book to an experienced network administrator or programmer?
No, not really. But even for them it could be a useful tool, to be handed out to
the biggest troublemakers on the network in order to develop a common language.

But I shall definitely recommend this book to everyone who is either just starting
with computer networking, or has been misled by the companies and governments
promising "ease and convenience".

* Contacts

Subscribe and donate if you find anything in this blog and/or other pages useful.
Repost, share and discuss, feedback helps me become better.

I also have:
- Facebook :: http://facebook.com/vladimir.nikishkin
- Telegram :: http://t.me/unobvious
- GitLab ::   http://gitlab.com/lockywolf
- Twitter :: https://twitter.com/VANikishkin
- PayPal :: https://paypal.me/independentresearch

# 01_cover_networking-for-systems-administrators.jpg http://lockywolf.files.wordpress.com/2023/05/01_cover_networking-for-systems-administrators.jpg
# 02_sysadmin_system-administrator-network-engineer-characters-set-network-diagnostics-users-support-server-maintenance-cartoon-illustrations_178650-2934.jpg http://lockywolf.files.wordpress.com/2023/05/02_sysadmin_system-administrator-network-engineer-characters-set-network-diagnostics-users-support-server-maintenance-cartoon-illustrations_178650-2934.jpg
# 03_computer-science-memes-on-210845.jpg http://lockywolf.files.wordpress.com/2023/05/03_computer-science-memes-on-210845.jpg
# 04_tracert.jpg http://lockywolf.files.wordpress.com/2023/05/04_tracert.jpg
