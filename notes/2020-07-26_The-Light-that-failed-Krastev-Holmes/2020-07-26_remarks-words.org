# Time-stamp: <2023-08-07 22:03:51 lockywolf>
#+created: <2020-07-26 Sun 15:10>
#+author: lockywolf gmail.com
#+title: Unknown words and notes for the "Light that Failed by Ivan Krastev and Steven Holmes".

* Introduction

I wrote this file post-factum, after already publishing the review.
Therefore, the remarks section is empty.

* Remarks

See the review file: [[file:2020-07-14_Review-on-The-Light-that-Failed_Ivan-Krastev-and-Stephen-Holmes.org]]

* Words

I (lockywolf) copied these words from the main planner file.

|   0 | Word               | Translation                                     |
|-----+--------------------+-------------------------------------------------|
|   1 | feckless           | тщетный                                         |
|   2 | undercut           | сбить цену                                      |
|   3 | tellingly          | красноречиво                                    |
|   4 | once-fêted         | когда-то чествованный                           |
|   5 | lustre             | глянец, блеск                                   |
|   6 | topple             | опрокинуть                                      |
|   7 | bullwark           | насыпь, бастион                                 |
|   8 | loosed             | made more loosed                                |
|   9 | genuflect          | преклонять колени                               |
|  10 | trite              | банальный                                       |
|  11 | firmament          | небосвод                                        |
|  12 | stand-off          | ничья, пат                                      |
|  13 | fraught            | преисполненный                                  |
|  14 | veering            | контролируемо ослаблять                         |
|  15 | fomented           | разжигать, подстрекать                          |
|  16 | disparaged         | преуменьшать                                    |
|  17 | malaise            | недомогание                                     |
|  18 | perfunctory        | небрежный                                       |
|  19 | mimetic            | подражательный                                  |
|  20 | glib               | бойкий                                          |
|  21 | contentious        | сварливый                                       |
|  22 | prescient          | предвидящий                                     |
|  23 | swathe             | обмотанный слоями                               |
|  24 | feign              | симулировать                                    |
|  25 | rankling           | нагноение, (фиг. обида)                         |
|  26 | imperious          | властный, повелительный                         |
|  27 | erstwhile          | былой                                           |
|  28 | irksome            | надоедливый                                     |
|  29 | ricketiness        | покосившеся (нар.)                              |
|  30 | supercilious       | высокомерный                                    |
|  31 | condescendingly    | снисходительно                                  |
|  32 | travail            | тяжкий труд                                     |
|  33 | apprehension       | опасение                                        |
|  34 | mingle             | смешение                                        |
|  35 | roil               | муть                                            |
|  36 | crassly            | несуразный                                      |
|  37 | seethe             | бурлить                                         |
|  38 | indelibly          | неизгладимо                                     |
|  39 | prescient          | предвидящий                                     |
|  40 | capsize            | опрокинуть                                      |
|  41 | taproot            | стержневой корень                               |
|  42 | scurrilous         | непристойный                                    |
|  43 | offhand            | экспромтом                                      |
|  44 | implacable         | неумолимый                                      |
|  45 | root-and-branch    | целиком и полностью                             |
|  46 | pungently          | пикантно                                        |
|  47 | wily               | коварно                                         |
|  48 | hoodwink           | обмануть, провести                              |
|  49 | inapposite         | неуместный                                      |
|  50 | excoriation        | разнос, жёсткая критика                         |
|  51 | condescendingly    | снисходительно                                  |
|  52 | presciently        | прозорливо                                      |
|  53 | grist              | помол зерна/солода                              |
|  54 | fomented           | разожжённый                                     |
|  55 | throe              | агония                                          |
|  56 | bait-and-switch    | обман путём предложения товара по неверной цене |
|  57 | foisted            | навязанный                                      |
|  58 | onerous            | обременительны                                  |
|  59 | gratuitously       | беспричинно                                     |
|  60 | repudiation        | отказ, отречение                                |
|  61 | heinous            | гнусный                                         |
|  62 | denigrate          | порочить                                        |
|  63 | humdrum            | банальный                                       |
|  64 | incongruous        | нелепый                                         |
|  65 | tatty              | безвкусный                                      |
|  66 | grisly             | скверный                                        |
|  67 | underhandedness    | скрытое нечестное поведение                     |
|  68 | congruous          | гармоничный                                     |
|  69 | lofty              | возвышенный                                     |
|  70 | afflict            | тревожить, заражать, влиять в плохом смысле     |
|  71 | gnawing            | подтачивать                                     |
|  72 | fulminate          | гремучая (смесь)                                |
|  73 | wrought            | кованый                                         |
|  74 | candour            | откровенность                                   |
|  75 | gabfest            | торжество словоблудия                           |
|  76 | rattle             | греметь                                         |
|  77 | heinous            | гнусный                                         |
|  78 | fluster            | слегка возбудиться                              |
|  79 | uncouth            | неотёсанный                                     |
|  80 | patrimony          | вотчина                                         |
|  81 | scoffing           | насмешливый                                     |
|  82 | dishearten         | привести в уныние                               |
|  83 | aсquiesce          | неохотно согласиться                            |
|  84 | muzzled            | в наморднике                                    |
|  85 | condescension      | снисходительность                               |
|  86 | indelible          | неизгладимый                                    |
|  87 | preposterous       | нелепый                                         |
|  88 | pent-up            | сдерживаемый                                    |
|  89 | galling            | раздражение (типа кожи, также фиг.)             |
|  90 | pillory            | выставить на осмеяние                           |
|  91 | extirpation        | искоренение                                     |
|  92 | volte-face         | резкая перемена                                 |
|  93 | segue              | переход                                         |
|  94 | bristling          | ощетинившийся                                   |
|  95 | complacent         | самодовольный                                   |
|  96 | scathing           | уничтожающий                                    |
|  97 | pungently          | пикантно                                        |
|  98 | truculent          | свирепый                                        |
|  99 | conceit            | тщеславный                                      |
| 100 | gnawing            | грызущий                                        |
| 101 | shabbily           | затрапезно                                      |
| 102 | preponderance      | преобладание                                    |
| 103 | vainglorious       | тщеславный                                      |
| 104 | inexorably         | неумолимо                                       |
| 105 | disavowing         | отрекаясь                                       |
| 106 | pent-up            | сдерживаемый                                    |
| 107 | stave off          | предотвратить                                   |
| 108 | papered            | обёрнутый                                       |
| 109 | demeaning          | унизительный                                    |
| 110 | makeshift          | импровизированный                               |
| 111 | fending off        | парировать                                      |
| 112 | sneering           | насмешливый                                     |
| 113 | ingratiating       | льстивый                                        |
| 114 | bequeath           | завещать                                        |
| 115 | spinmeister        | пропагандист                                    |
| 116 | inchoate           | незавершённый                                   |
| 117 | heyday             | рассвет                                         |
| 118 | mendacity          | лживость                                        |
| 119 | subliminal         | подсознательный                                 |
| 120 | gimmicks and ruses | "трюки и трюки?"                                |
| 121 | hobbling           | хромать                                         |
| 122 | bungling           | головотяпство                                   |
| 123 | faltering          | прерывистый                                     |
| 124 | thwarter           | пресечь                                         |
| 125 | dizzying           | головокружительный                              |
| 126 | denouement          | развязка                        |
| 127 | paean               | победная песня                  |
| 128 | cosying             | успокаивание                    |
| 129 | strut               | подпорка                        |
| 130 | flout               | попирать                        |
| 131 | moonstruck          | помешанный                      |
| 132 | scornful            | презрительный                   |
| 133 | vaunt               | превозносить                    |
| 134 | impelled            | побуждать                       |
| 135 | pollen              | пыльца                          |
| 136 | stammering          | заикание                        |
| 137 | abet                | соучастие                       |
| 138 | impute              | вменить, приписать              |

| 139 | ratchet up          | усиливать                       |
| 141 | chicanery           | кляузничество                   |
| 142 | cribbing            | списывание (на уроках)          |
| 143 | gauche              | неловкий                        |
| 144 | boorish             | невоспитанный                   |
| 145 | jeering             | глумление                       |
| 146 | clutter             | беспорядок, суматоха            |
| 147 | gaudy               | безвкусный                      |
| 148 | basking             | (гигантский?)                   |
| 149 | denigrating         | клеветнический                  |

| 150 | fluke               | счастливая случайность, неудача |
| 151 | meandering          | извилистый                      |
| 152 | replete             | переполненный                   |
| 153 | self-effacing       | скромный                        |
| 154 | repudiation         | отрицание                       |
| 155 | airy                | мечтательный, ветреный          |
| 156 | conceit             | тщеславие                       |
| 157 | candour             | откровенность                   |
| 158 | jingoistic          | экстремально патриотичный       |
| 159 | strewn              | усыпанный                       |
| 160 | clout               | лоскут                          |
| 161 | engender            | породить                        |
| 162 | hamstrung           | с подрезанными крыльями         |
| 163 | interloper          | тот, кто вмешивается в дела     |
| 164 | fortuitously        | случайно                        |
| 165 | stupendous          | колоссальной важности           |
| 166 | misbegotten         | рождённый по ошибке             |
| 167 | to irk              | раздражать                      |
| 168 | to contend          | соперничать                     |
| 169 | subliminal          | подсознательный                 |
| 170 | underhanded         | коварный                        |
| 171 | fray                | износ                           |
| 172 | insouciance         | беззаботность                   |
| 173 | bamboozle           | надувать                        |
| 174 | connivance          | попустительство                 |
| 175 | enraptured          | приводить в восторг             |
| 176 | foment              | разжигать                       |
| 177 | profligate          | расточительный                  |
| 178 | posterity           | последующие поколения           |
| 179 | unfazed             | невозмутимый                    |
| 180 | precipitous         | обрывистый                      |
| 181 | curry favour        | добиться одобрения лестью       |
| 182 | working stiff       | те, кто работают                |
| 183 | roiling             | бурливый, мутный                |
| 184 | sapper              | сапёр                           |
| 185 | consanguinity       | единокровность                  |
| 186 | coddling            | нянчить                         |
| 187 | prevaricate         | увиливать                       |
| 188 | untoward            | неблагоприятный                 |
| 189 | dish out            | выгибаться                      |
| 190 | brashly             | порывисто                       |
| 191 | duped               | обманутый                       |
| 192 | candid              | беспристрастный                 |
| 193 | rowdy               | дебошир                         |
| 194 | epitomise           | резюмировать                    |
| 195 | mendacity           | лживость                        |
| 196 | loosed              | "подрасслабить"                 |
| 197 | take up             | принятся за                     |
| 198 | adjudication        | приговор                        |
| 199 | sidle               | ходить бочком                   |
| 200 | titillate           | щекотать                        |
| 201 | excoriate           | сделать ссадину                 |
| 202 | momentous           | важный                          |
| 203 | flagrant            | вопиющий                        |
| 204 | foment              | поджигать                       |
| 205 | go to the brink     | дойти до грани                  |
| 206 | wager               | ставка                          |
| 207 | hindsight           | "глядя в прошлое"               |
| 208 | repudiated          | аннулировать, отречься          |
| 209 | pernicious          | пагубный                        |
| 210 | unflinching         | неустрашимый                    |
| 211 | flipside            | оборотная сторона               |
| 212 | hoist               | подъёмник                       |
| 213 | eschew              | избегать, сторониться           |
| 214 | pent-up             | сдерживать                      |
| 215 | caller of the shots | тот, кто отвечает               |
| 216 | assay               | анализ                          |
| 217 | feign               | симулировать                    |
| 218 | copious             | обильный                        |
| 219 | fray                | изношенный                      |
| 220 | coaxing             | уговаривание                    |
| 221 | daub                | штукатурка                      |
| 222 | cheek-by-jowl       | плечом к плечу                  |
| 223 | jostling            | тесниться                       |
#+TBLFM: $1=@#-1

