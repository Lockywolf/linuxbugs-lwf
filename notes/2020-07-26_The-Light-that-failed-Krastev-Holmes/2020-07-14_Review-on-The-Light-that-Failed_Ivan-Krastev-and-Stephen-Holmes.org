#+BLOG: wordpress
#+POSTID: 225
#+DATE: [2020-07-18 Sat 22:56]
#+Time-stamp: <2020-10-27 19:15:16 lockywolf>
#+author: lockywolf gmail.com
#+title: A Review on The Light That Failed by Ivan Krastev and Stephen Holmes.

#+begin_abstract

[[file:./001-book-front-s32310042.jpg]]

I have read "A Light That Failed". 
This review is not a part of the series of reviews on technological books.
Nevertheless, in order for a brain to keep moving, some humanitarian reading is advised.

This book was a part of a book club in discussion in Shanghai.
Speaking briefly, it discusses the new (2019) tendency in politics in which the western politicians start using the rhetoric that they have not been using so far; the rhetoric that is not unlike the one used by the authoritarian politicians.
Surprisingly, the book is known among readers in China, it has a rating on DouBan.
The book is not related (at least, directly) to the eponymous novel by Rudyard Kipling.
The book is not related (at least, directly) to the eponymous film of 1939, based on the work by Rudyard Kipling.

#+end_abstract

#+html: <p><!--more--></p>

* The Format



This book looks like a scientific book.
It tries to be serious.
The book structure is well defined, the chapters have a clearly defined scope, the narrative looks as if it is trying to make a self-consistent argument in favour of a certain thesis, in such a way to establish this thesis' truthiness.
What is certainly typical for scientific books, it is accompanied by an extensive bibliography.

On the other hand, even though it presents a very carefully crafted image of a scientific book, certain tiny wrinkles on the seemingly impeccable surface of the outside image.
Why are the terms the authors are using are not that clearly defined in right in the beginning?
Why does the reference list include so many links to newspapers and other publicist material?

Well, maybe it is not actually a scientific book? 

My friend told me that this is written in just the way the U.S. discusses the society.
Hmm... too bad for science, too good for the U.S.A., I guess.
Perhaps I would appreciate if Russian books about societal matters were written with this care.

* The Narrative

[[file:002-right-left.jpg]]

The book largely follows the standard left-wing (but not ultra left-wing) narrative of the generalised West.
This alone would have been enough to not consider the book altogether.
However, the attention to detail, combined with a uniquely broad coverage of the various sources, especially a one of a kind attention to the Central European view of what is going on, still attracted my attention.

The book speaks about the essential tiredness of the peoples all over the world, from Russia, through Central Europe, especially Hungary, to the USA, with it's already not that freshly inaugurated president Donald Trump, tiredness of the traditional way of speaking about the world's political path.

There is a lot spoken about the age of imitation.
Contrary to the traditional perception, the imitation by itself is not described as something intrinsically bad.
It is noted, however, that imitation is not traditionally seen as something worth of being proud of either.

This very much reminds me of the typical Chinese attitude that I hear way too often, and even more from people doing "Chinese copies" of stuff.
"We are first and foremost learning, and copies are a nice by-product that makes us rich."
A similar attitude is explored in the Central Europe, where people were also forced by the laws of history to imitate the Western models, and too often superficially.
Similarly, Russia is shown to behave in an imitative way, only imitating (badly) certain less appreciated elements of the Western behaviour.

The imitation is one of the central themes in the book, even though it didn't deserve a place in the title.
The other pervasive concept (that is even less well defined than imitation) is "liberalism". 
Liberalism is not actually defined anywhere in the book, and what the authors imply by "liberalism" seems to be quite conflicting with what I would call liberalism.
This disparity is even more exacerbated by the fact that the authors are forced (by the object of study) to consider the Central European definition of "liberalism" that is "anti-communism", and somehow reconcile it with the Western European one, which has been recently appropriated by the "left".

I won't expose too much of this inevitable conflict in such a short review, and the curious reader is invited to visit the book itself.

* What to make out of liberalism?

[[file:003-liberalism.jpeg]]

What can I make out of the book as a reader?

The authors claim that the world got tired of "liberalism" and now wants freedom from it.
I disagree.
The rhetoric that various politicians all over the world are increasingly using, may be discomfiting to the "establishment liberals", but the reason why it is used lies not within the literal meaning of the words being pronounced, but rather owes to the fact that the "establishment liberals", who are so well versed in the liberal jargon, are in fact representing a more authoritarian standpoint that the supposed "illiberals".
The liberalism itself, as a concept of "live and let live" will probably never get old, just as it, probably, has never been totally alien to people.
It is always worth looking at the essence of things rather than their superficial cover.

This "live and let live", however, raises an interesting question of "who" should live, and let live "who"?
The idea of "illiberal democracy" (I am quoting the book for a lack of a better term) can be roughly summarised as "the easiest way to protest is to go away". 
Why would I be fighting a dictator, when I can just move to another country?
In China this may even turn out to be like "why would I be criticising my province head, when I can move to another province?".

And if taking such an idea to the extreme, you would end up in the world that is incredibly intolerant to each other's views.
Peculiarly, this world will be also very visibly diverse, as people would mix a lot and have a lot of unlike superficial features.
However, inside of it you will find less diversity, not more, because "really different" people would move away.
It will thus be the world that is both free and intolerant at the same time.
It will be superficially diverse and homogeneous inside the core.

* Imitation

[[file:004-imitation.jpeg]]

Imitation is the essence of life.
The first version of this review has "human life" in the first sentience, but in fact it lies in the core of all life whatsoever.
Life is the way that protein bodies replicate.
Information transmission not exist without a medium, and a medium is a thing that copies information from one storage to another.
Each time something is heard, seen, felt, smelled or tasted, there is a copy of it made.
Moreover, there is even nothing that can reliably differentiate between a real object and a synthetic signal to a "brain in a jar", and we all are essentially "brains in jars" of our own skulls.

It is needless to repeat that repetition is the mother of learning. 
(A Russian proverb.)
Repetition is imitation, imitation is copying.
The West seems to astonishingly unwilling to accept the fact that information is not a physical object.
Immeasurable effort has been spent onto trying to turn information into canned digital artefacts that behave like physical objects, and all of it is wonderfully faulty.
Trying to restrict imitation of anything practical while urging the people around you to imitate you superficially is a perfect recipe for a disaster.
And part of an image of this disaster is so carefully carved on the pages of the book.

The book does not give a definitive answer on what sense to make out of the new prevalence of imitation in the modern world.
Still, as a survey of imitative political endeavours the book is quite comprehensive.

The book even speaks a bit about China, but judging from the fact that I cannot remember anything from that chapter, their discussion wasn't very compelling.

* Conclusion

[[file:005-umbrella.jpg]]

If I have to summarise my impression of the book contents in few words: the authors are wrong in that the Light of Liberalism has failed, but Liberalism of the new century will not be very similar to the liberalism that was a baby of the Century of Two World Wars.

It was an entertaining reading. 
I learned a lot of new English words, and reinforced my habit of reading books with a pen.
Will this book be remembered?
Unlikely. 
I think, it will be forgotten withing a couple of years.
Is it fun? 
I'd say, it beats Twitter.
Is it telling the truth?
No, but if you are interested in such kind of literature, you would be filtering out meaningless water automatically.

* Contacts

I also have:
- Facebook :: http://facebook.com/vladimir.nikishkin
- Telegram :: http://t.me/unobvious
- GitLab ::   http://gitlab.com/lockywolf


# ./001-book-front-s32310042.jpg http://lockywolf.files.wordpress.com/2020/07/001-book-front-s32310042.jpg
# 002-right-left.jpg http://lockywolf.files.wordpress.com/2020/07/002-right-left.jpg
# 003-liberalism.jpeg http://lockywolf.files.wordpress.com/2020/07/003-liberalism.jpeg
# 004-imitation.jpeg http://lockywolf.files.wordpress.com/2020/07/004-imitation.jpeg
# 005-umbrella.jpg http://lockywolf.files.wordpress.com/2020/07/005-umbrella.jpg
