#!/bin/bash

# Simple memory monitor. Now only supports a single process.

logfile=$1
rm "$logfile"
shift

"$@" &

CHILD_PID=$!

runtime_sec=0

while true ; do
  if kill -0 $CHILD_PID > /dev/null 2>&1; then
    child_memory=$(ps --no-headers --pid $CHILD_PID -orss)
    #printf "%s:Child RSS=%s bytes.\n" "$runtime_sec" "$((child_memory * 1024))"
    printf "%s %s\n" "$runtime_sec" "$((child_memory))" >> "$logfile"
  else
    printf "No child running.\n"
    break
  fi
  sleep 60;
  runtime_sec=$((runtime_sec + 1))
done

wait -f $CHILD_PID
retval=$?

if ((retval == 127 )) ; then printf "Failed to wait child\n" ; exit 127 ; fi

printf "Child exited with %s\n" $retval

exit $retval
