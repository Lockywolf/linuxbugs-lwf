#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2021-09-26 13:27:03 lockywolf>
#+title: 
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs 27.1


set -e

alias winword="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msoffice2010/drive_c/Program\ Files/Microsoft\ Office/Office14/winword.exe"
alias powerpnt="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msoffice2010/drive_c/Program\ Files/Microsoft\ Office/Office14/powerpnt.exe"
alias msproject="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msproject/drive_c/Program\ Files/Microsoft\ Office/Office14/winproj.exe"
