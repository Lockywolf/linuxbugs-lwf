#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-11-10 20:41:47 lockywolf>
#+title: Common aliases for all machines.
#+author: lockywolf
#+date: 
#+created: <2021-09-26 Sun 13:22>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :bash:admin:shell
#+creator: Emacs 27.1


#launch windowed emacs from the console
alias emw="XDG_RUNTIME_DIR=/run/user/$(id -u) emacsclient -n -a ''"
alias emc="XDG_RUNTIME_DIR=/run/user/$(id -u) emacsclient -nw -a ''"

#git
alias gits="git status"
alias gitd="git --no-pager diff"
alias gitfix="git commit -a --amend --no-edit"
#<2020-11-03 Tue 11:46>
alias gitrv="git remote -v"


# locate
#alias l='locate -ieb'
function l
{
  locate -ieb "$@" | grep -v -F /home/lockywolf/.emacs.d --line-buffered
}

# curl
alias curltouch='curl --head'
alias curlget='curl --styled-output -O -C - '



alias ls="ls $LS_OPTIONS"
alias la="ls -lrht $LS_OPTIONS"

alias pgl="pgrep -lafi"

alias free="free -h"
alias cat="cat -t"

#wget always continue
alias wget='wget -c --content-disposition --xattr'

function a2()
{
aria2c "--log=/tmp/RAMFS/$(date --iso-8601=seconds)-aria2-download.log" \
 --max-connection-per-server=120 \
 --min-split-size=148576 \
 --split=120 \
 --dir="." \
 --auto-file-renaming=false "$@"
}

function lwf_pstree()
{
  pstree -T -s -p "$1"
}

function lwf_pgrep()
{
  for l_id in $(pgrep -fi "$1")
  do
    lwf_pstree "$l_id"
  done
}

if [[ $INSIDE_EMACS ]] ;
then
    export PAGER=cat;
fi

