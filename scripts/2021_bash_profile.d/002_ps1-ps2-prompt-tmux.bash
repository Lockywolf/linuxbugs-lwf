#!/usr/bin/env bash
#+Time-stamp: <2023-11-21 10:26:16 lockywolf>

# https://www.babushk.in/posts/renew-environment-tmux.html

A=$(date +%z)
MY_Z=${A:0:3}:${A:3:5}

MY_STARTTIME=$EPOCHREALTIME
MY_AT_PROMPT=false

if [[ $TERM != dumb ]] ; then
  GREEN="$(tput setaf 2)"
  RED="$(tput setaf 1)"
  YELLOW="$(tput setaf 3)"
  PINK="$(tput setaf 9)"
  RESET="$(tput sgr0)"
  #RED=""
  #GREEN=""
  #RESET=""
fi

function after_prompt_command()
{
  L_EXIT=$?
  local retval_color
  #if (( L_EXIT != 0 )) ; then retval_color=${RED} ; fi
  if (( L_EXIT != 0 )) ; then l_status=$'\001'${RED}$'\002'$L_EXIT$'\001'${RESET}$'\002'; else l_status=$L_EXIT ; fi
  prg_time_consumption_sec=$(echo $EPOCHREALTIME - $MY_STARTTIME | bc)
  prg_time_consumption_msec=$(echo "$prg_time_consumption_sec * 1000" | bc)
  prg_time_consumption_msec=${prg_time_consumption_msec%.*}
  if (( prg_time_consumption_msec > (59*59*1000) )) ; then prg_time_consumption_sec=$'\001'${PINK}$'\002'$prg_time_consumption_sec$'\001'${RESET}$'\002';
  elif (( prg_time_consumption_msec > (59*1000) )) ; then prg_time_consumption_sec=$'\001'${RED}$'\002'$prg_time_consumption_sec$'\001'${RESET}$'\002';
  elif (( prg_time_consumption_msec > 1000 )) ; then prg_time_consumption_sec=$'\001'${YELLOW}$'\002'$prg_time_consumption_sec$'\001'${RESET}$'\002';fi

  echo -e "$(date +%Y-%m-%dT%H:%M:%S)$MY_Z|$l_status|$prg_time_consumption_sec|"
  MY_AT_PROMPT=true
}

before_prompt_command() {
  declare -g last_command
  declare -g current_command
  declare -g l_co
  last_command=$current_command
  current_command=$BASH_COMMAND
  if [[ "$current_command" != after_prompt_command ]] ; then
    # we are running something, show program name in terminal title
    l_d=${current_command:0:12}
  else
    # we are idling at a directory, show directory title
    l_d=${DIRSTACK[0]}
    l_d=${l_d##*/}
    l_d=${l_d:-/}
    l_d=${l_d:0:10}
  fi

  if [[ "$TERM" != "dumb" ]] ; then
    echo -ne "\033]0;$l_d\007"
  fi

  if [ -n "$TMUX" ]; then
    export $(tmux show-environment | grep "^SSH_AUTH_SOCK")      > /dev/null
    export $(tmux show-environment | grep "^DISPLAY") > /dev/null
  fi

  if [[ $MY_AT_PROMPT == true ]] ; then
    MY_AT_PROMPT=false
    MY_STARTTIME=$EPOCHREALTIME
  fi
  return 0
}
trap - DEBUG
trap before_prompt_command DEBUG

PROMPT_COMMAND='after_prompt_command'

unset PS0
shopt -s checkwinsize
export PS1='\u@\h:\w$(if (( UID == 0 )) ; then echo -ne \001${RED}\002 ; fi)\$$(echo \001${RESET}\002) '

