#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-12-06 17:02:58 lockywolf>
#+title: Compute total memory consumed by process. Dirty hack.
#+author: lockywolf
#+date:
#+created: <2023-12-06 Wed 16:58:32>
#+refiled: 
#+language: bash
#+category: admin
#+filetags: :memory:processes:programming:bash
#+creator: Emacs-30.0.50/org-mode-9.6.8

if [[ "" == "$1" ]] ; then
  printf "Usage: %s <process name>\n" "$0" 1>&2
  exit 1
fi

pgrep -f "$1" | xargs -I{} -n1 bash -c 'pmap -x {} | tail -n 1' | awk -v SSSUM=0 'BEGIN{SSSUM=0;}{print $4;SSSUM=SSSUM+$4;}END{print "sum:"; print SSSUM;}'  | tail -n 1 | xargs -I{} bash -c 'echo $(( {} / 1024))M'
