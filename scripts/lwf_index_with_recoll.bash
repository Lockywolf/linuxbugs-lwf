#!/bin/bash

RECOLL_DIR=/home/lockywolf/.recoll/xapiandb

if [[ "$1" == "conditional" ]]
then
    echo let us check change
    SECONDS_NOW=$(date '+%s')
    SECONDS_OLDEST=$(stat --format='%Y' $RECOLL_DIR/** | sort -V | head -1)
    if (( SECONDS_NOW - SECONDS_OLDEST < $(( 60 * 60 * 24 )) ))
    then
        printf "Index is new enough.\n"
        exit 0
    fi
fi

if ! pgrep recollindex
then
    if ! nice -20 recollindex 2> "/run/user/$(id -u)/recoll-stderr.out"
    then
        printf "Recoll encountered a Fatal Error!\n" 2>&1
    fi
else
    printf "Recoll already running: %s" "$(pgrep -la recollindex)"
fi

#time recollindex -c ~/.recoll/subindices-lwf/Data/
