#!/bin/bash

cgcreate -g 'name=noctrl:lockywolf/wechat'
cgclassify -g 'name=noctrl:lockywolf/wechat' $BASHPID

DISPLAY=:0.0
APPNAME=wechat

if pgrep -fi 'bin/WeChat.exe'
then
  notify-send  --expire-time=$(( 60 * 1000 )) "Wechat already running."
  exit 1
fi



mkdir -p /run/user/"${UID}"/"$APPNAME"/
export LC_ALL=zh_CN.utf8
(
  printf "%s" "$$" > /run/user/"${UID}"/"$APPNAME"/"$APPNAME"-right-monitor.pid
  while true
  do
      export __NV_PRIME_RENDER_OFFLOAD=1
      export __VK_LAYER_NV_optimus=NVIDIA_only
      export __GLX_VENDOR_LIBRARY_NAME=nvidia
    #  ts < <( wine "/home/lockywolf/.wine/drive_c/Program Files/Tencent/WeChat/WeChat.exe" 2>&1 ) &>> /run/user/"${UID}"/"$APPNAME"/"$APPNAME"-right.stdout+stderr
      ts < <( wine '/home/lockywolf/.wineprefix-wechat64/drive_c/Program Files/Tencent/WeChat/WeChat.exe' 2>&1 ) &>> /run/user/"${UID}"/"$APPNAME"/"$APPNAME"-right.stdout+stderr
    if [[ x"$PPID" == x"1" ]] ; then exit 1 ; fi
    notify-send  --expire-time=$(( 60 * 1000 )) "Wechat died."
    sleep 60
  done
) &
printf "%s" "$!" > /run/user/"${UID}"/"$APPNAME"/"$APPNAME"-right-monitor-from-parent.pid

disown

#| tee -a "$LOG" &


