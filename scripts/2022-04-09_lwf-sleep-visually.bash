#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-04-07 14:41:29 lockywolf>
#+title: A demo of a countdown timer for bash.
#+author: 
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: 
#+filetags: 
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


L_RE="[[:digit:]]+"

L_I="$1"

if [[ "$L_I" =~ $L_RE ]]
then
  : # all fine
else
  printf "%s\n" "Usage: $0 number "
fi


while (( L_I > 0 )) ; do  printf "\033[1K\r%s" "$L_I"; L_I=$(( L_I - 1 )) ; sleep 1 ; done
