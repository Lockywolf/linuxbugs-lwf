#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-03-10 16:31:32 lockywolf>
#+title: A loop to set desktop background for i3.
#+author: lockywolf
#+date: 
#+created:
#+refiled:
#+language: bash
#+category: admin
#+filetags: :admin:x11:xorg:i3:background:wallpaper
#+creator: Emacs 28



while true
do
  nitrogen --set-color=#404040 --set-auto --random "$HOME"/gd/PICS/Instagram/
  L_PID=$(ps -o ppid= -p $$ | sed 's/ //g')
  if [[ x"$L_PID" == x"1" ]] ; then exit 1 ; fi
  sleep 60
done
