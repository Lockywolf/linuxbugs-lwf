#!/usr/local/bin/bash
# test
export LC_ALL=C
TARGET=/home/pub/pub/01_repos
mkdir -p "$TARGET"
cd "$TARGET"
rm -rf slackbuilds slackbuilds.tar.xz slackbuilds.sha512sum
if ! git clone 'https://github.com/Ponce/slackbuilds/' 2>&1
then
    printf "Clone failed!\n" 1>&2
    exit 1
fi
cd slackbuilds || exit 1
git checkout master 2>&1 || exit 1
cd ..
gtar cvvf slackbuilds.tar.xz slackbuilds || exit 1
sha512 -q slackbuilds.tar.xz > slackbuilds.sha512sum || exit 1
printf "Debug: slackbuilds fetched successfully\n"


