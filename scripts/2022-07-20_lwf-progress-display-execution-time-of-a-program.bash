#!/usr/bin/env bash

PR=/home/lockywolf/OfficialRepos/yepstat/print2
YS=/home/lockywolf/OfficialRepos/yepstat/yepstat

shopt -s lastpipe

SEC=0
while true;
do
  $PR "$SEC"
  SEC=$(( SEC + 1 ))
  sleep 1
  if (( PPID == 1 )) ; then exit 1 ; fi
done | $YS &
trap "kill -9 $(jobs -lp | tr '\n' ' ' )" EXIT
"$@"

#kill -9  $(jobs -lp | tr  '\n' ' ')
