#!/bin/bash
# Time-stamp: <2021-03-30 08:37:52 lockywolf>
#+date: <2020-11-02 Tue 23:28>
#+author: lockywolf
#+title: A script to start "common good" tasks in the evening.

export DISPLAY=:0

case "$1" in
    stop)
	printf "case down\n"
        sudo /etc/init.d/boinc-client stop
	return 0
	;;
    start)

	if pgrep boinc_client > /dev/null
	then
	    : # already started
	else
	    sudo /etc/init.d/boinc-client start 1> /dev/null
	fi
	;;
    status)
	printf "Transmission:%d\n" "$(pgrep -c transmission)"
	printf "boinc:%d\n" "$(pgrep -c boinc)"
	;;
    *)
	printf "usage: {up,down,status}\n"
esac



