#!/bin/bash
# Time-stamp: <2021-02-19 14:49:57 lockywolf>
#+modified: <2021-02-19 Fri 14:31>
set -e

dt=$(date -Idate)
l_bd=/home/lockywolf/BACKUP/01-FirefoxSessions/$dt/
mkdir -p $l_bd
#cp -v --no-clobber /home/lockywolf/.mozilla/firefox/*.default/sessionstore-backups/previous.jsonlz4 $l_bd
rsync -arq  --prune-empty-dirs --include "*/" --include="*recovery.jsonlz4"  --include="*previous.jsonlz4" --exclude="*" /home/lockywolf/.mozilla/firefox "$l_bd"
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'previous.jsonlz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'recovery.jsonlz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'recovery.baklz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done


# find /home/lockywolf/BACKUP/01-FirefoxSessions/

