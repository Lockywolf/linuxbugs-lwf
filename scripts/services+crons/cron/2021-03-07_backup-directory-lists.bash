#!/bin/bash
# Time-stamp: <2021-03-17 21:30:27 lockywolf>
#+created: <2021-03-07 Sun 19:45>
#+modified: 

set -e

dt="$(date --iso=date)"
l_bd=/home/lockywolf/BACKUP/13_Directories-Lists/"$dt"

mkdir -p "$l_bd"

to_backup=("/home/lockywolf/DevLinux/" "/home/lockywolf/OfficialRepos/")

for dir_to_backup in "${to_backup[@]}"
do
    savefolder=$(printf "%s" "$dir_to_backup" | sed 's/\//!/g')
    find "$dir_to_backup" -mindepth 1 -maxdepth 1 > "$l_bd/$savefolder"
done
