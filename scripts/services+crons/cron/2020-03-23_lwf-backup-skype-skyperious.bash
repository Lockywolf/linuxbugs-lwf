#!/bin/bash
# Time-stamp: <2024-11-02 10:24:13 lockywolf>

# set -e

EAGAIN=11

function parse_timeout_exitstatus()
(
  input="$1"
  if (( input == 124 ))
  then
    printf "timeout"
  elif (( input == 125 ))
  then
    printf "command failed"
  elif (( input == 126 ))
  then
    printf "command cannot be invoked"
  elif (( input == 127 ))
  then
    printf "command not found"
  elif (( input == 137))
  then
    printf "command is killed"
  else
    printf "command terminated with exit status %s" "$input"
  fi
)

FORCE="$1"
echo "FORCE=$FORCE"

L_LOCKDIR=/tmp/skyperius.lockdir
mkdir "$L_LOCKDIR" && trap 'rm -rf "$L_LOCKDIR"' EXIT || exit


#skyperious_home="$HOME/binary_software/2021-04-09_skyperious"
skyperious_home="$HOME/binary_software/2022-12-13_skyperious-with-caching"
skyperious="$skyperious_home"/bin/skyperious
STATE_DIR="$HOME/.local/var/skyperious"
mkdir -p "$STATE_DIR"
DB_NAME="$HOME/BACKUP/08_Chat-Logs/003_Skype/2021-03-10_Skyperious-lockywolf.main.db"
EXPORT_TO_DIR="$HOME/BACKUP/08_Chat-Logs/003_Skype/2021-03-10_Skyperious-lockywolf-export"
mkdir -p "$EXPORT_TO_DIR" || exit 1
mkdir -p "$EXPORT_TO_DIR"/staging || exit 2
mkdir -p "$EXPORT_TO_DIR"/indexable || exit 3

HUMAN_READABLE_NAME="$(date --iso=seconds)_skype-backup"
UNIQ_NAME="$HUMAN_READABLE_NAME""_$(uuidgen)"

source "$skyperious_home"/bin/activate

mkdir -p "$STATE_DIR"
LAST_SUCCESS="$(stat --printf='%Y\n' "$STATE_DIR"/last-success || true )"
LAST_SUCCESS=${LAST_SUCCESS:-0}
LAST_FILE="$(stat --printf='%Y\n' "$EXPORT_TO_DIR"/staging/* | sort -r | head -1)"
LAST_FILE=${LAST_FILE:-0}

# LAST_* is in seconds. Matching seconds is hard. We match minutes.
if (( LAST_FILE > LAST_SUCCESS ))
then
    printf "Bogus files present in backup directory %s!\n" "$EXPORT_TO_DIR"/staging/ 1>&2
elif (( ((LAST_SUCCESS - LAST_FILE)/60) > 150 )) # backup is ~2 hours in length
then
    printf "Files disappeared from backup directory %s!\n" "$EXPORT_TO_DIR"/staging/ 1>&2
    printf "last-success file: %s\n" "$(stat --printf='%y\n' "$STATE_DIR"/last-success || true )" 1>&2
    printf "last file: %s\n" "$(stat --printf='%y\n' "$EXPORT_TO_DIR"/staging/* | sort -r | head -1)" 1>&2
    LAST_SUCCESS=LAST_FILE
fi

NOW=$(date '+%s')
if [[ x"$FORCE" == x"-f" ]]
then
   LAST_SUCCESS=0
fi
if (( (NOW - LAST_SUCCESS) < 60*60*24*1 ))
then
    printf "Existing backup is new enough: %s\n" "$(date --iso=date --date=@${LAST_FILE})"
    # we do not update the last_success date here, because there is no success
#    exit 0
fi

#/home/lockywolf/bin/wgexec
# config in /home/lockywolf/Syncthing_2021/laptop.lockywolf.net/binary_software/2022-12-13_skyperious-with-caching/lib/python3.9/site-packages/skyperious/skyperious.ini
/bin/timeout --kill-after=60s 60m "$skyperious" sync --no-terminal  "$DB_NAME"
exit_status="$?"

if (( exit_status != 0 ))
then
  printf "Skyperious sync failed (exiting). exit_status=%s (%s).\n" "$exit_status" "$(parse_timeout_exitstatus $exit_status)" # 1>&2
  printf "sync failed\n" > "$STATE_DIR"/last-failure
  exit $EAGAIN
elif ! /home/lockywolf/bin/wgexec /bin/timeout --kill-after=60s 3h  "$skyperious" export --no-terminal -t html -o /tmp/"$UNIQ_NAME" "$DB_NAME"
then
  #retval="$?"
  printf "Skyperious export failed (exiting). exit_status=%s (%s).\n" "${PIPESTATUS[0]}" "$(parse_timeout_exitstatus ${PIPESTATUS[0]})" # 1>&2
  printf "Export failed\n" > "$STATE_DIR"/last-failure
  exit $EAGAIN
fi
printf "DEBUG: Skyperious sync and export succeeded.\n"

RESULTING_SIZE=$(du -s /tmp/"$UNIQ_NAME" | cut -f 1)
RESULTING_SIZE=${RESULTING_SIZE:-0}
if (( 250000 > RESULTING_SIZE ))
then
  printf "Export directory too small. Backup likely failed.\n" # 1>&2
  #rm -rf "${EXPORT_TO_DIR:?}/${UNIQ_NAME:?}"
  printf "Directory corrupt\n" > "$STATE_DIR"/last-failure
else
  # 1>&2 is for debugging. When script matures, remove it.
  printf "Skyperious deleting: \n"

  rm -r "$EXPORT_TO_DIR"/indexable/* 1>&2
  mv /tmp/"$UNIQ_NAME" "$EXPORT_TO_DIR"/indexable/"$HUMAN_READABLE_NAME" || { printf "Move to indexable failed! CHECK!\n" ; exit 3 ;}
  (
    cd "$EXPORT_TO_DIR"/indexable/ || { printf "CD failed\n" 1>&2 && exit 1 ; }
    tar --force-local -cf ../staging/"$HUMAN_READABLE_NAME".tar.gz "$HUMAN_READABLE_NAME"
  )
  printf "Success: %s.\n" "$(date --iso=minutes)" >  "$STATE_DIR"/last-success
fi

if (( NOW - LAST_SUCCESS > 60*60*24*30 ))
then
    printf "Skype backups are failing for too long\n" 1>&2
fi

(
cd /home/lockywolf/Syncthing_2021/laptop.lockywolf.net/DevLinux/lwf_private-scripts_private-repo/specific-machine-laptop/lockywolf/bin/ || exit 5
./lwf_rotate-backups.bash --actually-delete --directory="$EXPORT_TO_DIR" --number=20
)
FULL_SIZE=$(du -s "$EXPORT_TO_DIR" | cut -f 1)

if (( FULL_SIZE > 9089952 ))
then
    printf "Skype backups are too big (>5G). Check and delete.\n" 1>&2
fi
