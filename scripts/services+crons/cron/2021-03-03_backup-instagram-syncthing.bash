#!/bin/bash

declare ERROR_DIR

COPY_TIMEOUT=${COPY_TIMEOUT:- $(( 60 * 60 * 1))}

function my_backup()
{
    mkdir -p "$2"
    if ! /bin/timeout --kill-after=15s "$COPY_TIMEOUT"m \
         flock --nonblock --exclusive "$1" \
         flock --nonblock --exclusive "$2" \
         rsync  -aHX --stats -v --partial --inplace --no-i-r --progress --info=progress2 \
         "$1" "$2" #
    then
        printf "Backup Instagram:rsync failed!\n" 1>&2
        exit 1
    fi
}



my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Pictures/Instagram/ "$HOME"/gd/PICS/Instagram/

my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Movies/Instagram/ "$HOME"/gd/PICS/Instagram/

my_backup "$HOME"/Personal_Planner/Markor-Camera-Notes/Daily-Records/ "$HOME"/gd/PICS/Own_PHOTO-Life-Chronology/
# my_backup "$HOME/Personal_Planner/Markor-Camera-Notes/Daily-Records/" "$HOME/gd/PICS/Own_PHOTO-Life-Chronology/"

my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Record/ "$HOME"/Recordings_Video_Audio/2019-08-02_Record-On-the-Stock-Recorder-App-on-OnePlus-5t/

my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Pictures/Screenshots/ "$HOME"/Recordings_Video_Audio/Screenshots-Mobile/

my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes/Diaries-Paper/ "$HOME"/gd/Diaries-Paper/

#my_backup "$HOME"/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes/2021-07-01_Chinese-with-Xie-SiSi/ "$HOME"/gd/STUDY-Material/Chinese/2021-07-01_Chinese-with-Xie-SiSi/

# <2023-06-18 Sun 19:28> : I have disabled this line at restaurant in XuJiaHuai.
# I intended to replace it with unison.
#my_backup "$HOME/Personal_Planner/Markor-Camera-Notes/Incoming/" "$HOME/Incoming/"

# 2021-06-16. I have resigned from SMEE and stopped copying anything.
# my_backup "$HOME/Personal_Planner/Markor-Camera-Notes/SMEE/" "$HOME/gd/Work-Industry-Job-Employment-Staff-University-staff-SMEE/2017-10-05_SMEE/Photo-Records"



# <2023-06-04 Sun 17:11> : I want to automatically delete markor directories that are too old.
# But I do not want to delete everything from a phone that has not been touched for a year.

rm ~/.local/var/log/lockywolf/unison-projects.log
if ! unison -contactquietly -dumbtty -logfile ~/.local/var/log/lockywolf/unison-projects.log -auto -batch -follow 'Regex .*' \
       ~/gd/Projects-Generic-Unclassified-Open/  ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes/Projects-Open/ 1>/dev/null 2>/dev/null ; then
  DISPLAY=:0.0 notify-send "unison Projects-Open failed!" ;
  printf "unison Projects-Open failed:\n" 1>&2
  cat ~/.local/var/log/lockywolf/unison-projects.log 1>&2
fi

rm ~/.local/var/log/lockywolf/unison-incoming.log
if ! unison -contactquietly -dumbtty -logfile ~/.local/var/log/lockywolf/unison-incoming.log -auto -batch -follow 'Regex .*' \
     ~/gd/Incoming/                            ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes/Incoming/ 1>/dev/null 2>/dev/null ; then
  DISPLAY=:0.0 notify-send "unison Incoming failed!" ;
  printf "unison Incoming failed\n" 1>&2
  cat ~/.local/var/log/lockywolf/unison-incoming.log 1>&2
fi

BEGIN_DATE="2023-06-20"

(
  shopt -s nullglob
  cd ~/Syncthing_2021/Oneplus-5t-sdcard/
  printf "%s\n" "# -*- mode: org; eval: (visual-line-mode) ; eval: (auto-revert-mode 1) ; eval: (read-only-mode 1) ; -*-"
  printf "%s\n" "#+title: Auto_Projects"
  for proj in  Personal_Planner/Markor-Camera-Notes/Projects-Open/* ; do
    printf "* TODO %s\n" "$(basename "$proj")"
    printf "\n"
    printf "%s\n" "[[$proj]]"
    printf "\n"
  done
) > ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/"$BEGIN_DATE"_auto_projects.org

(
  shopt -s nullglob
  cd ~/Syncthing_2021/Oneplus-5t-sdcard/
  printf "%s\n" "# -*- mode: org; eval: (visual-line-mode) ; eval: (auto-revert-mode 1) ; eval: (read-only-mode 1) ; -*-"
  printf "%s\n" "#+title: Auto_Incoming"
  for proj in  Personal_Planner/Markor-Camera-Notes/Incoming/* ; do
    printf "* TODO %s\n" "$(basename "$proj")"
    printf "\n"
    printf "%s\n" "[[$proj]]"
    printf "\n"
  done
) > ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/"$BEGIN_DATE"_auto_incoming.org

(
  shopt -s nullglob
  mkdir -p ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Laptop-Only
  cd
  printf "%s\n" "# -*- mode: org; eval: (visual-line-mode) ; eval: (auto-revert-mode 1) ; eval: (read-only-mode 1) ; -*-"
  printf "%s\n" "#+title: Auto_Sched_Laptop"
  printf "%s\n" "#+category: Auto"
  for proj in [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_* ; do
    printf "* TODO %s\n" "$(basename "$proj")"
    printf "%s%s%s\n" "    SCHEDULED: <" "$(stat --format='%w' "$proj" | cut -d ' ' -f 1)" ">"
    printf "\n"
    printf "%s\n" "[[~/$proj]]"
    printf "\n"
  done
) > ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Laptop-Only/"$BEGIN_DATE"_auto_scheduled_laptop.org

(
  shopt -s nullglob
  cd ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes/ || \
    { printf "No directory ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/Markor-Camera-Notes\n" && exit 1 ; }
  printf "%s\n" "# -*- mode: org; eval: (visual-line-mode) ; eval: (auto-revert-mode 1) ; eval: (read-only-mode 1) ;  -*-"
  printf "%s\n" "#+title: Auto_Sched_Phone"
  for proj in [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_* ; do
    printf "* TODO %s\n" "$(basename "$proj")"
    printf "%s%s%s\n" "    SCHEDULED: <" "$(stat --format='%w' "$proj" | cut -d ' ' -f 1)" ">"
    printf "\n"
    printf "%s\n" "[[Personal_Planner/Markor-Camera-Notes/$proj]]"
    printf "\n"
  done
) > ~/Syncthing_2021/Oneplus-5t-sdcard/Personal_Planner/"$BEGIN_DATE"_auto_scheduled_phone.org
