#!/bin/bash
set -uoe pipefail

MARKER=3706e950-626a-11ec-9070-f71ed20d5560
if pgrep -la conky > /dev/null 2>&1
then 
  :
else
  if ! pgrep -lf "$MARKER"
     then
       DISPLAY=:0 zenity --error --text "Conky not running" "$MARKER" &
       disown
  fi
fi
