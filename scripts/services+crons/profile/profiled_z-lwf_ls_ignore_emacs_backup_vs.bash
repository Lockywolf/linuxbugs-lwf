#!/bin/bash
export LS_OPTIONS="$LS_OPTIONS --hide=*~ --hide=\#*\# "
# Set up aliases to use color ls by default:
if [ "$SHELL" = "/bin/zsh" ] ; then
# By default, zsh doesn't split parameters into separate words
# when it encounters whitespace.  The '=' flag will fix this.
# see zshexpn(1) man-page regarding SH_WORD_SPLIT.
  alias ls='/bin/ls ${=LS_OPTIONS}'
else
  alias ls='/bin/ls $LS_OPTIONS'
fi
