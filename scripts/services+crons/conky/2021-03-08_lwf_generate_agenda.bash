#!/bin/bash

mkdir -p /run/user/"$(id -u)"
if ! emacs -q  -batch --eval '(progn (load "~/.emacs.d/init-custom.el")
                                     (setq org-todo-keywords
                                           (quote ((sequence "TODO" "|" "DONE")
                                                   (sequence "|" "CANCELLED"))))
                                     (org-batch-store-agenda-views
                                      org-agenda-span (quote day)
                                      org-agenda-include-diary t))'  \
                                        --kill &> /run/user/"$(id -u)"/cron-emacs-agenda-generator.stderr

then
  printf "Generating agenda failed. \n" 1>&2
else
  head -n 40  /run/user/1000/agenda-snapshot.txt  | grep -Eo '^.{0,80}' | sed '1d'
fi
