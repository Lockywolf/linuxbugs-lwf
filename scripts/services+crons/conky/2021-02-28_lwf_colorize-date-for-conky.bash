#!/bin/bash

if [[ ! "$#" == 1 ]]
then
    printf "Usage: appname validity-period < date in --iso=minutes\n" 1>&2
    printf "validity-period can be {day,week,[0-9]+}\n" 1>&2
    printf "given: %s" "$*" 1>&2
    exit 1
fi

declare DATE_TO_PROCESS

IFS='' read DATE_TO_PROCESS

if [[ -z "$DATE_TO_PROCESS" ]]
then
    printf '${color6}EMPTY INPUT!${color}'
fi

declare VALIDITY_PERIOD
VALIDITY_PERIOD="$1"

SECONDS_IN_DAY=$((60*60*24))
SECONDS_IN_WEEK=$((60*60*24*7))

declare testvalue

case $VALIDITY_PERIOD in
     day)
         testvalue="$SECONDS_IN_DAY"
         ;;
     week)
         testvalue="$SECONDS_IN_WEEK"
         ;;
     *)
         testvalue="$VALIDITY_PERIOD"
         ;;
esac

seconds_now="$(date '+%s')"
seconds_arg="$(date +%s --date="$DATE_TO_PROCESS")"

result_bool=$(( seconds_now - seconds_arg > testvalue ))

if [[ $result_bool == 1 ]]
then
    printf '${color6}'
else
    printf '${color}'
fi
FRIENDLIER_DATE=$(date --iso=minutes --date="$DATE_TO_PROCESS")
printf "%s" "$FRIENDLIER_DATE"
printf '${color}'

