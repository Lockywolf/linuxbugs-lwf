#!/bin/bash
# Time-stamp: <2023-10-23 13:38:45 lockywolf>
#+title: Startup script for conky.
#+author: lockywolf
#+date:
#+created: <2021-07-13 Tue 09:31>
#+refiled:
#+language: bash
#+category: programming
#+filetags: :programming:administration:monitoring:conky
#+creator: Emacs 27.1


export DISPLAY=:0

MY_PIDFILE=/run/user/"$UID"/conky/conky-parent.pid
mkdir -p /run/user/"${UID}"/conky

touch "$MY_PIDFILE"
exec 9<"$MY_PIDFILE"; if ! flock -ne 9  ; then printf 'conky-parent: another instance is running\n';  exit 1 ; fi
printf '%s' "$$" > "$MY_PIDFILE"

while ! xset q &> /dev/null
do
  notify-send  --expire-time=$(( 5 * 1000 )) "Waiting for X to start conky"
  printf "Waiting for X to start conky"
  sleep 5
done

# while ! pgrep -lf xfdesk &> /dev/null
# do
#   notify-send  --expire-time=$(( 5 * 1000 )) "Waiting for x to appear to start conky"
#   sleep 5
# done

## Conky Left
(
  if pgrep -lafi conky-left
  then
    exit 1 # Heuristic.
  fi
  LEFT_LOCKPID=/run/user/"${UID}"/conky/conky-left-monitor.pid
  touch "$LEFT_LOCKPID"
  exec 9<"$LEFT_LOCKPID"; if ! flock -ne 9  ; then printf 'conky-left-insist: another instance is running\n';  exit 1 ; fi
  printf "%s" "$$" >  "$LEFT_LOCKPID"
  while true
  do
    ts < <( /usr/bin/conky  -DD -c /home/"${USER}"/.config/conky/conky-left.conf 2>&1 )     &>>  /run/user/"${UID}"/conky/conky-left.stdout+stderr
    notify-send  --expire-time=$(( 60 * 1000 )) "Conky-left died."
    printf "%s" ""
    sleep 60
  done
) &
LEFT_INSIST_PID="$!"
printf "%s" "$LEFT_INSIST_PID" > /run/user/"${UID}"/conky/conky-left-monitor-from-parent.pid

# Conky right

(
  if pgrep -lafi conky-right
  then
    exit 1 # Heuristic.
  fi
  RIGHT_LOCKPID=/run/user/"${UID}"/conky/conky-right-monitor.pid
  touch "$RIGHT_LOCKPID"
  exec 9<"$RIGHT_LOCKPID"; if ! flock -ne 9  ; then printf 'conky-right-insist: another instance is running\n';  exit 1 ; fi
  printf "%s" "$$" >  "$RIGHT_LOCKPID"
  while true
  do
    ts < <( ~/DevLinux/lwf_bugfixes-for-generalized-bugfixes-and-small-scripts_gitlab/scripts/2023-10-23_monitor-memory-consumption.bash /run/user/1000/conky-right-memory.log /usr/bin/conky  -DD -c /home/"${USER}"/.config/conky/conky-right.conf 2>&1 )     &>>  /run/user/"${UID}"/conky/conky-right.stdout+stderr
    notify-send  --expire-time=$(( 60 * 1000 )) "Conky-right died."
    printf "%s" ""
    sleep 60
  done
) &
RIGHT_INSIST_PID="$!"
printf "%s" "$RIGHT_INSIST_PID" > /run/user/"${UID}"/conky/conky-right-monitor-from-parent.pid

wait
printf "conky-parent: terminate.\n"
