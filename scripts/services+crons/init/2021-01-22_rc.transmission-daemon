#!/bin/bash -x
# Time-stamp: <2021-01-22 16:16:09 lockywolf>
# Author: lockywolf
# Created: <2021-01-22 Fri 14:40>

# rc script to start/stop transmission-daemon

# Based on 2011  Alan Alberghini <414N@slacky.it>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PROGRAM_NAME=transmission-daemon
DAEMON_PATH=/usr/bin/transmission-daemon
PID_FILE=/run/transmission-daemon.pid
DAEMON_DIR=/mnt/big_storage/Torrents-from-Laptop-2021
DAEMON_CONFIG_DIR=$DAEMON_DIR/config
DAEMON_TORRENT_FILES_DIR=$DAEMON_DIR/dotTorrent
DAEMON_INCOMPLETE_DIR=$DAEMON_DIR/Downloading
DAEMON_DOWNLOAD_DIR=/mnt/big_storage/Torrents-from-Laptop-2021/Finished
DAEMON_LISTEN_ADDRESS="10.0.0.4,192.168.2.1"
DAEMON_USER=lockywolf
DAEMON_LOGFILE=$DAEMON_DIR/transmission-daemon.log

declare DAEMON_PID
EXIT_SUCCESS=0
EXIT_NOTRUNNING=254
EXIT_UNKILLABLE=253
EXIT_UNSTARTABLE=252

. /etc/rc.d/rc.transmission-daemon.conf



function start()
{
 	echo -n "Starting $DAEMON_PATH..."
	if sudo -u lockywolf "$DAEMON_PATH" -a '10.0.0.*,192.168.*.*,10.0.2.*' --config-dir "$DAEMON_CONFIG_DIR" -c "$DAEMON_TORRENT_FILES_DIR" --encryption-required --rpc-bind-address "$DAEMON_LISTEN_ADDRESS" --no-portmap --dht --incomplete-dir "$DAEMON_INCOMPLETE_DIR" --download-dir "$DAEMON_DOWNLOAD_DIR" --auth --username "$DAEMON_USER" --password "$DAEMON_PASSWORD" --utp --logfile "$DAEMON_LOGFILE"
        then
            DAEMON_PID="$!"
            printf "%s" "$DAEMON_PID" > "$PID_FILE"
            printf "Done! (Warning: no portmapping!)\n"
        else
            printf "Failed to start $PROGRAM_NAME\n"
            return "$EXIT_UNSTARTABLE"
        fi
        return "$EXIT_SUCCESS"
}

function stop()
{
 	echo -n "Stopping $DAEMON_PATH ..."
	if (pgrep "$PROGRAM_NAME" | grep "$(cat $PID_FILE)")
	then
	    if kill --signal TERM --timeout 1000 KILL "$(cat $PID_FILE)"
            then
		printf "Done!\n"
                return "$EXIT_SUCESS"
            else
                printf "Failed to kill $PROGRAM_NAME with pid $(cat $PID_FILE)\n"
                return "$EXIT_UNKILLABLE"
            fi
	else
	    printf "Failed! $DAEMON_NAME not running! Or rather, we do not have cgroups.\n"
            #return "$EXIT_NOTRUNNING"
	fi
        killall "$PROGRAM_NAME"
}

function restart()
{
	stop
	sleep 1
	start
}

case $1 in
start)
	start;;
stop)
	stop;;
restart)
	restart;;
*)
	echo "Usage: $(basename $0) start | stop | restart"
	exit 1;;
esac
