#!/bin/bash

# https://gist.github.com/nat-418/135a62fb9f37cc87cd70af1ab72e276a
# This file should be loaded from ~/.config/autostart/hyper-key.desktop
# Some Linux distributions like Ubuntu have Hyper set to the same mappings
# as Super (Mod4), so we need to unset those
xmodmap -e "remove Mod4 = Hyper_L"
# Set Escape to be the left Hyper key
xmodmap -e "keycode 94 = Hyper_L"
# Set Hyper_L to use the normally unused Mod3
xmodmap -e "add Mod3 = Hyper_L"

# I use Hyper only for movement in Emacs
