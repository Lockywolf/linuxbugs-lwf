#!/bin/bash
#
# sample script to use autossh to open up a remote screen
# session, or reconnect to an existing one.
#
# $Id: rscreen,v 1.4 2002/05/07 17:54:13 harding Exp $
#
# Tuned by lockywolf in 2019, 2020
# Adapted for tmux in 2021
# Added xpra in 2022-11-16

if [ "X$1" = "X" ]; then
	echo "usage: $(basename "$0") <host> [--xpra] [<ssh-args>]..."
	exit 1
fi

DESTINATION="$1"

shift 1

l_xpra_remote_string=""
l_xpra_postfix=""
if [[ x"$1" == x"--xpra" ]] ; then
  shift 1
  l_port=9999
  for (( l_port = 10100 ; l_port <= 11001 ; l_port++ )); do
    if ! /usr/sbin/ss -4 -ln --tcp | awk 'NR>1{print $4;}'  | grep -F :"$l_port" ; then
      break
    fi
    if (( l_port == 11000 )) ; then exit 1 ; fi
  done
  # if (( l_port > 10999 )) ; then exit 1 ; fi
  # this fixed post 9999 is a bad idea, but what can I do? I guess there should be some way to forward a UNIX socket?
  l_xpra_remote_string='if type xpra >/dev/null ; then echo found remote xpra ; else echo No XPRA ; exit 1 ; fi ; xpra start :100 --bind-tcp=127.0.0.1:9999 ; export DISPLAY=:100 ; '
  l_xpra_postfix=' ; xpra detach'
  # this "sleep 10" is the bad part here, but I do not know how to make LocalCommand wait until the socket is the actual socket.
  set -- "$@" -oLocalForward="$l_port 127.0.0.1:9999" -oPermitLocalCommand=yes -oLocalCommand="(sleep 10 ; xpra attach tcp://127.0.0.1:$l_port >/tmp/lwf_xpra.log 2>&1 & exit)& echo launched ; disown ; exit"
fi

if [ "X$SSH_AUTH_SOCK" = "X" ]; then
    eval "$(ssh-agent -s)"
    ssh-add "$HOME"/.ssh/id_rsa
fi


AUTOSSH_POLL=45
#AUTOSSH_PORT=20000
#AUTOSSH_GATETIME=30
#AUTOSSH_LOGFILE=$HOST.log
#AUTOSSH_DEBUG=yes 
#AUTOSSH_PATH=/usr/local/bin/ssh
export AUTOSSH_POLL AUTOSSH_LOGFILE AUTOSSH_DEBUG AUTOSSH_PATH AUTOSSH_GATETIME AUTOSSH_PORT
#ssh-add
MYHOST=$(hostname | sed 's/\./_/g' )
SOCKETS="\"SSH_AUTH_SOCK SSH_CONNECTION DISPLAY\""
# you also have to run C-z :utf8 on when you log in. Why just -U is not enough, I do not know.
# 2021-03-04: removed -U, because OpenBSD started nagging after an update.
autossh -M 0 -t "$DESTINATION" \
        -o ServerAliveInterval=15 \
        -o ServerAliveCountMax=10 \
        -o ExitOnForwardFailure=yes \
        "$@" \
        "$l_xpra_remote_string""LC_ALL=en_US.UTF-8 tmux -u new-session -s std -d \; set-option -g prefix C-z \; set -ga update-environment $SOCKETS ; tmux attach -t std \; set -ga update-environment $SOCKETS\; ""$l_xpra_postfix"
