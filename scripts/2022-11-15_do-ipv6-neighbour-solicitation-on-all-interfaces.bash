#!/usr/bin/bash
#+Time-stamp: <2022-11-15 13:19:15 lockywolf>
#+created: <2022-11-15 Tue 13:00>
#+author: lockywolf
#+category: admin

declare -a l_ifs
readarray l_ifs < <(/sbin/ip -6 -j address | jq -r '.[] | .ifname ')

for l_if in ${l_ifs[@]} ; do
  echo $l_if
  declare -a l_addrs
  readarray l_addrs < <(/sbin/ip -6 -j address show dev "$l_if" | \
                         jq -r  '.[0].addr_info[].local')
  for l_addr in ${l_addrs[@]} ; do
    echo $l_addr
    ping -c 4 -6 -I "$l_addr" ff02::1%"$l_if"
  done
done



