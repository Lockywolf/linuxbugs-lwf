#!/usr/bin/env bash
# -*- mode:sh; -*-
# Time-stamp: <2022-02-08 17:04:27 lockywolf>
#+title: Check if the last shutdown was graceful or a crash.
#+author: lockywolf
#+date:
#+created: <2022-02-08 Tue 16:56>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :linux:openbsd:slackware:reboot:system:management
#+creator: Emacs 27.1


MY_SYS="$(uname)"

if [[ x"$MY_SYS" == x"Linux" ]]
then
  OPTS="-xn2"
elif [[ x"$MY_SYS"  == x"$OpenBSD" ]]
then
  OPTS="-n2"
else
  printf "Unsupported system, only Linux or OpenBSD are supported.\n" &>2
fi

STATUS=$(last $OPTS shutdown reboot | awk 'FNR==1 || FNR==2 {printf $1;}')

if [[ x"$STATUS" == x"rebootshutdown" ]]
then
  # graceful reboot
  printf "Last shutdown was graceful.\n"
  exit 0
else
  # hard reboot
  printf "Last boot was hard.\n"
  exit 1
fi

