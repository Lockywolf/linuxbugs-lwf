#!/usr/bin/env bash

size_in_bytes=$((5 * 1000 * 1000 * 1000))
timestamp_now=$(date +'%s')
timestamp_old=$(( timestamp_now - ( 7 * 24 * 60 * 60 )))

l_dir="$(pwd -P)"

if [[ $l_dir = *Telegram* ]]; then
  echo "<$l_dir> contains <substring>"
else
  echo "<$l_dir> does not contain <substring>"
  exit 1
fi

l_find_output="$(find "$l_dir" -maxdepth 1 -mindepth 1 -type f -exec stat --format='%Z=ctime %s=size %n=name' {} + | sort)"

printf "%s" "$l_find_output"

printf "TODO:UNFINISHED\n"

exit 1

