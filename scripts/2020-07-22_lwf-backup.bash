#!/bin/bash
#+date: <2020-12-29 Tue>
#+author: lockywolf
#+updated: <2021-12-29 Wed 22:17> added network backup.
# This seems to be working now. The speed is horrible (~3MB/s), but it is passable
#for my case.
# 2024: found the reason for the slow speed.
# The controllers need _really_ good cooling.

# If you need to replace uas with usb-storage (may help or may not)
# blacklist uas in modprobe.d or something.
# modprobe -v usb-storage 'quirks=2109:0715:u' # via ugreen enclosure
# https://github.com/gnubee-git/GnuBee_Docs/issues/98


l_s=$(basename "$0")
l_scriptname=$(printf '%s' "$l_s" | sed -E 's|[^[:alnum:]._]/||g')
l_script_with_param=$l_scriptname
for param in "${@}" ; do
    p=$(printf '%s' "$param" | sed -E 's|[^[:alnum:]._]/||g')
    l_script_with_param="$l_script_with_param"_"$p"
done

function mylog
(
    printf "%s: %s: " "$(date --iso=seconds)" "$l_script_with_param"
    printf "$@"
)

LOGFILE="/var/log/lockywolf/$l_script_with_param".log
mkdir -p "$(dirname "$LOGFILE")"
echo "" > "$LOGFILE"
COUNTERFILE="/var/log/lockywolf/$l_script_with_param".counter
MAIL_LOG=0
export DISPLAY=:0
trap 'pkill -P $$' EXIT

# kbackup is icon name
/home/lockywolf/Syncthing_2021/laptop.lockywolf.net/DevLinux/lwf_private-scripts_private-repo/imap-idle-mail-checker/2021-08-21_tray-icon.py "Backup: $*
$(date --iso=minutes)"  drive-harddisk 2>&1  | sed "s/^/lwf_backup-disks-laptop:child:$1:/g" | ts "%Y-%m-%dT%H:%M:%S%z"  &
disown

mylog "LOGFILE=%s\n" "$LOGFILE" | tee -a "$LOGFILE"
mylog "COUNTERFILE=%s\n" "$COUNTERFILE" | tee -a "$LOGFILE"
if grep -q '[^0-9]' "$COUNTERFILE"; then
  mylog "Error: COUNTERFILE (%s)  contains not only numbers.\n" "$COUNTERFILE"  | tee -a "$LOGFILE" 1>&2
  head -n10 "$COUNTERFILE" | cut -z -c '1-1000' | tee -a  "$LOGFILE" 1>&2
  exit 0
fi

if [[ "$(hostname)" != "laptop.lockywolf.net" ]]
then
  printf "This script is only intended to run on the laptop!\n" | tee -a "$LOGFILE" 1>&2
  exit 1
fi

mylog "Trying to backup: %s on %s\n" "$1" "$2" | tee -a "$LOGFILE"

HOST=""
if [[ x"$2" != x"" ]]
then
    mylog "Apparently, we want to backup over the network, and the host is %s.\n" "$2" | tee -a "$LOGFILE"
    HOST="$2"

    if arping -c 1 "$HOST" > /dev/null
    then
        mylog "The host seems on the same network, good! Proceeding.\n" | tee -a "$LOGFILE"
    elif ping -c 3 "$HOST" > /dev/null
    then
        mylog "The host seems NOT on the same network, but it is at least up!\n" | tee -a "$LOGFILE"
        if [[ "$3" == "--force" ]]
        then
            mylog "You decided to force backup over a remote channel! Ok.\n" | tee -a "$LOGFILE"
        else
            mylog "Not backup-ing stuff over remote channel.\n" | tee -a "$LOGFILE" 1>&2
            exit 0
        fi
    else
        mylog "Not backup-ing stuff over remote channel.\n" | tee -a "$LOGFILE" 1>&2
        exit 0
    fi
else
  mylog "Apparently, we want to backup locally.\n" | tee -a "$LOGFILE"
  HOST="localhost"
fi


declare FROM
declare TO
declare EXCLUDES

#
L_HOME_EXCLUDES=(--exclude='*position.glass' \
--exclude='*termlist.glass' \
--exclude='*postlist.glass' \
--exclude='*.mbsync/*journal' \
--exclude='*.mbsync/*lock' \
--exclude='*.mbsync/*new' \
--exclude='**chromium/component_crx_cache/**' \
--exclude='**chromium/Default/Extension State/*.ldb' \
--exclude='**chromium/Default/Extension State/*.log' \
--exclude='**chromium/Default/IndexedDB**' \
--exclude='**chromium/Default/Local Extension Settings**' \
--exclude='**chromium/Default/Local Storage/leveldb/*.log' \
--exclude='**chromium/Default/Local Storage/leveldb/*.ldb' \
--exclude='**chromium/Default/Sessions/**' \
--exclude='**chromium/Default/Session Storage/*.log' \
--exclude='**chromium/Default/Session Storage/*.ldb' \
--exclude='**chromium/Default/Service Worker**' \
--exclude='**chromium/Default/.org.chromium.Chromium.*'
--exclude='**chromium-nowg/Default/Cache/Cache_Data**' \
--exclude='**chromium-nowg/component_crx_cache/**' \
--exclude='**chromium-nowg/Default/IndexedDB**' \
--exclude='**chromium-nowg/Default/Local Extension Settings/*/*.log' \
--exclude='**chromium-nowg/Default/Local Extension Settings/**.ldb'
--exclude='**chromium-nowg/Default/Local Storage/leveldb/**' \
--exclude='**chromium-nowg/Default/Sessions/Session_**' \
--exclude='**chromium-nowg/Safe Browsing/**' \
--exclude='**chromium-ungoogled-tor/Default/Local Extension Settings/**.log' \
--exclude='**chromium-ungoogled-tor/Default/Local Extension Settings/**.ldb' \
--exclude='**chromium-ungoogled-tor/Default/Local Storage/leveldb/**' \
--exclude='**gvfs-metadata/**.log' \
--exclude='**skypeforlinux/IndexedDB/file__0.indexeddb.**' \
--exclude='**skypeforlinux/Local Storage/leveldb/**' \
--exclude='**skypeforlinux/Cache/*' \
--exclude='**discord/Local Storage/leveldb/*.l*' \
--exclude='**discord/Cache/**.l*' \
--exclude='**.thunderbird/*/saved-telemetry-pings/**' \
--exclude='**.mozilla/firefox/*/bookmarkbackups/bookmarks-*.jsonlz4' \
--exclude='**.mozilla/firefox/*/saved-telemetry-pings/**' \
--exclude='**.mozilla/firefox/*/storage/default/moz-extension**.sqlite**' \
--exclude='**.mozilla/firefox/*/storage/default/moz-extension**/idb/**' \
--exclude='**.mozilla/firefox/*/storage/default/*/cache/**' \
--exclude='**.mozilla/firefox/*/storage/default/**/cache' \
--exclude='**.mozilla/firefox/*/storage/default/*.microsoft.com*' \
--exclude='**.mozilla/firefox/*/storage/permanent/chrome/idb/*.sqlite-*' \
--exclude='**.wine/drive_c/users/*/Application Data/Tencent/WeChat/1/kvcomm/monitordata**' \
--exclude='**.wine/drive_c/users/*/Application Data/Tencent/WeChat/1/kvcomm/key_*.statistic' \
--exclude='*.cache.sync*' \
--exclude='**.stversions**' \
--exclude='**.config/Slack/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Android/data/com.skype.raider/cache/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Android/data/io.mrarm.irc/files/chat-logs/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Android/data/org.telegram.messenger/cache/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Android/data/org.forkgram.messenger/cache/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Android/data/com.tencent.mm/cache/**' \
--exclude='**Syncthing_2021/Oneplus-5t-sdcard/Telegram/Telegram Images/-*' \
--exclude='**Personal_Planner/.git/index.lock' \
--exclude='**/.android/' \
--exclude='**.config/Recoll.org/tneedidxretrydate' )

L_ROOT_EXCLUDES=(--exclude='/var/cache/man/**' \
--exclude='/var/cache/logwatch/**' \
--exclude='/var/lib/docker/**' \
--exclude='/var/lib/smokeping/**' \
--exclude='/var/lib/syncthing/config/**log' \
--exclude='/var/lib/syncthing/config/**ldb' \
--exclude='/var/lib/GeoIP/.Geo*' \
--exclude='/var/log/pcp/pmfind/**' \
--exclude='/var/log/pcp/pmie/**' \
--exclude='/var/log/pcp/pmlogger/**' \
--exclude='/var/spool/postfix/defer**' \
--exclude='/var/spool/postfix/active**' \
--exclude='/var/tmp/**' )

L_TONAME="$1"

expected_percent_used=100
expected_percent_used_root=50
expected_percent_used_home=40
expected_percent_used_efi=1
case $1 in
     "failsafe-home")
         mylog "Trying to backup home.\n" | tee -a "$LOGFILE"
         FROM="/home"
         TO="/mnt/failsafe_home"
         # EXCLUDES=(--exclude='*position.glass' --exclude='*termlist.glass' --exclude='*postlist.glass' --exclude='**chromium-nowg/Default/Cache/Cache_Data**' --exclude='/home/lockywolf/.config/skypeforlinux/IndexedDB/file__0.indexeddb.blob/**' --exclude='/home/lockywolf/.config/skypeforlinux/Local Storage/leveldb/**log' --exclude='**home/lockywolf/Syncthing_2021/Oneplus-5t-sdcard/.stversions/**' --exclude='/home/lockywolf/Syncthing_2021/Oneplus-5t-sdcard/Android/data/com.tencent.mm/cache/**')
         EXCLUDES=( "${L_HOME_EXCLUDES[@]}" )
         HOST=localhost
         FROM_DEV_ID_EXPECTED="/dev/disk/by-id/nvme-Samsung_SSD_990_PRO_4TB_S7DPNJ0WC28510B-part1"
         expected_percent_used=$expected_percent_used_home
         ;;
     "failsafe-root")
         mylog "Trying to make a failsafe_root backup.\n" | tee -a "$LOGFILE"
         FROM="/"
         TO="/mnt/failsafe_root"
         EXCLUDES=( "${L_ROOT_EXCLUDES[@]}" )
         HOST=localhost
         FROM_DEV_ID_EXPECTED="/dev/disk/by-id/nvme-Samsung_SSD_990_PRO_4TB_S7DPNJ0WC28510B-part2"
         expected_percent_used=$expected_percent_used_root
         ;;
     "failsafe-efi")
         mylog "Trying to make a failsafe_efi backup.\n" | tee -a "$LOGFILE"
         FROM="/boot/efi"
         TO="/mnt/failsafe_efi"
         EXCLUDES=()
         HOST=localhost
         FROM_DEV_ID_EXPECTED="/dev/disk/by-id/nvme-Samsung_SSD_990_PRO_4TB_S7DPNJ0WC28510B-part4"
         expected_percent_used=$expected_percent_used_efi
         ;;
     "backup-home")
         mylog "Trying to backup home onto a separate drive.\n" | tee -a "$LOGFILE"
         FROM="/home"
         TO="/mnt/backup-$(hostname -s)-home"
         EXCLUDES=( "${L_HOME_EXCLUDES[@]}" )
         FROM_DEV_ID_EXPECTED="/dev/disk/by-id/nvme-Samsung_SSD_990_PRO_4TB_S7DPNJ0WC28510B-part1"
         expected_percent_used=$expected_percent_used_home
         ;;
     "backup-root")
         mylog "Trying to backup home onto a separate drive.\n" | tee -a "$LOGFILE"
         FROM="/"
         TO="/mnt/backup-$(hostname -s)-root"
         EXCLUDES=( "${L_ROOT_EXCLUDES[@]}" \
                      --exclude='/etc/fstab' )
         FROM_DEV_ID_EXPECTED="/dev/disk/by-id/nvme-Samsung_SSD_990_PRO_4TB_S7DPNJ0WC28510B-part2"
         expected_percent_used=$expected_percent_used_root
         ;;
     *)
         mylog "%s not from the list. What do you want? Maybe update the script?\n" "$1"  | tee -a "$LOGFILE" 1>&2
         exit 0
         ;;
esac

FROM_DEV_EXPECTED=$(readlink -f "$FROM_DEV_ID_EXPECTED")
if ! grep -F "$FROM_DEV_EXPECTED ${FROM} " /proc/mounts ; then
  mylog "Source device (%s) mismatch. Expected %s, got %s\n"\
         "$FROM_DEV_ID_EXPECTED" "$FROM_DEV_EXPECTED"\
         "$(grep -F " $FROM " /proc/mounts)" | tee -a "$LOGFILE" 1>&2
  mylog "Exiting\n"  | tee -a "$LOGFILE" 1>&2
  exit 0
fi

percent_used=$(df -B1 $FROM_DEV_ID_EXPECTED | tail -n1 | awk '{print $5;}' | tr -d '%')
if (( percent_used < expected_percent_used )) ; then
  mylog "%s almost empty: %d%%! Investigate!\n" "$FROM" "$percent_used" 1>&2
  exit 0
fi

ssh "$HOST" "umount ${TO@Q}" > /dev/null 2>/dev/null

if ssh "$HOST" "mount ${TO@Q}"  ; then
  mylog "Destination mounted: %s\n" "$TO"  | tee -a "$LOGFILE"
else
  mylog "Failed to mount %s.\n" "$TO"  | tee -a "$LOGFILE" 1>&2
  exit 0
fi

_TO="${TO%/}"
if ssh "$HOST" "grep ${_TO@Q} /proc/mounts" ; then
    mylog "Destination is a mount point, ready to backup.\n" | tee -a "$LOGFILE"
else
    mylog "Destination not a mount point. Bailing out.\n" | tee -a "$LOGFILE" 1>&2
    exit 0
fi
# --whole-file --atimes

if [[ "$HOST" != "localhost" ]]
then
    _TO="${HOST}":"${TO}"
    mylog "DEBUG: _TO=%s\n" "$_TO"  | tee -a "$LOGFILE"
fi


#printf "DEBUG: running:"
#echo time rsync --archive --hard-links --acls --atimes --xattrs --no-i-r --whole-file \
#     --delete-before --delete-excluded --one-file-system --human-readable \
#     --stats "${EXCLUDES[@]}" "${FROM}" "${_TO}"

# --verbose --info=progress2 --progress
#set -x
mylog "Running rsync\n\n"  | tee -a "$LOGFILE"
l_rsync_start_secs=$(date '+%s') # --info=misc0
rsync_exit_status=""

export RSYNC_RSH='ssh -oServerAliveInterval=15 -oServerAliveCountMax=10'
timeout --kill-after=30s 180m  rsync \
       --no-i-r \
       --partial \
       --human-readable\
       --archive --hard-links --acls --atimes --xattrs --whole-file \
       --delete  --one-file-system \
       --stats "${EXCLUDES[@]}" "${FROM}"/ "${_TO}"  >> "$LOGFILE"
rsync_exit_status="$?"

l_rsync_end_secs=$(date '+%s')
mylog "%s\n" "<lwf:SKIP long rsync>"
tail -n 20 "$LOGFILE" # not appending to LOGFILE, because already there.

if [[ $rsync_exit_status == "" ]]; then
  mylog "Rsync has no exit status! TODO and check.\n"  | tee -a "$LOGFILE" 1>&2 ;
fi
if (( rsync_exit_status == 24 )) ; then
  mylog "Rsync exit status = 24. This is not fatal.\n"
  rsync_exit_status=0
fi
if (( rsync_exit_status != 0 )) ; then
  mylog "Rsync failed, exit status=%d.\n" "$rsync_exit_status" 1>&2
  MAIL_LOG=1
else
  mylog "Rsync succeeded.\n"
fi

printf '\n' | tee -a "$LOGFILE"
mylog 'Rsync from %s to %s took %s seconds.\n' "${FROM}" "${_TO}" $(( l_rsync_end_secs - l_rsync_start_secs ))  | tee -a "$LOGFILE"


case $L_TONAME in
  "failsafe-home")
    mylog "Running post-backup for previously backup home.\n"  | tee -a "$LOGFILE"
    mylog "Doing nothing.\n"  | tee -a "$LOGFILE"
    ;;
  "failsafe-root")
    mylog "Running post-backup for previously make a failsafe_root backup.\n"  | tee -a "$LOGFILE"
    mylog "TODO: We need to rewrite root.\n"  | tee -a "$LOGFILE"
    # no swap on failsafe boot
    sed -i  '/swap/d' "$_TO"/etc/fstab
    # no failsafe anything on the failsafe boot
    sed -i  '/failsafe_root/d' "$_TO"/etc/fstab
    sed -i  '/failsafe_home/d' "$_TO"/etc/fstab
    sed -i  '/failsafe_efi/d' "$_TO"/etc/fstab
    # replacing root (/) PARTUUID
    sed -i  's|72c6c620-3ee1-4e25-9d05-dad5e24b05f7    /  |433e79b9-6afc-ee4b-8a17-b835d896566c    /  |g' "$_TO"/etc/fstab
    # replacing efi (/boot/efi) PARTUUID
    sed -i  's|2af97b48-dcc4-4dc0-b916-53784e7722ea    /boot/efi |8806c263-3cb8-3d42-8f12-ee95df594e55    /boot/efi |g' "$_TO"/etc/fstab
    # replacing home (/home) PARTUUID
    sed -i  's|9bca8a25-5988-4bbc-aaa0-06d45da7e2c3    /home |e4d881cd-0e6d-3e46-b49d-9ddcb6d6c86f    /home |g' "$_TO"/etc/fstab
    ;;
  "failsafe-efi")
    mylog "Running post-backup for previously make a failsafe_efi backup.\n"  | tee -a "$LOGFILE"
    # refind "volume" and root=
    sed -i 's/72c6c620-3ee1-4e25-9d05-dad5e24b05f7/433e79b9-6afc-ee4b-8a17-b835d896566c/g' "$_TO"/EFI/refind/refind.conf
    ;;
  "backup-home")
    mylog "Running post-backup for previously backup home onto a separate drive.\n" | tee -a "$LOGFILE"
    mylog "Doing nothing.\n" | tee -a "$LOGFILE"
    ;;
  "backup-root")
    mylog "Running post-backup for previously backup home onto a separate drive.\n" | tee -a "$LOGFILE"
    mylog "Doing nothing.\n"
    ;;
  *)
    mylog "This place should not be reachable.\n Post-backup for broken subtree: %s. \n" "$1" | tee -a "$LOGFILE" 1>&2
    exit 0
    ;;
esac


#time rsync --archive --hard-links --atimes --acls --xattrs --inplace --one-file-system --fuzzy --human-readable --info=progress2 --progress --partial /home/ /mnt/backup_home

ssh "$HOST" sync 2>&1 | tee -a "$LOGFILE" 1>&2
sleep 5

if ! ssh "$HOST" "umount ${TO@Q}" 2>&1 | tee -a "$LOGFILE" 1>&2 ; then
  mylog "%s failed! \n" "ssh $HOST umount ${TO@Q}" | tee -a "$LOGFILE" 1>&2
else
  mylog "%s succeeded\n" "ssh $HOST umount ${TO@Q}" | tee -a "$LOGFILE"
fi

mylog "Backup script done.\n"  | tee -a "$LOGFILE"

echo $(( $(cat "$COUNTERFILE" 2>/dev/null ) + 1 )) > "$COUNTERFILE"
mylog "COUNTERFILE value=%d\n" "$(cat "$COUNTERFILE")" | tee -a "$LOGFILE"
if (( "$(cat "$COUNTERFILE")" > 15 )) ; then
    if (( rsync_exit_status == 0 )) ; then
        mylog "Counterfile overflow (%d). Mailing log!\n" "$(cat "$COUNTERFILE")" 1>&2
    fi
    MAIL_LOG=1
    echo 0 > "$COUNTERFILE"
fi

if (( MAIL_LOG == 1 )) ; then
    if (( $(stat --format='%s' "$LOGFILE") > 1000000 )) ; then
      mylog "%s\n" "LOGFILE size greater than 1M. "\
"Not sending. See $(hostname) $LOGFILE" 1>&2
    else
        mylog "LOGFILE below:\n" 1>&2
        cat "$LOGFILE" 1>&2
    fi
fi


exit 0 # this is for cron's AFTER feature.
