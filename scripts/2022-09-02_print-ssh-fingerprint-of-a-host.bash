#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2022-09-02 09:40:12 lockywolf>
#+title: Print ssh fingerprint of a host. Use $2 as the algo, or ED25519.
#+author: lockywolf (based on StackOverflow)
#+date: unpublished
#+created: <2022-09-02 Fri 09:37>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :admin:programming:bash:ssh:remote:crypto
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


ALG="${2:-ED25519}"

ssh-keyscan "$1" 2>/dev/null | ssh-keygen -lf - | grep "$ALG" | awk '{print $2;}'
