#!/bin/bash -x

set -euo pipefail
declare KERNEL_VERSION
KERNEL_VERSION="$(find /boot -maxdepth 1 -iname 'vmlinuz-huge*' -type f | sort -rV | sed 1q | cut -c 20-)"
printf "Last kernel: %s\n" "$KERNEL_VERSION"

  # -i wireguard-linux-compat:KERNEL="$KERNEL_VERSION" \

/usr/sbin/sbopkg -B \
		 -i nvidia-kernel:KERNEL="$KERNEL_VERSION" <<"EOF"
c
c
c
EOF

