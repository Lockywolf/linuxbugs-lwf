#!/usr/bin/bash

function show_help()
(
  printf "Usage: %s directory n_backups\n" "$0"  1>&2
)

die() {
    printf '%s\n' "$1" >&2
    exit 1
}

#l_original_args=("${@[@]}")

while :; do
    case $1 in
        --help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        --actually-delete)
            l_actually_delete=true
            ;;
        --directory)       # Takes an option argument; ensure it has been specified.
            if [[ "$2" ]]; then
                l_directory=$2
                shift
            else
                die 'ERROR: "--directory" requires a directory as an option argument.'
            fi
            ;;
        --directory=?*)
            l_directory=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --directory=)         # Handle the case of an empty --file=
            die 'ERROR: "--directory" requires a non-empty option argument.'
            ;;
        --number)       # Takes an option argument; ensure it has been specified.
            if [[ "$2" ]]; then
                l_number=$2
                shift
            else
                die 'ERROR: "--number" requires an integer as an option argument.'
            fi
            ;;
        --number=?*)
            l_number=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --number=)         # Handle the case of an empty --file=
            die 'ERROR: "--number" requires a non-empty option argument.'
            ;;
        -v|--verbose)
            verbose=$((verbose + 1))  # Each -v adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

if [[ x"$l_directory" == x"" || x"$l_number" == x"" ]] ; then
  show_help
  exit 1
fi


# Let me explain the logic here.
# 1. There should be "yearly" backups. Those are not size-limited, and are expected
#    to grow as time progresses. Hopefully, 300Mb per year is not too much.
# 2. Yearly backups start at the day of the first backup, and are expected to be
#    refreshed as soon as a year passes.
# 3. The next yearly backup is added when $now is one year away from $last_yearly_backup_date
#    and the backup added is new newest backup that is older than today, older than
#    one year between the previous yearly backup, and newer than the previous
#    backup, if it exists. If not, today's backup is used.
# 4. If the difference between the last yearly backup and today is still >1year,
#    the procedure is repeated, until $last_yearly_backup-$today is less than 1 year.
#
# Next, backup files are checked if their number is less than N, and rotated if more.
# Backups are expected to back off exponentially in time, and are deleted one by one,
# trying to enforce this distribution.
# 1. Partition the time between "$now" and "$1_year_ago" into two equal intervals A
#    (newer half-year) and B (older half-year).
# 2. If nbackups(A)/2 < nbackups(B) , select B, else select A.
# 3. Repeat the selection procedure with each selected interval, until you find
#    A and B which have 1 and 0 backups respectively. Delete the backup which is the
#    only one left.

L_DIR="$l_directory"
L_NUMBER="$l_number"

L_STAGING_DIR="$L_DIR"/staging
L_YEARLY_DIR="$L_DIR"/yearly
L_DELETED_DIR="$L_DIR"/deleted

if [[ ! -e "$L_DIR" ]] ; then printf "Directory %s does not exist.\n" "$L_DIR"; exit 3 ; fi
if (( L_NUMBER == 0 )) ; then printf "What do you want to rotate?\n"; exit 1 ; fi
if (( L_NUMBER == 1 )) ; then printf "What do you want to rotate?\n"; exit 2 ; fi

mkdir -p "$L_STAGING_DIR"
mkdir -p "$L_YEARLY_DIR"
mkdir -p "$L_DELETED_DIR"

#function rm()
#( printf "Trying to rm %s\n" "$*" )
# function ln()
# ( printf "Trying to ln %s\n" "$*" )

printf "\nWelcome to the rotation script:%s .\n" "$0" 

readarray -t -d '' l_staging_files < <(find "$L_STAGING_DIR" -maxdepth 1 -mindepth 1 -type f -print0 | sort -z -V)

if (( ${#l_staging_files} == 0 )) ;  then
  printf "Not staging files at all! Not possible to rotate backups.\n" 1>&2
  exit 1
fi


l_date_regex='([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})(T.*)?_(.*)'

for idx in "${!l_staging_files[@]}" ; do
  if [[ $(basename "${l_staging_files[idx]}") =~ $l_date_regex ]] ; then
    printf "File to rotate: %s\n" "${l_staging_files[idx]}"
    l_year="${BASH_REMATCH[1]}"
    l_month="${BASH_REMATCH[2]}"
    l_day="${BASH_REMATCH[3]}"
    l_staging_dates[idx]=$(date --date="$l_year"-"$l_month"-"$l_day" '+%s')
    #    printf "Ok! Year=%s, month=%s, day=%s\n" "$l_year" "$l_month" "$l_day";
  else
    printf "Does not match! You have bogus files in the rotation dir. Do something!\n" ; exit 4 ; fi
done

# Do yearly stuffs.

# printf "\nDebug1\n"
# readarray -t -d '' l_yearly_files < <(find "$L_YEARLY_DIR" -maxdepth 1 -mindepth 1 -type f -print0 | sort -z -V)
# for f in "${l_yearly_files[@]}" ; do
#   #printf "Yearly file: %s\n" "$f"
#   if [[ $f =~ $l_date_regex ]] ; then
#     l_year="${BASH_REMATCH[1]}" ; l_month="${BASH_REMATCH[2]}"; l_day="${BASH_REMATCH[3]}"
# #    printf "Ok! Year=%s, month=%s, day=%s\n" "$l_year" "$l_month" "$l_day";
#  #   printf "Date=%s\n" "$l_newest_yearly_filedate"
#   else
#     printf "Does not match! You have bogus files in the yearly dir. Do something!\n" 1>&2 ;
#     exit 4 ;
#   fi
# done
# l_newest_yearly_filedate=$(date --date="$l_year"-"$l_month"-"$l_day" '+%s')
# printf "Newest yearly file: %s (%s)\n" "$l_newest_yearly_filedate" $(date --date=@"$l_newest_yearly_filedate" --iso=date)
# exit 255

function compute_newest_yearly_filedate() {
readarray -t -d '' l_yearly_files < <(find "$L_YEARLY_DIR" -maxdepth 1 -mindepth 1 -type f -print0 | sort -z -V)
local l_newest_yearly_filedate=0
local l_year
local l_month
local l_day
for f in "${l_yearly_files[@]}" ; do
  #printf "Yearly file: %s\n" "$f"
  if [[ $(basename "$f") =~ $l_date_regex ]] ; then
    l_year="${BASH_REMATCH[1]}" ; l_month="${BASH_REMATCH[2]}"; l_day="${BASH_REMATCH[3]}"
#    printf "Ok! Year=%s, month=%s, day=%s\n" "$l_year" "$l_month" "$l_day";
 #   printf "Date=%s\n" "$l_newest_yearly_filedate"
  else
    printf "Does not match! You have bogus files in the yearly dir. Do something!\n" 1>&2 ;
    exit 4 ;
  fi
done
l_newest_yearly_filedate=$(date --date="$l_year"-"$l_month"-"$l_day" '+%s')
printf "%s" "$l_newest_yearly_filedate"
}

L_NOW="$(date '+%s')"

function is_filedate_older_than_one_year() (
  local -r l_newest_yearly_filedate=$1
  if (( (L_NOW - l_newest_yearly_filedate) > (60 * 60 * 24 * 365) )) ; then
    return 0
  else return 1 ; fi )

l_newest_yearly_filedate=$(compute_newest_yearly_filedate)
printf "Newest yearly filedate=%s (%s)\n" "$l_newest_yearly_filedate" "$(date --date=@"$l_newest_yearly_filedate" --iso=date)"
printf "Newest staging filedate=%s (%s)\n" "${l_staging_dates[-1]}" "$(date --date=@"${l_staging_dates[-1]}" --iso=date)"

hardstop=10

while is_filedate_older_than_one_year  "$l_newest_yearly_filedate" &&
    (( l_newest_yearly_filedate < l_staging_dates[-1] )) &&
    (( hardstop > 0 )); do
  #hardstop=$(( hardstop - 1 ))
  if (( hardstop == 1 )) ; then
    printf "Rotating 10 years old. Debug!\n" 1>&2
  fi
  printf "Newest yearly backup is old. Offset=%s days. We need to do add some.\n" $(( (L_NOW - l_newest_yearly_filedate)/ (60 * 60 * 24) )) 1>&2
  l_staging_to_become_yearly=-1
  for idx in "${!l_staging_files[@]}"; do
#    printf "idx=%s " "$idx"
    if (( l_staging_dates[idx] <= l_newest_yearly_filedate )) ; then
#      printf "Staging backup is older than the newest yearly.\n"
      :
    elif (( l_staging_dates[idx] < (l_newest_yearly_filedate + (60 * 60 * 24 * 365)) )) ; then
      :
#      printf "Backup %s is less than one year far from the last yearly.\n" "${l_staging_files[idx]}"
    else
      printf "Backup %s is more than one year far from the last yearly.\n" "${l_staging_files[idx]}"
      if (( idx == 0 )) ; then
        l_staging_to_become_yearly=0
      else
        l_staging_to_become_yearly=$((idx - 1)) # (good case)
      fi
      break
    fi
  done
  printf "Adding %s\n" "${l_staging_files[$l_staging_to_become_yearly]}"
  ln "${l_staging_files[$l_staging_to_become_yearly]}" "$L_YEARLY_DIR"/
  l_newest_yearly_filedate=$(compute_newest_yearly_filedate)
done

# yearly stuff done.
# let us do exponential backoff


# I have l_staging_files and l_staging_dates and l_newest_yearly_filedate
# arr=(a b c); unset 'arr[1]'; arr=("${arr[@]}"); declare -p arr


l_upper_bound_init="$L_NOW"
l_lower_bound_candidate1=$(( L_NOW - ((60 * 60 * 24 * 365) * 2) ))
l_lower_bound_candidate2=$(( L_NOW - ((L_NOW - l_staging_dates[0]) * 2) ))
if (( l_lower_bound_candidate1 < l_lower_bound_candidate2 )) ; then
  l_lower_bound_init=$l_lower_bound_candidate1
else
  l_lower_bound_init=$l_lower_bound_candidate2
fi

printf "\nDebug only\n"
printf "Date lower=%s\n" $(date --date=@$l_lower_bound_init --iso=date)
printf "Date upper=%s\n" $(date --date=@$l_upper_bound_init --iso=date)

declare -a l_files_to_delete

l_outer_loop_iter_no=0

l_bound_outer_loop="${#l_staging_files[@]}"

while (( "${#l_staging_files[@]}" > L_NUMBER )) ; do
  if (( l_outer_loop_iter_no > (l_bound_outer_loop - L_NUMBER) )) ; then
    printf "Too many outer iterations!\n" 1<&2
    break
  fi
  l_outer_loop_iter_no=$((l_outer_loop_iter_no + 1))
#  printf "Outer loop iteration %s. Total number of files=%s\n" "$l_outer_loop_iter_no" "${#l_staging_files[@]}"
  nfiles_before_removal="${#l_staging_files[@]}"
  # unset "l_staging_files[-1]"
  # printf "removing...\n"
  #debug_iter=7
  l_lower_bound="$l_lower_bound_init"
  l_upper_bound="$l_upper_bound_init"
  l_iteration_indices=("${!l_staging_files[@]}")
  l_binsearch_iteration=0
  while /bin/true   ; do # && ((debug_iter > 0)) ; do
    # read
    #printf '===== Binsearch iteration %s====\n' "$l_binsearch_iteration"
    if (( l_binsearch_iteration > 15 )) ; then
      printf "Inner loop does not converge.\n" 1>&2
      exit 1
    fi
#    printf "Current iteration indices %s\n" "${l_iteration_indices[@]}"
    l_binsearch_iteration=$(( l_binsearch_iteration + 1 ))
    debug_iter=$((debug_iter - 1))
    l_split_time=$(( (l_upper_bound + l_lower_bound) / 2 ))
    #printf "Date lower=%s\n" $(date --date=@$l_lower_bound --iso=date)
    #printf "Date split=%s\n" $(date --date=@$l_split_time  --iso=date)
    #printf "Date upper=%s\n" $(date --date=@$l_upper_bound --iso=date)
    if (( l_staging_dates[l_iteration_indices[0]] < l_lower_bound )) ; then
      printf "Error: lower bound of files is less than lower bound by time\n"
      exit 5
    fi
    if (( l_staging_dates[l_iteration_indices[-1]] > l_upper_bound )) ; then
      printf "Error: upper bound of files is greater than upper bound by time\n"
      exit 6
    fi

    l_idx_before_barrier=()
    l_idx_after_barrier=()
    for idx in "${l_iteration_indices[@]}" ; do
      if (( l_staging_dates[idx] < l_split_time )) ; then
#        printf "File before the barrier: %s \n" "${l_staging_files[idx]}"
        l_idx_before_barrier+=("$idx")
      else
#        printf "File after the barrier: %s \n" "${l_staging_files[idx]}"
        l_idx_after_barrier+=("$idx")
      fi
    done
    #printf "Files before: %s, after %s\n" "${#l_idx_before_barrier[@]}" "${#l_idx_after_barrier[@]}"
    l_before_samedate_flag=true
    l_after_samedate_flag=true
    for i in "${l_idx_before_barrier[@]}" ; do
      if (( l_staging_dates[i] != l_staging_dates[l_idx_before_barrier[0]] )) ; then l_before_samedate_flag=false ; fi
    done
    for i in "${l_idx_after_barrier[@]}" ; do
      if (( l_staging_dates[i] != l_staging_dates[l_idx_before_barrier[0]] )) ; then l_after_samedate_flag=false ; fi
    done


    if (( "${#l_idx_before_barrier[@]}" == 0 )) && (( "${#l_idx_after_barrier[@]}" == 0 )) ; then
      printf "Error: both binsearch intervals are empty!\n"
      exit 3
    elif [[ $l_before_samedate_flag == true ]] && (( "${#l_idx_after_barrier[@]}" == 0 )) ; then
      #printf "Deleting in older interval.\n"
      l_files_to_delete+=("${l_staging_files[${l_idx_before_barrier[0]}]}")
      unset "l_staging_files[${l_idx_before_barrier[0]}]"
      break;
    elif (( "${#l_idx_before_barrier[@]}" == 0 )) && [[ $l_after_samedate_flag == true ]] ; then
      #printf "Deleting in newer interval.\n"
      l_files_to_delete+=("${l_staging_files[${l_idx_after_barrier[0]}]}")
      unset "l_staging_files[${l_idx_after_barrier[0]}]"
      break;
      # elif (( "${#l_idx_before_barrier[@]}" == 1 )) && (( "${#l_idx_after_barrier[@]}" == 1 )) ; then
    elif [[ $l_before_samedate_flag == true ]] && [[ $l_after_samedate_flag == true ]] ; then
      #printf "(before=after case) Deleting in older interval.\n"
      l_files_to_delete+=("${l_staging_files[${l_idx_after_barrier[0]}]}")
      unset "l_staging_files[${l_idx_after_barrier[0]}]"
      break;
    elif (( "${#l_idx_before_barrier[@]}" > ( ( "${#l_idx_after_barrier[@]}"  * 2 ) / 3 ) )) ; then
      #printf "Working with older interval.\n"
      l_iteration_indices=()
      l_iteration_indices=("${l_idx_before_barrier[@]}")
      l_upper_bound="$l_split_time"
    elif (( "${#l_idx_before_barrier[@]}" <= ( ( "${#l_idx_after_barrier[@]}" * 2 ) / 3 ) )) ; then
      #printf "Working with newer interval (after barier).\n"
      l_iteration_indices=()
      l_iteration_indices=("${l_idx_after_barrier[@]}")
      l_lower_bound="$l_split_time"
    else
      printf "Error: I somehow missed some case in the switch.\n"
      exit 4
    fi
#    printf "New iteration indices %s\n" "${l_iteration_indices[@]}"
  done
  nfiles_after_removal="${#l_staging_files[@]}"
  if (( (nfiles_before_removal - nfiles_after_removal) != 1 )) ; then
    printf "Assertion failed!\n" 1>&2
    exit 1
  fi
#  break
done

for fname in "${l_files_to_delete[@]}" ; do
  printf "To delete: %s\n" "$(basename "$fname")"
  if [[ $l_actually_delete == true ]] ; then
    #mv "$fname" "$L_DELETED_DIR"/
    rm "$fname"
  fi
done | sort -V

printf "\n"

for fname in "${l_staging_files[@]}" ; do
  printf "To keep: %s\n"   "$(basename "$fname")"
done | sort -V

printf "Rotation done.\n"
