#!/bin/bash


if pgrep -fi wxwork
then
  exit 1
fi


mkdir -p /home/lockywolf/.local/var/log/
LOG="/home/lockywolf/.local/var/log/wxwork.stdout+stderr.log"
rm "$LOG"
MESSAGE="LWF: Starting wxwork (WechatBusiness)"
L_TIMEOUT=60
printf "%s \n\n" "$(date --iso=seconds) $MESSAGE" | tee -a "$LOG"
notify-send  --expire-time=$(( "$L_TIMEOUT" * 1000 )) "$MESSAGE"

(
  while true
  do
    export LC_ALL=en_US.utf8
    export WINEPREFIX=~/.wineprefix-qiyeweixin/
    export GTK_IM_MODULE=""
    wine "$HOME/.wineprefix-qiyeweixin/drive_c/Program Files (x86)/WXWork/WXWork.exe"  >> "$LOG" 2>&1
    if [[ x"$PPID" == x"1" ]] ; then exit 1 ; fi
    notify-send  --expire-time=$(( "$L_TIMEOUT" * 1000 )) "WXWork died."
    sleep 60
  done
) &
#   | tee -a "$LOG"

