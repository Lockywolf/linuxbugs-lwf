#!/bin/bash

for i in "$@" ; do
  safename=$(/home/lockywolf/bin/lwf_safe-filename <<< "$i")
  myre="^(.+)(\.avi|\.mp4|\.mov|\.webm|\.mpg|\.mkv)$"
  if [[ "$safename" =~ $myre ]]; then
    base="${BASH_REMATCH[1]}"
    ext="${BASH_REMATCH[2]}"
    printf "The file ends with a known video file extension: %s\n" "${ext}"
    mv "$i" "$base".original"$ext"
  else
    printf "The file does NOT end with a known video file extension.\n"
    continue
  fi

  ffmpeg -i "$base".original"$ext" -c:v libx264\
       -tune fastdecode \
       -profile:v baseline -level 3.0 -movflags +faststart \
       "$base".recoded.mp4
done
