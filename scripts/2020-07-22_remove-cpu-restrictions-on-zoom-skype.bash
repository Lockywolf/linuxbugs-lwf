#!/bin/bash
# Time-stamp: <2020-07-22 09:12:47 lockywolf>
#+created: <2020-07-22 Wed 09:04>
#+author: lockywolf gmail.com

set -oue pipefail

case "$1" in
    skype)
	printf "case up\n"
	sudo cgclassify --sticky -g cpu:sysdefault "$(pgrep skype)"
	;;
    zoom)
	printf "case down\n"
	sudo cgclassify --sticky -g cpu:sysdefault "$(pgrep zoom)"

	;;
    status)
	printf "not implemented\n"
	;;
    *) printf "usage: {skype,zoom}\n"
esac


