#!/usr/bin/env bash
# example: ./font_find.sh 🎩︎
# credits: David Baynard, https://unix.stackexchange.com/a/393740/14907


request_string=""
for i in "${@}" ; do
#  echo looking for "$i"
  char=${i:0:1}
  request_string="${request_string}"$(printf '%x' \'"$char")","
done
request_string="${request_string:0:-1}"
printf "looking for %s\n" "$request_string"
#exit 1
fc-list ":charset=$request_string"
fc-list ":charset=$request_string" | awk '{print $1;}' | sort -V | uniq

