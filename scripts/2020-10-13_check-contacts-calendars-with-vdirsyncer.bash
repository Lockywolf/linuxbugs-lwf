#!/bin/bash
# Time-stamp: <2023-12-01 20:48:12 lockywolf>
# Author: lockywolf gmail.com
# Created: <2020-10-13 Tue 10:30>

# TODO: redirect error to dev/null, and only fail if vdirsyncer does not work for quite a while

#set -e

#REPORTS_DIR=/run/user/$(id -u)/vdirsyncer
REPORTS_DIR="$HOME/.local/var/lwf_vdirsyncer"
mkdir -p "$REPORTS_DIR"

printf "Running vdirsyncer discover\n"
if ! ~/bin/wgexec vdirsyncer --verbosity ERROR discover 2>&1
then
    printf "Vdirsyncer discover failed!\n"
    printf "Vdirsyncer discover failed: %s\n" "$(date --iso=minutes)" > "$REPORTS_DIR"/last-failure
    exit 0
fi
printf "\n"
sleep 10

printf "Running vdirsyncer metasync\n"
if ! ~/bin/wgexec vdirsyncer --verbosity ERROR metasync 2>&1
then
    printf "Vdirsyncer metasync failed!\n"
    printf "Vdirsyncer metasync failed: %s\n" "$(date --iso=minutes)" > "$REPORTS_DIR"/last-failure
    exit 0
fi
printf "\n"
sleep 10
printf "Running vdirsyncer sync\n"
if ! ~/bin/wgexec vdirsyncer --verbosity ERROR sync 2>&1
then
    printf "Vdirsyncer sync failed!\n"
    printf "Vdirsyncer sync failed: %s\n" "$(date --iso=minutes)" > "$REPORTS_DIR"/last-failure
    exit 0
fi

printf "Vdirsyncer succeeded: %s\n" "$(date --iso=minutes)" > "$REPORTS_DIR"/last-success

(
  cd "$HOME"/PIM/Contacts-vdir
  git add .
  git commit -m "Update $(date --iso=minutes)"
)

NOW=$(date '+%s')
LAST_CHANGE=$(stat --format='%Y' "$HOME/PIM/Contacts-vdir/lockywolf@gmail.com/default"/* | sort | tail -1 )
LAST_CHANGE=${LAST_CHANGE:-0}
if (( NOW - LAST_CHANGE > 60*60*24*30 ))
then
    printf "No change in contacts for too long\n" 1>&2
fi

LAST_SUCCESS=$(stat --format='%Y' "$REPORTS_DIR"/last-success)
LAST_SUCCESS=${LAST_SUCCESS:-0}
LAST_FAILURE=$(stat --format='%Y' "$REPORTS_DIR"/last-failure)
LAST_FAILURE=${LAST_FAILURE:-$(( 60 * 60 * 24 * 30 ))}

if (( LAST_FAILURE - LAST_SUCCESS > 60*60*24*1 ))
then
    printf "No successful check for contacts for too long\n" 1>&2
fi


exit 0

