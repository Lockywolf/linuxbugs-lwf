#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-12-23 18:07:19 lockywolf>
#+title: Run mpv with hardware decoding.
#+author: lockywolf
#+date: 
#+created: <2021-11-03 Wed 19:15>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :bash:multimedia:mpv:vdpau:vaapi:cuda
#+creator: Emacs 27.1

coproc MONITOR {
while true ; do
  l_bashpid=$BASHPID
  l_ppid=$(ps -p $l_bashpid -o ppid= )
  if (( l_ppid == 1 )) ; then
    printf "I was reparented to init!\n"
    exit 1
  fi
  xdg-screensaver reset ;
  sleep 10 ;
done
}
l_pid=$!
l_jobid='%1'

trap "kill $l_pid" EXIT

__NV_PRIME_RENDER_OFFLOAD=1 __VK_LAYER_NV_optimus=NVIDIA_only __GLX_VENDOR_LIBRARY_NAME=nvidia mpv --ao=pulse --vo=gpu --hwdec=nvdec "$@"

#printf ""
