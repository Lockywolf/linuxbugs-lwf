#!/bin/bash
# -*- mode:sh; -*-
# Time-stamp: <2023-05-11 11:31:00 lockywolf>
#+title: Read all xattrs from file.
#+author: lockywolf
#+date: 
#+created: <2023-05-11 Thu 11:30>
#+refiled:
#+language: bash
#+category: admin
#+filetags: :xattrs:admin:bash:
#+creator: Emacs GNU Emacs 29.0.50 (build 1, x86_64-slackware-linux-gnu, GTK+ Version 3.24.31, cairo version 1.16.0) of 2022-02-23


if [[ "$1" == "" ]] ; then printf "Give me filename!\n" ; exit 1 ; fi

sudo getfattr -m '.*' -d  "$1"
