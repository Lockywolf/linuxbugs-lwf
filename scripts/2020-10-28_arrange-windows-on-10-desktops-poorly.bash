#!/bin/bash

#+updated: <2020-12-13 Sun 14:17>

readarray arrayFirefox < <(wmctrl -lxp | grep -i Navigator\.Firefox | gawk '{print $1}')
readarray arrayYuWei < <(wmctrl -lxp | grep -i YuWei-Work | gawk '{print $1}')
readarray arrayEmacs < <(wmctrl -lxp | grep -i emacs\.Emacs | gawk '{print $1}')
#readarray arrayPidgin < <(wmctrl -lxp | grep -i Pidgin\.Pidgin | gawk '{print $1}')
readarray arrayChromium < <(wmctrl -lxp | grep -i 'chromium .*\.Chromium' | gawk '{print $1}')
readarray arrayWineWechat < <(wmctrl -lxp | grep -i wechat.exe.wechat.exe | gawk '{print $1}')
readarray arrayThunderbird < <(wmctrl -lxp | grep -i Mail.Thunderbird | gawk '{print $1}')
readarray arrayDino < <(wmctrl -lxp | grep -i dino.Dino | gawk '{print $1}')
readarray arrayJStock < <(wmctrl -lxp | grep -i jstock | gawk '{print $1}')
readarray arrayRecoll < <(wmctrl -lxp | grep -i recoll.recoll | gawk '{print $1}')
# readarray arrayTransmissionRemote < <(wmctrl -lxp | grep -i transmission-remote-gtk\\.Transmission-remote-gtk | gawk '{print $1}')
readarray arrayMatlab < <(wmctrl -lxp | grep -i "sun-awt-X11-XFramePeer\\.MATLAB" | gawk '{print $1}')
readarray arrayQemu < <(wmctrl -lxp | grep -i "qemu.Qemu-system-x86_64" | gawk '{print $1}')
readarray arrayQemuTerm < <(wmctrl -lxp | grep -i "vbox-openbsd.lockywolf.net" | gawk '{print $1}')
readarray arrayTencentMeet < <(wmctrl -lxp | grep -i "wemeet" | gawk '{print $1}')


wmctrl -r 'hexchat' -t 9

wmctrl -r 'discord'   -t 0
wmctrl -r 'wemeetapp' -t 0

for f in "${arrayTencentMeet[@]}"
do
    wmctrl -i -r "$f" -t 0
    wmctrl -i -r "$f" -b add,maximized_vert,maximized_horz
done


for f in "${arrayJStock[@]}"
do
    wmctrl -i -r "$f" -t 5
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayRecoll[@]}"
do
    wmctrl -i -r "$f" -t 8
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done


for f in "${arrayDino[@]}"
do
    wmctrl -i -r "$f" -t 0
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done


for f in "${arrayWineWechat[@]}"
do
    wmctrl -i -r "$f" -t 0
done

for f in "${arrayThunderbird[@]}"
do
    wmctrl -i -r "$f" -t 0
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz

done


for f in "${arrayFirefox[@]}"
do
    wmctrl -i -r "$f" -t 3
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayChromium[@]}"
do
    wmctrl -i -r "$f" -t 4
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayEmacs[@]}"
do
    wmctrl -i -r "$f" -t 2
done

# for f in "${arrayPidgin[@]}"
# do
#     wmctrl -i -r "$f" -t 4
#     wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
# done


# for f in "${arrayTransmissionRemote[@]}"
# do
#     wmctrl -i -r "$f" -t 10
#     wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
# done

for f in "${arrayMatlab[@]}"
do
    wmctrl -i -r "$f" -t 7
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayQemu[@]}"
do
    wmctrl -i -r "$f" -t 7
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayQemuTerm[@]}"
do
    wmctrl -i -r "$f" -t 7
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done

for f in "${arrayYuWei[@]}"
do
    wmctrl -i -r "$f" -t 6
    wmctrl -i -r "$f" -b  add,maximized_vert,maximized_horz
done


wmctrl -x -r 'xfce4-terminal.Xfce4-terminal' -t 1
wmctrl -x -r 'xfce4-terminal.Xfce4-terminal' -b add,maximized_vert,maximized_horz

wmctrl -x -r 'skype.skype' -t 0

wmctrl -x -r 'slack.slack' -t 0
wmctrl -x -r 'zoom.zoom' -t 0


